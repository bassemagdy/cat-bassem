import { Component, Input } from '@angular/core';
import { DriversService } from '../../services/drivers.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import {CarTypesService} from '../../services/car-types.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-drivers',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  
    driver = {
    name:'',
    national_id:'',
    phone_number:'',
    license_number:'',
    license_degree:'',
    license_expiry:'',
  };

  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };

  constructor(private router: Router, private cookie: CookieService, private cartypesService: CarTypesService, 
    private modalService: NgbModal , 
public driversService : DriversService
  ) {
 
  }
back(){
  this.router.navigate( [ '/drivers']);


}



  done() {
    console.log(this.driver);
    let id=[];
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };
    this.driversService.create({ driver: this.driver
    }).subscribe((reply) => {
      this.driversService.index().subscribe((repo) => {
        if(reply){
        console.log(repo.data[repo.data.length-1].id);
        id.push(repo.data[repo.data.length-1].id);
        let password = reply;
        let driversRef = firebase.database().ref("/drivers/Auth/"+id[0]);
        driversRef.set({
          id: id[0],
          phone: this.driver.phone_number,
          password: password
        });
      }
    })
    // console.log(id[0]);
    
     
      // firebase.database().ref('/drivers/profile').push({phone:this.driver.phone_number, password:password});
      this.router.navigate ( [ '/drivers/'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}





