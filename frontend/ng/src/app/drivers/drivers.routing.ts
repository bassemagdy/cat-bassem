import { Routes } from '@angular/router';
 import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewall/viewall.component';
 import { ViewComponent } from './view/view.component';

export const DriversRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'Create driver'
      }
    },
    {
      path: '',
      component: ViewAllComponent,
      data: {
        heading: 'View All drivers'
      }
    },
    
    {
      path: 'view/:id',
      component: ViewComponent,
      data: {
      heading: 'View driver'
      }
    },
  ],
  }
];
