import {Component, Input, OnInit} from '@angular/core';
import {DriversService} from '../../services/drivers.service';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { SecurityService } from '../../services/security.service';

@Component({
selector: 'app-drivers-viewll',
templateUrl: './viewall.component.html',
styleUrls: ['./viewall.component.scss']
})
export class ViewAllComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;



  constructor(
    private router: Router,
    private driversService: DriversService,
    public securityService: SecurityService,
    public modalService: NgbModal
  ) {}

  ngOnInit() {
    this.driversService.index().subscribe((reply) => {
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      console.log(this.rows);

    //  this.collectionSize = reply.count.count;
    });
  }

  delete(id) {
    if(confirm('Are you sure you want to delete')) {
      this.spinner = 1;
      this.driversService.delete({id: id}).subscribe((reply) => {
        this.ngOnInit();
        this.spinner = 0;
      });
    }
  }

  open(driver) {
    this.router.navigate ( [ '/drivers/view/' + driver.id] );
  }

  create() {
    this.router.navigate ( [ '/drivers/create/'] );
  }
}  
