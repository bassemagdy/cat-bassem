import { Component, Input, OnInit } from '@angular/core';
import {DriversService} from '../../services/drivers.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CarTypesService} from '../../services/car-types.service';



@Component({
  selector: 'app-view-driver',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
    driver = {
      name:'',
      national_id:'',
      phone_number:'',
      license_number:'',
      license_degree:'',
      license_expiry:'',
    };
  constructor(private router: Router,private activatedRoute: ActivatedRoute,
     private cookie: CookieService,
     public driversService : DriversService
    ) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });
}

  ngOnInit() {
     this.driversService.view({id: this.id}).subscribe((reply) => {
     console.log(this.id);
      console.log(reply);
      this.driver = reply;
      console.log("/////////////");
      console.log(this.driver);
    });
  }

  editDriver() {
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/drivers/'] );
  }


  done() {
    console.log(this.driver);
    this.driversService.edit({ 
        
        name:this.driver.name ,
    national_id:this.driver.national_id,
    phone_number: this.driver.phone_number,
    license_number: this.driver.license_number,
    license_degree: this.driver.license_degree,
    license_expiry: this.driver.license_expiry,
    id:this.id }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/drivers/'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




