import { Routes } from '@angular/router';

import { CreateComponent } from './create/create.component';




import { ViewAllComponent } from './viewall/viewAll.component';
import { ViewComponent } from './view/view.component';

export const PaymentsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'Create Payments'
      }
    },
    {
      path: '',
      component: ViewAllComponent,
      data: {
        heading: 'View All Payments'
      }
    },
    
    {
      path: 'view/:id',
      component: ViewComponent,
      data: {
      heading: 'View payment'
      }
    },
  ],
  }
];
