import {Component, Input, OnInit} from '@angular/core';
import {PaymentsService} from '../../services/payments.service';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { SecurityService } from '../../services/security.service';

@Component({
selector: 'app-payments-viewall',
templateUrl: './viewAll.component.html',
styleUrls: ['./viewAll.component.scss']
})
export class ViewAllComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;



  constructor(
    private router: Router,
    private paymentsService: PaymentsService,
    public securityService: SecurityService,
    public modalService: NgbModal
  ) {}

  ngOnInit() {
    this.paymentsService.index().subscribe((reply) => {
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
    //  this.collectionSize = reply.count.count;
    });
  }

  delete(id) {
    if(confirm('Are you sure you want to delete')) {
      this.spinner = 1;
      this.paymentsService.delete({id: id}).subscribe((reply) => {
        this.ngOnInit();
        this.spinner = 0;
      });
    }
  }

  open(payment) {
    this.router.navigate ( [ '/payments/view/' + payment.id] );
  }

  create() {
    this.router.navigate ( [ '/payments/create'] );
  }
}
