import { Component, Input, OnInit } from '@angular/core';
import { PaymentsService } from '../../services/payments.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-view-payment',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id: string;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
  payment={
    name:'',
    id:'',
  };

  constructor(private router: Router,private activatedRoute: ActivatedRoute, private cookie: CookieService, private paymentsService: PaymentsService) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });
}

  ngOnInit() {
     this.paymentsService.view({id: this.id}).subscribe((reply) => {
     console.log(this.id);
      console.log(reply);
      this.payment = reply;
      console.log(this.payment);
    });
  }

  editPayments() {
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/payments'] );
  }


  done() {
    console.log(this.payment);
    this.paymentsService.edit({ name: this.payment.name,id:this.payment.id }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/payments'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




