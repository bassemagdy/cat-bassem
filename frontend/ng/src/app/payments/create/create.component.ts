import { Component, Input } from '@angular/core';
import { PaymentsService } from '../../services/payments.service';
import { EmployeeService } from '../../services/employee.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
  selector: 'app-payments',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  
  payment = {
    name: '',

  };
  
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };

  constructor(private router: Router, private cookie: CookieService, private paymentsService: PaymentsService, private modalService: NgbModal) {
 
  }
back(){
  this.router.navigate( [ '/payments']);


}



  done() {
    console.log(this.payment);
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };
    this.paymentsService.create({ name: this.payment.name
    }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/payments'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}





