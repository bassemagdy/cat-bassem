import { NgModule, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import {  NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { LeadsComponent } from './leads.component';
import { LeadsRoutes } from './leads.routing';

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(LeadsRoutes),
            FormsModule,
            ReactiveFormsModule,
            JsonpModule,
            NgbModule],
  declarations: [LeadsComponent]
})

export class LeadsModule {}
