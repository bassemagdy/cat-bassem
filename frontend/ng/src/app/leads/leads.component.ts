import {TablesService} from '../services/tables.service';
import {Component, OnInit, Input, Renderer2} from '@angular/core';
import {DataService} from '../services/data.service';
import {DragulaService} from 'ng2-dragula/ng2-dragula';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import { CatServiceService } from '../services/cat-service.service';
import { ProductService } from '../services/product.service';
import { LeadService } from '../services/lead.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { EmployeeService } from '../services/employee.service';
import { DepartmentService } from '../services/department.service';
import { ScrollToService, ScrollToConfig } from '@nicky-lenaers/ngx-scroll-to';
import { ModalAddLead } from '../taskboard/taskboard.component';
import { ModalViewLead } from '../taskboard/taskboard.component';
import { SecurityService } from '../services/security.service';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.scss']
})
export class LeadsComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  fulltext = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;
  call=false;
  alert: alert = {
    visible: false,
    type: '',
    message: ''
  };
  timeFrom:'';
  timeTo:'';



  constructor(
    private tablesService: TablesService,
    private leadService: LeadService,
    public securityService: SecurityService,
    public modalService: NgbModal
  ) {}

  ngOnInit() {
    this.tablesService.getData().subscribe((reply) => {
      console.log(reply.data);
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;

      for (let i = 0; i < this.headersType.length; i++) {
        if (this.headersType[i] === 'string') {
          this.fulltext.push(this.headers[i]);
        }
      }

    });

   


  }

  dismissAlert() {
    this.alert = {
      visible: false,
      type: '',
      message: ''
    };
  }

  onPager(event) {
      if (!isNaN(event)) {
        if (((Math.floor(event / this.pageSize)) !== this.group)) {
        this.tablesService.paginate({
          'page': event,
          'columns': this.headersSearch,
          'keyword': this.keyword,
          'column': this.selectedHeader,
          'order': this.order
        }).subscribe((reply) => {

          this.group = event % this.pageSize;
          this.data = reply;
          this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);

        });
      } else {
        this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);
      }
      } else {
        this.page = 1;
      }
  }



  delete(id) {
    if(confirm('Are you sure you want to delete')) {
      this.spinner = 1;
      this.leadService.delete({id: id}).subscribe((reply) => {
        this.ngOnInit();
        this.spinner = 0;
      });
    }
    else {
     console.log("hi");
return;

    }
    
  }
  search() {
    this.spinner = 1;
    this.tablesService.search({
      'columns': this.headersSearch,
      'keyword': this.keyword
    }).subscribe((reply) => {
      console.log(reply);
      this.data = reply.data;
      this.group = 0;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;
      console.log(reply.count.count);
      this.spinner = 0;
    });
}

  sort(column) {
    if (this.selectedHeader === column && this.order === 'asc') {
      this.order = 'desc';
      this.asc = 0;
    } else if (this.selectedHeader === column && this.order === 'desc') {
      this.order = 'asc';
      this.asc = 1;
    } else {
      this.selectedHeader = column;
    }
    this.spinner = 1;
    this.tablesService.sort({
      'column': column,
      'order': this.order,
      'keyword': this.keyword,
      'columns': this.headersSearch
    }).subscribe((reply) => {
      this.data = reply;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.spinner = 0;
    });
  }



  

  
  newLead(){
    const modalRef = this.modalService.open(ModalAddLead);
    modalRef.result.then((result) => {
      console.log(result);
      this.leadService.createLead(result).subscribe((reply) => {
        console.log(reply);
        this.ngOnInit();
          this.alert = {
            visible: true,
            type: 'success',
            message: 'Successfully added!'
          }
          setTimeout(() => {
            this.dismissAlert();
          }, 1000);
      }, (reply) => {
        this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          }
      })

    }, (result) => {

    });
  }
  viewLead(lead,m) {
    if (m==true){
      console.log("wasl");

      this.leadService.getLead({id:lead.id, customer:lead.customer_id}).subscribe((reply) => {
        const modalRef = this.modalService.open(ModalViewLead);
        modalRef.componentInstance.lead = lead;
        modalRef.componentInstance.products = reply.products;
        modalRef.componentInstance.services = reply.services;
        modalRef.componentInstance.customer = reply.customer;
        modalRef.result.then((result) => {  
        }, (reply) => {});
  
        }, (result) => {
  
      });


    }
 
  }

showdate(){
  let space = ' ';
  if(this.timeFrom.length>0 && this.timeTo.length>0){
  console.log(this.timeFrom.split('T').join(space));
  console.log(this.timeTo.split('T').join(space));
  
  this.tablesService.searchDates({dateFrom:this.timeFrom.split('T').join(space), dateTo:this.timeTo.split('T').join(space)}).subscribe((reply)=>{
    this.data = reply.data;
    this.group = 0;
    this.page = 1;
    this.rows = this.data.slice(this.page - 1, this.pageSize);
    this.collectionSize = reply.count;
    this.spinner = 0;
  });}
  else{
    this.alert = {
      visible: true,
      type: 'danger',
      message: 'Please Enter Valid Dates'
    }
  }
}

}

interface alert {
  visible: boolean;
    type: string;
    message: string;
}