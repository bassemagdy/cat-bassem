import { Routes } from '@angular/router';

import { LeadsComponent } from './leads.component';

export const LeadsRoutes: Routes = [{
  path: '',
  component: LeadsComponent,
  data: {
    heading: 'Leads',
    removeFooter: true
  },

}];
