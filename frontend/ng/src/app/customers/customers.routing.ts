import { Routes } from '@angular/router';

// import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewAll/viewAll.component';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';

export const CustomersRoutes: Routes = [
  {
    path: '',
    children: [
    {
      path: '',
      component: ViewAllComponent,
      data: {
        heading: 'View All Customers'
      }
    },
    {
        path: 'view/:id',
        component: ViewComponent,
        data: {
          heading: 'View Customer'
        }
      },
      {
        path: 'create',
        component: CreateComponent,
        data: {
          heading: 'Create Customers'
        }
      }
   
  ]
  }
];
