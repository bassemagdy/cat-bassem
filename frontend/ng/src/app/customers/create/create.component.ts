import { Component, Input } from '@angular/core';
import { StatusService } from '../../services/car-status.service';
import { DepartmentService } from '../../services/department.service';
import { CustomersService } from '../../services/customers.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
// import {RlTagInputModule} from 'angular2-tag-input';
import { PaymentsService } from '../../services/payments.service';


@Component({
  selector: 'app-create-cars',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

name;
phone;
email;
address;
phone2;
email2;
company;
payment_info;
vip;
notes;
payments;

  alert: alert = {
      visible: false,
      type: "",
      message: ""
    };
   
  
 
  constructor(private router: Router,private cookie: CookieService, private CustomersService: CustomersService, private modalService: NgbModal , public statusService : StatusService,      public paymentsService: PaymentsService,
  ) {

    this.CustomersService.index().subscribe((reply) => {
      console.log(reply);
    });

    this.paymentsService.index().subscribe((reply) => {
      this.payments = reply.data;
      console.log(reply.data);
    });

  }

 back(){
  this.router.navigate ( [ '/customers'] );
 } 



  done(){
    // console.log(this.car);
   
    
   
    

    this.alert = {
            visible: true,
            type: "info",
            message: "Please wait - this may take a while"
          }
    // this.car.status= this.statusname;
    this.CustomersService.create({ customerName: this.name, customerPhone:this.phone,
    customerPhone2:this.phone2,customerPayment:this.payment_info,customerAddress:this.address,
    customerEmail:this.email,customerEmail2:this.email2,customerCompany:this.company,
    vip:this.vip}).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/customers'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    })
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}


  export interface From {
      name: string;
      email: string;
  }

  export interface To {
      name: string;
      email: string;
  }

  export interface Bcc {
      name: string;
      email: string;
  }

  interface RootObject {
      html: string;
      text: string;
      subject: string;
      from: string;
      to: string;
      bcc: string;
  }








