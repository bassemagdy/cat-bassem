
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RlTagInputModule } from 'angular2-tag-input';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

import { TagInputModule } from 'ngx-chips';
import {  NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CommonModule } from '@angular/common';
import { CustomersRoutes } from './customers.routing';
//import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewAll/viewAll.component';
import { ViewComponent } from './view/view.component';
import { CreateComponent } from './create/create.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CustomersRoutes),
    FormsModule,
    RlTagInputModule,
    NgbCollapseModule,
    TagInputModule,
    ReactiveFormsModule,
    NgbModule
  ],
  declarations: [ ViewAllComponent, ViewComponent, CreateComponent]
})

export class CustomersModule {}
