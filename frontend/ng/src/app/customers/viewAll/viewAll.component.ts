import {Component, Input, OnInit} from '@angular/core';
import {CustomersService} from '../../services/customers.service';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { SecurityService } from '../../services/security.service';
import {TablesService} from '../../services/tables.service';
import { ActivatedRoute, Params } from '@angular/router';
import {FormsModule} from "@angular/forms";
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';
 
@Component({
selector: 'app-customers-viewall',
templateUrl: './viewAll.component.html',
styleUrls: ['./viewAll.component.scss']
})
export class ViewAllComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;
  myid;
customer;


  constructor(
    private router: Router,
    private customersService: CustomersService,
    public modalService: NgbModal,
    public securityService: SecurityService,
    public cookie: CookieService,
    private tablesService: TablesService,

  ) {
  }

  ngOnInit() {
    this.customersService.getData().subscribe((reply) => {
        
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count;
    });
  }
  create(){
    this.router.navigate(['/customers/create']);
  }
  onPager(event) {
    console.log(event);
    if (!isNaN(event)) {
      if (((Math.floor(event / this.pageSize)) !== this.group)) {
      this.customersService.paginate({
        'page': event,
        'columns': this.headersSearch,
        'keyword': this.keyword,
        'column': this.selectedHeader,
        'order': this.order
      }).subscribe((reply) => {

        this.group = event % this.pageSize;
        this.data = reply;
        this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);

      });
    } else {
      this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);
    }
    } else {
      this.page = 1;
    }
}


   open(customer) {
  
        this.router.navigate ( [ '/customers/view/' + customer.id] );
   
      }

  delete (id){
        if (confirm('Are you sure you want to delete' )){
            this.customersService.delete({id: id}).subscribe((reply)=>{
            this.ngOnInit();
          });
 
        }
     }

     edit(idd){
this.customersService.viewCustomer({id : idd}).subscribe((reply)=>{

console.log(reply);
this.customer=reply;
const modalRef = this.modalService.open(ModalViewLead2);
modalRef.componentInstance.customer=this.customer[0];
modalRef.componentInstance.id = idd;
modalRef.result.then((result) => {
  this.ngOnInit();

});

// console.log(this.customer[0]);
});
       
console.log(idd);
     }

  



      
  
}





@Component({
  selector: 'ngbd-modal-view-lead2',
  templateUrl: './modalViewCustomer.html',
  styleUrls: ['./viewAll.component.scss']

})

export class ModalViewLead2 implements OnInit {


  @Input() customer;
  @Input() id;
customerr = {
customerName:'',
customerAddress :'',  
customerCompany :'' ,
customerPhone :'',
customerPhone2:'',
customerEmail:'' ,
customerEmail2:'' ,
id:''};



  constructor(
    private router: Router,
    public activeModal: NgbActiveModal,
    public securityService: SecurityService,
  public customersService : CustomersService) {
      console.log(this.customer);

  }

  ngOnInit() {
    console.log(this.id);
    this.customerr.id = this.id;
    this.customerr.customerName = this.customer.name;
    this.customerr.customerAddress = this.customer.address;  
    this.customerr.customerCompany = this.customer.company;
    this.customerr.customerPhone = this.customer.phone;
    this.customerr.customerPhone2 = this.customer.phone2;
    this.customerr.customerEmail = this.customer.email;
    this.customerr.customerEmail2 = this.customer.email2;
  }





  
  
  close() {
    this.customersService.update({customer :this.customerr}).subscribe((reply)=>{
console.log(reply);
//this.close();
  });
    
    
    this.activeModal.close({
   
   
   
    });
    this.ngOnInit();

  }


}

   
   
//     //  this.leadService.edit({products: this.products,
//     //                         count: this.count,
                            
//     //                         chosenproducts: this.chosenproducts,
//     //                         serviceID: this.serviceID, 
//     //                         servicedata: this.output, 
//     //                         services: this.services, 
//     //                         id: this.lead.id, 
//     //                         customer: this.customer, 
//     //                         deletedProducts: this.deletedProducts,
//     //                         deletedServices: this.deletedServices,
//     //                         lead: this.lead}).subscribe((reply) => {
//     //      console.log(reply);
//     //    //  this.close();
//     //    })

    
//   //   }
//   //    this.ngOnInit();
//   // }

 

  


  
  

 


 
  //  }