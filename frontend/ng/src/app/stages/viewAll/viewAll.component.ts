import {Component, Input, OnInit} from '@angular/core';
import {StagesService} from '../../services/stages.service';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { SecurityService } from '../../services/security.service';

@Component({
selector: 'app-stagess-viewall',
templateUrl: './viewAll.component.html',
styleUrls: ['./viewAll.component.scss']
})
export class ViewAllComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;
  noitems;



  constructor(
    private router: Router, 
    private stagesService: StagesService,
    public securityService: SecurityService,
    public modalService: NgbModal
  ) {}

  ngOnInit() {
    this.stagesService.index().subscribe((reply) => {
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;
    });
  }

 /* delete(id) {
    if(confirm('Are you sure you want to delete')) {
      this.spinner = 1;
      this.stagesService.delete({id: id}).subscribe((reply) => {
        this.ngOnInit();
        this.spinner = 0;
      });
    }
  }

*/
  delete(id){
    if(confirm('Are you sure you want to delete')) {
        this.stagesService.countstage({delete: id}).subscribe((reply)=> {
          
          this.noitems=reply;  
          
if (this.noitems == 0){
console.log("nere");
  this.stagesService.delete({delete: id}).subscribe((reply) => {
    
    for(var i = 0; i < this.rows.length; i++)
    {
      if(this.rows[i].id == id)
        this.rows.splice(i,1);
    }
    return;

  })

}


if (this.noitems != 0){
  
      const modalRef = this.modalService.open(ModalDeleteStage);
      modalRef.componentInstance.stages = this.rows;
      modalRef.componentInstance.del = id;
      modalRef.result.then((result) => {
        console.log(result);
        this.stagesService.delete({replacement: result, delete:  id}).subscribe((reply) => {
          console.log(reply);
          this.ngOnInit();
          this.spinner = 0;
          for(var i = 0; i < this.rows.length; i++)
            {
              if(this.rows[i].id == id)
                this.rows.splice(i,1);
            }
        })
      }, (result) => {});
    }
    })
    }
  
  }


  open(stage) {
    this.router.navigate ( [ '/stages/view/' + stage.id] );
  }

  create() {
    this.router.navigate ( [ '/stages/create'] );
  }
}



@Component({
  selector: 'modal-delete-stage',
  templateUrl: 'modalDeleteStage.html',
  styleUrls:['./viewAll.component.scss']
})
export class ModalDeleteStage implements OnInit {

  @Input() stages;
  @Input() del;
  stage;
  viewstages = [];

  constructor(public activeModal: NgbActiveModal, public stagesService: StagesService) {
  }

  ngOnInit(){
    for(var i = 0; i < this.stages.length; i++)
    {
      if(this.stages[i].id != this.del)
        this.viewstages.push(this.stages[i]);
    }
  }


  close() {
    console.log(this.stage);
    this.activeModal.close(this.stage);
  }




}