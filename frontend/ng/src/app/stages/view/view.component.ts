import { Component, Input, OnInit } from '@angular/core';
import { StagesService } from '../../services/stages.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';


@Component({
  selector: 'app-view-stage',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
    stage={
      name:'',
      sort:'',
      id:''
    };
  constructor(private router: Router,private activatedRoute: ActivatedRoute, private cookie: CookieService, private stagesService: StagesService) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });
}

  ngOnInit() {
     this.stagesService.view({id: this.id}).subscribe((reply) => {
      console.log(reply);
      this.stage = reply;
      console.log(this.stage);
    });
  }

  editStages() {
    this.edit = !this.edit;
  }


  back(){
    this.router.navigate ( [ '/stages'] );
  }

  
  done() {
    console.log(this.stage);
    this.stagesService.edit({ name: this.stage.name, sort: this.stage.sort, id: this.stage.id }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/stages'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




