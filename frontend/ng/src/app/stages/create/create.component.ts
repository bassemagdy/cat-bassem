import { Component, Input } from '@angular/core';
import { StagesService } from '../../services/stages.service';
import { EmployeeService } from '../../services/employee.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
  selector: 'app-stages',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  stage = {
    name: '',
    sort: ''
  };

  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };

  constructor(private router: Router, private cookie: CookieService, private stagesService: StagesService, private modalService: NgbModal) {
  }

  back(){
    this.router.navigate  ( [ '/stages'] );
  }


  done() {
    console.log(this.stage);
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };

var m = +this.stage.sort;
console.log(m);
          if (m<0){

            this.alert = {
              visible: true,
              type: 'danger',
              message: 'Something went wrong. Please enter another number'
            };
            return ;
          }
    this.stagesService.create({ name: this.stage.name, sort: this.stage.sort
    }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/stages'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}





