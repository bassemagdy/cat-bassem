import { Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewAll/viewAll.component';
import { ViewComponent } from './view/view.component';

export const CarTypesRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'Create type'
      }
    },
    {
      path: '',
      component: ViewAllComponent,
      data: {
        heading: 'View All types'
      }
    },
    
    {
      path: 'view/:id',
      component: ViewComponent,
      data: {
      heading: 'View type'
      }
    },
  ],
  }
];
