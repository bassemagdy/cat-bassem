import { Component, Input } from '@angular/core';
import { StatusService } from '../../services/car-status.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import {CarTypesService} from '../../services/car-types.service';

@Component({
  selector: 'app-types',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {


    car_type = {
    model: '',
    manufacturer:'',
    productionYear:'',
    capacityPeople:'',
    capacityBags:''
  };

  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };

  constructor(private router: Router, private cookie: CookieService, private cartypesService: CarTypesService, private modalService: NgbModal) {

  }
back(){
  this.router.navigate( [ '/car-types']);


}



  done() {
    console.log(this.car_type);
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };
    this.cartypesService.create({ car_type: this.car_type
    }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/cartypes/'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}
