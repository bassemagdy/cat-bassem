import { Component, Input, OnInit } from '@angular/core';
import { StatusService } from '../../services/car-status.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CarTypesService} from '../../services/car-types.service';



@Component({
  selector: 'app-view-type',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
  type={
    model:'',
    manufacturer:'',
    productionYear:'',
    id:'',
    capacityPeople:'',
    capacityBags:''

  };

  constructor(private router: Router,private activatedRoute: ActivatedRoute,
     private cookie: CookieService,
      private statusService: StatusService ,
      private cartypesService: CarTypesService
    ) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });
}

  ngOnInit() {
     this.cartypesService.view({id: this.id}).subscribe((reply) => {
     console.log(this.id);
      console.log(reply);
      this.type = reply;
      console.log("/////////////");
      console.log(this.type);
    });
  }

  editType() {
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/cartypes/'] );
  }


  done() {
    console.log(this.type);
    this.cartypesService.edit({ model: this.type.model,manufacturer : this.type.manufacturer,productionYear :this.type.productionYear, id:this.type.id,capacityPeople:this.type.capacityPeople, capacityBags:this.type.capacityBags }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/cartypes/'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}
