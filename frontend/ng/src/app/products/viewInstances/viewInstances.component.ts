import {Router, ActivatedRoute, Params} from '@angular/router';
import {OnInit, Component} from '@angular/core';
import { ProductService } from '../../services/product.service';
import { SecurityService } from '../../services/security.service';

@Component({
  selector: 'app-viewInstances',
  templateUrl: './viewInstances.component.html',
  styleUrls: ['./viewInstances.component.scss']
})
export class ViewInstancesComponent implements OnInit{
  
  isCollapsed = [];
  products:product[] = [];

 constructor(
   public router: Router,
   private activatedRoute: ActivatedRoute,
   public securityService: SecurityService,
   public productService: ProductService
  ) {}

  ngOnInit() {

    this.productService.getAllProducts().subscribe((reply) => {
      console.log(reply);
      this.products = [];
      for (var i = 0; i < reply.length; i++) {
        var data = reply[i].data.split(';');
        data.pop();
        this.products.push({ id:reply[i].id, category: reply[i].Category, name: reply[i].Name, details: reply[i].details, data: data});
        this.isCollapsed.push(true);
      }
    });
  }

  edit(id){
    this.router.navigate ( [ '/products/editInstances/' + id ] );
  }

  delete(id){
    if(confirm("Are you sure you want to delete")) {
    this.productService.deleteInstance({id:id}).subscribe((reply) => {
      console.log(reply);
      this.ngOnInit();
    })
  }
  }




}


interface product {
   id: number,
   category: string,
   name: string,
   details: string,
   data: string[]
}
