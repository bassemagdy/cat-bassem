import {Router, ActivatedRoute, Params} from '@angular/router';
import {OnInit, Component} from '@angular/core';
import { ProductService } from '../../services/product.service';
import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-edit-instances',
  templateUrl: './editInstances.component.html',
  styleUrls: ['./editInstances.component.scss']
})
export class EditInstancesComponent implements OnInit{
  
  id;
  product;
  details;
  name;
  fieldTypes = [];
  multivalued = [];
  data = [];
  alert: alert = {
      visible: false,
      type: "",
      message: ""
    };
  
 constructor(private router: Router, private activatedRoute: ActivatedRoute, public productService:ProductService) {}

  ngOnInit() {
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['name'];
      });

    this.productService.getInstance({id: this.id}).subscribe((reply) => {
      console.log(reply);
      this.product = reply[0].name; //category name
      this.name = reply[0].Name;
      var data = reply[0].data;
      var definition = reply[0].definition;
      var dataSeperateFields = data.split(";");
      var defSeperateFields = definition.split(";");
      for(var i = 0; i < dataSeperateFields.length - 1; i++)
      {
          var dataSeperateValues = dataSeperateFields[i].split(":");
          var defSeperateValues = defSeperateFields[i].split(":");
          this.data[i] = [];
          this.data[i][0] = dataSeperateValues[0];
          if(defSeperateValues[1]!='datetime')
            this.data[i][1] = dataSeperateValues[1];
          else
            this.data[i][1] = dataSeperateValues[1]+":"+dataSeperateValues[2];

          // this.fields.push(seperateValues[0]);
           this.fieldTypes.push(defSeperateValues[1]);
          if(defSeperateValues.length > 2)
          {
            var values = defSeperateValues[2].split(',');
            this.data[i][2] = values;
          }            

      }
    })
  }



  done(){
    console.log(this.data);
    // this.output = [];
    // for(var i = 0; i < this.input.length; i++)
    // {
    //   this.output.push(this.fields[i] + ':' + this.input[i]);
    // }
    // console.log(this.output);
     this.productService.editProductInstance({  id: this.id, name: this.name, data: this.data }).subscribe((reply) => {
         this.router.navigate ( [ '/products/viewInstances'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    });
      
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}
