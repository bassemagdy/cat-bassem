import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { RlTagInputModule } from 'angular2-tag-input';
import { NgbCollapseModule, NgbModal,NgbActiveModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { DateTimePickerModule } from 'ng-pick-datetime';
import { TagInputModule } from 'ngx-chips';

import { CommonModule } from '@angular/common';
import { ProductsRoutes } from './products.routing';
import { ViewComponent } from './view/view.component';
import { ViewInstancesComponent } from './viewInstances/viewInstances.component';
import { ViewCategoriesComponent } from './viewCategories/viewCategories.component';
import { CreateComponent } from './create/create.component';
import { EditInstancesComponent } from './editInstances/editInstances.component';
import { EditCategoriesComponent } from './editCategories/editCategories.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductsRoutes),
    FormsModule,
    RlTagInputModule,
    NgbCollapseModule,
    DateTimePickerModule,
    TagInputModule
  ],
  declarations: [ViewComponent, ViewInstancesComponent, ViewCategoriesComponent, EditInstancesComponent, EditCategoriesComponent, CreateComponent]
})

export class ProductsModule {}
