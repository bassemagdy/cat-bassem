import { Component, Input, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { SecurityService } from '../../services/security.service';

// import {RlTagInputModule} from 'angular2-tag-input';

@Component({
  selector: 'app-viewProductCategories',
  templateUrl: './viewCategories.component.html',
  styleUrls: ['./viewCategories.component.scss']
})
export class ViewCategoriesComponent implements OnInit { 

  data = [];

  constructor(
    private router: Router,
    private productService: ProductService,
    private modalService: NgbModal,
    public securityService: SecurityService
  ) {}

  ngOnInit() {

    this.productService.getAllCategories().subscribe((reply) => {
      console.log(reply);
      this.data = reply;
      for(var i = 0; i < this.data.length; i++){
        this.data[i].definition = this.data[i].definition.split(';');
        this.data[i].definition.pop();
        for(var j = 0; j < this.data[i].definition.length; j++)
        {
         this.data[i].definition[j] =  this.data[i].definition[j].replace(/:/g, " | ");
        }
      }

    })
  }

  new(id){
    this.router.navigate ( [ '/products/view/' + id ] );
  }

  edit(id){
    this.router.navigate ( [ '/products/editCategories/' + id ] );
  }

  delete(id){
    if(confirm("Are you sure you want to delete")) {
      this.productService.deleteCategory({id:id}).subscribe((reply) => {
        console.log(reply);
        this.ngOnInit();
      });
    }
  }
 
  create() {
    this.router.navigate ([ '/products/create']);
  }

}




