import { Routes } from '@angular/router';

import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { ViewInstancesComponent } from './viewInstances/viewInstances.component';
import { ViewCategoriesComponent } from './viewCategories/viewCategories.component';
import { EditInstancesComponent } from './editInstances/editInstances.component';
import { EditCategoriesComponent } from './editCategories/editCategories.component';

export const ProductsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'PRODUCTS'
      }
    }, {
      path: 'view/:name',
      component: ViewComponent,
      data: {
        heading: 'View'
      }
    }, {
      path: 'viewInstances',
      component: ViewInstancesComponent,
      data: {
        heading: 'View All Instances'
      }
    }, {
      path: 'viewCategories',
      component: ViewCategoriesComponent,
      data: {
        heading: 'View All Categories'
      }
    }, {
      path: 'editInstances/:name',
      component: EditInstancesComponent,
      data: {
        heading: 'Edit Instance'
      }
    }, {
      path: 'editCategories/:name',
      component: EditCategoriesComponent,
      data: {
        heading: 'Edit Category'
      }
    }
    
  ]
  }
];
