import {Router, ActivatedRoute, Params} from '@angular/router';
import {OnInit, Component} from '@angular/core';
import { ProductService } from '../../services/product.service';
import { FileUploader } from 'ng2-file-upload';
import {
  NgbModal,
  NgbActiveModal,
  ModalDismissReasons,
  NgbTimeStruct
} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit{
  
  id;
  product;
  details;
  name;
  fields = [];
  fieldTypes = [];
  multivalued = [];
  input = [];
  output = [];
  date;
  alert: alert = {
      visible: false,
      type: "",
      message: ""
    };
  
 constructor(private router: Router, private activatedRoute: ActivatedRoute, public productService:ProductService) {}

  ngOnInit() {
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['name'];
      });
    this.productService.getCategory({id: this.id}).subscribe((reply) => {
      console.log(reply);
      this.product = reply[0].name;
      var definition = reply[0].definition;
      var seperateFields = definition.split(";");
      for(var i = 0; i < seperateFields.length - 1; i++)
      {
          var seperateValues = seperateFields[i].split(":");
          this.fields.push(seperateValues[0]);
          this.fieldTypes.push(seperateValues[1]);
          if(seperateValues.length > 2)
          {
            var values = seperateValues[2].split(',');
            this.multivalued.push(values);
          }            
          else
            this.multivalued.push([]);

      }
    })
  }
  back(){
    this.router.navigate  ( [ '/products/viewCategories'] );
  }


  done(){
    console.log(this.date)
    this.output = [];
    for(var i = 0; i < this.input.length; i++)
    {
      this.output.push(this.fields[i] + ':' + this.input[i]);
    }
    console.log(this.output);
    this.productService.addProductInstance({ name: this.name, id: this.id, data: this.output }).subscribe((reply) => {
      this.router.navigate ( [ '/products/viewInstances'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    });
      
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}
