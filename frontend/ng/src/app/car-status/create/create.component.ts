import { Component, Input } from '@angular/core';
import { StatusService } from '../../services/car-status.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
  selector: 'app-status',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  
  status = {
    name: '',

  };
  
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };

  constructor(private router: Router, private cookie: CookieService, private statusService: StatusService, private modalService: NgbModal) {
 
  }
back(){
  this.router.navigate( [ '/status']);


}



  done() {
    console.log(this.status);
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };
    this.statusService.create({ status: this.status
    }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/status'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}





