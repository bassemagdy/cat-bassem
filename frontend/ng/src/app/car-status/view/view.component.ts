import { Component, Input, OnInit } from '@angular/core';
import { StatusService } from '../../services/car-status.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-view-status',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
  status={
    name:'',
    id:'',
  };

  constructor(private router: Router,private activatedRoute: ActivatedRoute, private cookie: CookieService, private statusService: StatusService) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });
}

  ngOnInit() {
     this.statusService.view({id: this.id}).subscribe((reply) => {
     console.log(this.id);
      console.log(reply);
      this.status = reply;
      console.log(this.status);
    });
  }

  editType() {
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/status'] );
  }


  done() {
    console.log(this.status);
    this.statusService.edit({ name: this.status.name,id:this.status.id }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/status'] );
    }, (reply) => {
      console.log(reply)
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




