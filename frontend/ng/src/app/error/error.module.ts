import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ErrorRoutes } from './error.routing';
import { Error4Component } from './error4/error4.component';
import { Error401Component } from './error401/error401.component';
import { Error5Component } from './error5/error5.component';
import { Error503Component } from './error503/error503.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ErrorRoutes)
  ],
  declarations: [Error4Component, Error5Component, Error503Component, Error401Component]
})

export class ErrorModule {}
