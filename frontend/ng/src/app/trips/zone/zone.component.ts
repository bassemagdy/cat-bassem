import {Component, Input, OnInit ,OnChanges} from '@angular/core';
// import {TripsService} from '../../services/trip.service';
import {ZonesService} from '../../services/zones.service';
import {Router} from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { SecurityService } from '../../services/security.service';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
// import * as firebase from 'firebase';
import {} from '@types/googlemaps';





@Component({

    selector: 'app-trips-zone',
     templateUrl: './zone.component.html',
     styleUrls: ['./zone.component.scss']

})


export class ZoneComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  x;
  limit;
  myid;
  map;
  google:any;
  title: string = 'My first AGM project';
  lat: number = 30.0326867;
  lng: number = 31.2380511;
  latLng;
  values;
  last;
  last2;
  last3;
  zone_name;
  zone_id;
  color;
  coloredNow;
 constructor(
    private router: Router,
    public modalService: NgbModal,
    public securityService: SecurityService,
    public cookie: CookieService,
    public ZonesService:ZonesService
    
  ) {
    this.coloredNow=[];
    this.ZonesService.index().subscribe((reply)=>{
    for(let i = 0; i < reply.data.length; i++){
      this.coloredNow.push({
      id: reply.data[i].id,
      color: "black",
      // resizeable: true
  });
}
});
// console.log('this.coloredNow');

// console.log(this.coloredNow);
    this.myid = cookie.get('id');
  }

  ngOnInit() {
    this.ZonesService.index().subscribe((reply)=>{
      console.log('reply');
      console.log(reply.data);
      this.last=[];
      this.last3=[];
      this.zone_name=[];
      this.zone_id=[];
      // this.color=[];

         for(let x=0;x<reply.data.length;x++){
          this.last2=[];
          console.log(reply.data[x]);
          this.zone_name.push(reply.data[x].name);
          this.zone_id.push(reply.data[x].id);
          // this.color.push(reply.data[x].color);

        this.values = reply.data[x].latLng;
        let n = this.values.split(';');
       for(let i=0; i<n.length ; i++){
          this.last[i] = n[i].split(',');
       }
     
       for(let j = 0; j < this.last.length ; j++){
        let p= [parseFloat(this.last[j][0]),parseFloat(this.last[j][1])];
           this.last2.push(p) ;
        }
        this.last3.push(this.last2.slice(0, n.length));
    }
    console.log('this.last3');
    console.log(this.last3);

    console.log('zone name');
    console.log(this.zone_name);

    // console.log('color');
    // console.log(this.color);


    });
}


delete(id) {
  if( confirm('Are you sure you want to delete') ) {
    this.spinner = 1;
    this.ZonesService.delete({id: id}).subscribe((reply) => {
      this.ngOnInit();
      this.spinner = 0;
    });
  }
}

colorPoly(event){
  for(let i = 0; i < this.coloredNow.length; i++){
    if(this.coloredNow[i].id == event){
        this.coloredNow[i].color = "red";
    }else{
      this.coloredNow[i].color = "black";
    }
  }

}




   }
