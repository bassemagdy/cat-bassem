import {Component, Input, OnInit ,OnChanges} from '@angular/core';
import {TripsService} from '../../services/trip.service';
import {Router} from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { SecurityService } from '../../services/security.service';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import * as firebase from 'firebase';
import {} from '@types/googlemaps';

@Component({

    selector: 'app-trips-viewAll',
    templateUrl: './viewall.component.html',
    styleUrls: ['./viewall.component.scss']

})


export class ViewAllComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  x;
  limit;
  myid;
map;
 markersArrSnap;
 google:any;
dbRefZ = firebase.database().ref('map/0');
markers;
 title: string = 'My first AGM project';
 lat: number = 30.0326867;
 lng: number = 31.2380511;


 constructor(
    private router: Router,
    private tripsService: TripsService,
    public modalService: NgbModal,
    public securityService: SecurityService,
    public cookie: CookieService
  ) {
    
    this.myid = cookie.get('id');
  }

  ngOnInit() {
    
    this.tripsService.index().subscribe((reply) => {
        console.log(reply);
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count;
      
    });
   
   }

  onPager(event) {

      if(!isNaN(event))
      {
        if (((Math.floor(event / this.pageSize)) != this.group)) {
        this.tripsService.paginate({
          "page": event,
          "columns": this.headersSearch,
          "keyword": this.keyword,
          "column": this.selectedHeader,
          "order": this.order
        }).subscribe((reply) => {

          this.group = event % this.pageSize;
          this.data = reply;
          this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);

        });
      }
      else
        this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);
      }
      else
        this.page = 1;
  }

  onChosenLocation(event){

    console.log(event);
  }


  cancelTrip(id){
    if(confirm('are you sure you want to canel?')){
    if(id){
      let check = "cancelled"
      this.tripsService.cancelTrip({check:check,id:id }).subscribe((reply)=>{ 
        if(reply){
          console.log(reply)
          this.ngOnInit() ;      
        }else{
          alert('something went wrong');
        }
      })
    }
  }
  
}
undoTrip(id){
  
  if(id){
    if(confirm('are you sure you want to undo?')){

    let check = "running"
  
    this.tripsService.cancelTrip({check:check,id:id }).subscribe((reply)=>{ 
      if(reply){
        console.log(reply)
        location.reload();          
      }else{
        alert('something went wrong');
      }
    });
  }
    
}
}
  sort(column) {
    if (this.selectedHeader == column && this.order == 'asc') {
      this.order = 'desc';
      this.asc = 0;
    } else if (this.selectedHeader == column && this.order == 'desc') {
      this.order = 'asc';
      this.asc = 1;
    } else
      this.selectedHeader = column;

    this.spinner = 1;
    this.tripsService.sort({
      'column': column,
      'order': this.order,
      'keyword': this.keyword,
      'columns': this.headersSearch
    }).subscribe((reply) => {
      this.data = reply;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.spinner = 0;
    });
   }
  
  

 
}
