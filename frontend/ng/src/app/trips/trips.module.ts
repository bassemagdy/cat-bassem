
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RlTagInputModule } from 'angular2-tag-input';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { TagInputModule } from 'ngx-chips';
import {  NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { TripsRoutes } from './trips.routing';
import { ViewAllComponent } from './viewall/viewall.component';
import { AgmCoreModule } from '@agm/core';
import { BookingComponent } from './bookTrip/booking.component';
import { ZoneComponent } from './zone/zone.component';
import { AddzoneComponent } from './addzone/addzone.component';
import { AirportComponent } from './airport/airport.component';
import { CarsMapComponent } from './cars-map/cars-map.component';
// import { CarsMapComponent } from './cars-map/cars-map.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TripsRoutes),
    FormsModule,
    RlTagInputModule,
    NgbCollapseModule,
    TagInputModule,
    ReactiveFormsModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBHLjuOnHBSw1Gce39rmbYzmFqlTQbaefQ',
      libraries: ["places"]
    })
  ],
  declarations: [ViewAllComponent,BookingComponent,ZoneComponent, AddzoneComponent, AirportComponent, CarsMapComponent]
   

})

export class TripsModule {}
