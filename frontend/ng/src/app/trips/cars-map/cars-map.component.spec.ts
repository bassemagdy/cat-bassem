import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarsMapComponent } from './cars-map.component';

describe('CarsMapComponent', () => {
  let component: CarsMapComponent;
  let fixture: ComponentFixture<CarsMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarsMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
