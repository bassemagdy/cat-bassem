import { Routes } from '@angular/router';
import { ViewAllComponent } from './viewall/viewall.component';
import { BookingComponent } from './bookTrip/booking.component';
import { ZoneComponent } from './zone/zone.component';
import { AddzoneComponent } from './addzone/addzone.component';
import { AirportComponent } from './airport/airport.component';
import { CarsMapComponent } from './cars-map/cars-map.component';



export const TripsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'trips',
      component: ViewAllComponent,
      data: {
        heading: 'View All Trips'
      }
    },
    {
      path: 'booktrip',
      component: BookingComponent,
      data: {
        heading: 'Book a Trip'
      }
    },
    {
      path: 'zone',
      component: ZoneComponent,
      data: {
        heading: 'Zones'
      }
    },
    {
      path: 'addzone',
      component: AddzoneComponent,
      data: {
        heading: 'Create a Zone'
      }
    },
    {
      path: 'airport',
      component: AirportComponent,
      data: {
        heading: 'Airport'
      }
    },
    {
      path: 'cars-map',
      component: CarsMapComponent,
      data: {
        heading: 'Cars Map'
      }
    }

  ]
  }
];