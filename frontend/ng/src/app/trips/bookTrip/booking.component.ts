import { Component,ElementRef,NgZone } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgModule, ViewChild, ViewContainerRef} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule, Validators, FormBuilder, FormGroup,FormControl} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {OnInit, Input, Renderer2, Pipe, PipeTransform,OnChanges}  from '@angular/core';
import {DataService} from '../../services/data.service';
import {DragulaService} from 'ng2-dragula/ng2-dragula';
import { CatServiceService } from '../../services/cat-service.service';
import { ProductService } from '../../services/product.service';
import { LeadService } from '../../services/lead.service';
import { DepartmentService } from '../../services/department.service';
import { ScrollToService, ScrollToConfig } from '@nicky-lenaers/ngx-scroll-to';
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import {TripsService} from '../../services/trip.service';
import { AgmCoreModule,MapsAPILoader } from '@agm/core';
import * as firebase from 'firebase';
import {} from '@types/googlemaps';
import { ResourcesService } from '../../services/resources.service';
import {ZonesService} from '../../services/zones.service';
import { ZonepricesService } from '../../services/zones-prices.service';
import { CarTypesService } from '../../services/car-types.service';

// import { MapsAPILoader } from 'angular2-google-maps/core';

@Component({
  selector: 'app-bookTrip',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss']
})
export class BookingComponent {
  

  sources = [];

  booking = {
    car_type_id:0,
    AddressFrom:'',
    AddressTo:'',
    trip_time:'',
    trip_date:'',
    lng:'',
    lat:'',
    latToo:'',
    lngToo:'',
    fromConcat:'',
    toConcat:'',
    land_mark:'',
    name:'',
    email:'',
    phone:'',
    numOfBags:0,
    numOfPassengers:0,
    car_type:'',
    source:'',
    flightNumber:'',
    zoneVarFrom:'',
    zoneVarTo:'',
    price:'',
    chosenZoneFromId:'',
    chosenZoneToId:'',
  };
  markers;
  alert: alert = {
    visible: false,
    type: '',
    message: ''
  };


title: string = 'My first AGM project';
lat = 30.0326867;
lng = 31.2380511;
latTo=30.0326867;
lngTo =31.2380511;
latFrom=30.0326867;
lngFrom =31.2380511;
stringA: string = "airport";
stringB: string = "مطار";
latLng;
values;
last;
last2;
last3;
zone_name;
zone_id;
showprice = false;
newprice;
newpricepeak;
pickedAddressFrom;
pickedAddressTo;
addressOptionsObjectFrom;
addressOptionsObjectArrayFrom;
addressesOptionsFrom=[];
addressOptionsObjectTo;
addressOptionsObjectArrayTo;
addressesOptionsTo=[];
booleanNumber = 0;
carTypes=[];
carTypesBig=[];

public latitude: number;
public longitude: number;
public searchControl: FormControl;
public zoom: number;

public searchControl2: FormControl;
public zoom2: number;
@ViewChild("search")
public searchElementRef: ElementRef;

@ViewChild("search2")
public searchElementRef2: ElementRef;


    constructor(public leadService: LeadService,
                public cookie: CookieService, 
                public productService: ProductService,
                private activatedRoute: ActivatedRoute,  
                public modalService: NgbModal,
                private router: Router,
                private tripsService: TripsService,
                public resourcesService : ResourcesService,
                public ZonesService:ZonesService,
                public ZonepricesService:ZonepricesService,
                public carTypesService: CarTypesService,
                private mapsAPILoader: MapsAPILoader,
                private ngZone: NgZone
              ) {
       
       
        this.activatedRoute.params.subscribe((params: Params) => {
         // console.log(this.id);
      });
       
       
  
      this.resourcesService.index().subscribe((reply) => {
        this.sources = reply.data;
        console.log(reply.data);
      });    
      
      this.carTypesService.index().subscribe((reply) => {
        this.carTypes = reply.data;
        console.log(reply.data);
        for(let i =0;i<reply.data.length;i++){
          if(reply.data[i].NumberOfBags>3 || reply.data[i].NumberOfPassengers>3){
          this.carTypesBig.push(reply.data[i]);
          console.log(reply.data[i]);
        }
        }
        console.log(this.carTypesBig)

      });


    }

    ngOnInit() {

    //set google maps defaults
    this.zoom = 7;
    this.zoom2=7;

    //create search FormControl
    this.searchControl = new FormControl();
    this.searchControl2 = new FormControl();
    
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["establishment", "geocode"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
  
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          
          //set latitude, longitude and zoom
          this.latFrom = place.geometry.location.lat();
          this.lngFrom = place.geometry.location.lng();
          this.zoom = 15;
        });
      });
    });
    
    this.mapsAPILoader.load().then(() => {
      let autocomplete2 = new google.maps.places.Autocomplete(this.searchElementRef2.nativeElement, {
        types: ["establishment", "geocode"]
      });
      autocomplete2.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place2: google.maps.places.PlaceResult = autocomplete2.getPlace();
  
          //verify result
          if (place2.geometry === undefined || place2.geometry === null) {
            return;
          }
          
          //set latitude, longitude and zoom
          this.latTo = place2.geometry.location.lat();
          this.lngTo = place2.geometry.location.lng();
          this.zoom2 = 15;
        });
      });
    });


    this.ZonesService.index().subscribe((reply)=>{
      this.last=[];
      this.last3=[];
      this.zone_name=[];
      this.zone_id=[];

         for(let x=0;x<reply.data.length;x++){
          this.last2=[];
          console.log(reply.data[x]);
          this.zone_name.push(reply.data[x].name);
          this.zone_id.push(reply.data[x].id);

        this.values = reply.data[x].latLng;
        let n = this.values.split(';');
       for(let i=0; i<n.length ; i++){
          this.last[i] = n[i].split(',');
       }
     
       for(let j = 0; j < this.last.length ; j++){
        let p= [parseFloat(this.last[j][0]),parseFloat(this.last[j][1])];
           this.last2.push(p) ;
        }
        this.last3.push(this.last2.slice(0, n.length));
    }
    console.log('this.last3');
    console.log(this.last3);

    console.log('zone name');
    console.log(this.zone_name);
    });
}

// showDropDown(){

// }
// pickingAddressFrom(event){
//   // console.log(event);
//   console.log(this.pickedAddressFrom);
  
//   console.log(this.addressOptionsObjectArrayFrom);
//   console.log(this.addressesOptionsFrom);
//   console.log(this.pickedAddressFrom);
 
//   for(let i=0;i<this.addressOptionsObjectArrayFrom.length;i++){
//       if(this.addressOptionsObjectArrayFrom[i].address===this.pickedAddressFrom){
//         this.latFrom= this.addressOptionsObjectArrayFrom[i].lat;
//         this.lngFrom= this.addressOptionsObjectArrayFrom[i].lng;
//         this.booking.AddressFrom=this.pickedAddressFrom;
//         console.log(this.latFrom);
//         console.log(this.lngFrom);
//         console.log(this.booking.AddressFrom);

//       }
//     }
// }
// pickingAddressTo(event){
//   console.log(this.pickedAddressTo);
//   console.log(this.addressOptionsObjectArrayTo);
//   console.log(this.addressesOptionsTo);
//   console.log(this.addressOptionsObjectArrayTo);
//   console.log(this.addressOptionsObjectArrayTo.length);
//   for(let i=0;i<this.addressOptionsObjectArrayTo.length;i++){
//     console.log(this.pickedAddressTo);
//     console.log(this.addressOptionsObjectArrayTo[i].address);
//     console.log(this.pickedAddressTo);
//     // console.log()
//       if(this.addressOptionsObjectArrayTo[i].address===this.pickedAddressTo){
//       this.latTo= this.addressOptionsObjectArrayTo[i].lat;
//       this.lngTo= this.addressOptionsObjectArrayTo[i].lng;
//       console.log(this.latTo);
//       console.log(this.lngTo);
//       this.booking.AddressTo=this.pickedAddressTo;
//       console.log(this.booking.AddressTo);

//     }}
// }
// geo(x){
//   if (x=='from'){
//     this.addressOptionsObjectFrom={};
//     this.addressesOptionsFrom=[];
//     this.addressOptionsObjectArrayFrom=[]
//     this.tripsService.geo({from : this.booking.AddressFrom }).subscribe((reply)=>{
//       if(reply=="null"){
//         this.alert = {
//           visible: true,
//           type: "danger",
//           message: "address not found"
//         }
//         console.log('no reply');
//       }else{
//     console.log(reply);
//         if(reply.length>0){
//     for(let i=0; i<reply.length;i++){
//       let addressNow=[];
//       this.addressOptionsObjectFrom={};
//       console.log(reply[i]);
//       this.addressOptionsObjectFrom.id=reply[i].id;
//       this.addressOptionsObjectFrom.lat=reply[i].latitude;
//       this.addressOptionsObjectFrom.lng=reply[i].longitude;
      
//       this.tripsService.getAddress({ lat :reply[i].latitude , lng :reply[i].longitude }).subscribe((replyAdd) => {    
//         if (reply==null){
//           console.log('Address not found');
//         }
//       if(replyAdd.length>0){
//       addressNow[0]=replyAdd;
//       console.log(addressNow);
//       this.addressOptionsObjectFrom.address=addressNow[0];
//       console.log('reply address first get address');
//       console.log(replyAdd);

//       this.addressesOptionsFrom.push(replyAdd);
//       console.log(this.addressesOptionsFrom);
//       this.addressOptionsObjectArrayFrom[i].address =this.addressOptionsObjectFrom.address;

//     }else{
//           console.log('reply Add fadya');
//           console.log(replyAdd.length);
//         }

//     });
//     this.addressOptionsObjectArrayFrom.push(this.addressOptionsObjectFrom);
//     }}else{
//       this.addressOptionsObjectFrom={};
//       this.tripsService.getAddress({ lat :reply.latitude , lng :reply.longitude }).subscribe((replyAdd) => {    
//         if (reply==null){
//           console.log('Address not found');
//         }
//       console.log('reply ADDRESS 241');
//       console.log(replyAdd);
//       if(replyAdd.length>0){
//       this.addressesOptionsFrom.push(replyAdd);
//       this.addressOptionsObjectFrom.id=reply.id;
//       this.addressOptionsObjectFrom.address=replyAdd;
//       console.log(this.addressOptionsObjectFrom.address);
//       this.addressOptionsObjectFrom.lat=reply.latitude;
//       this.addressOptionsObjectFrom.lng=reply.longitude;
//       this.addressOptionsObjectArrayFrom.push(this.addressOptionsObjectFrom);
//     }
//     });
//     }

//   }});
// }else{
//     this.addressOptionsObjectTo={};
//     this.addressesOptionsTo=[];
//     this.addressOptionsObjectArrayTo=[]
//     this.tripsService.geo({from : this.booking.AddressTo }).subscribe((reply)=>{
//       if(reply=="null"){
//         this.alert = {
//           visible: true,
//           type: "danger",
//           message: "address not found"
//         }
//         console.log('no reply');
//       }else{
//     console.log(reply);
//   if(reply.length>0){
//     for(let i=0; i<reply.length;i++){
//       let addressNow=[];
//       this.addressOptionsObjectTo={};
//       console.log(reply[i]);
//       this.addressOptionsObjectTo.id=reply[i].id;
//       this.addressOptionsObjectTo.lat=reply[i].latitude;
//       this.addressOptionsObjectTo.lng=reply[i].longitude;
      
//       this.tripsService.getAddress({ lat :reply[i].latitude , lng :reply[i].longitude }).subscribe((replyAdd) => {    
//         if (reply==null){
//           console.log('Address not found');
//         }
//         if(replyAdd.length>0){
//           addressNow[0]=replyAdd;
//       this.addressOptionsObjectTo.address=addressNow[0];
//       console.log('reply address first get address');
//       console.log(replyAdd);
//       this.addressesOptionsTo.push(replyAdd);
//       console.log(this.addressesOptionsTo);
//       this.addressOptionsObjectArrayTo[i].address =this.addressOptionsObjectTo.address;

//     }else{
//           console.log('reply Add fadya');
//           console.log(replyAdd.length);
//         }

//     });
//     this.addressOptionsObjectArrayTo.push(this.addressOptionsObjectTo);
//     }}else{
//       this.addressOptionsObjectTo={};
//       this.tripsService.getAddress({ lat :reply.latitude , lng :reply.longitude }).subscribe((replyAdd) => {    
//         if (reply==null){
//           console.log('Address not found');
//         }
//       console.log('reply ADDRESS 241');
//       console.log(replyAdd);
//       if(replyAdd.length>0){
//       this.addressesOptionsTo.push(replyAdd);
//       this.addressOptionsObjectTo.id=reply.id;
//       this.addressOptionsObjectTo.address=replyAdd;
//       console.log(this.addressOptionsObjectTo.address);
//       this.addressOptionsObjectTo.lat=reply.latitude;
//       this.addressOptionsObjectTo.lng=reply.longitude;
//       console.log('line 359');
//       console.log( this.addressOptionsObjectTo.lat);
//       console.log( this.addressOptionsObjectTo.lng)
//       this.addressOptionsObjectArrayTo.push(this.addressOptionsObjectTo);
//     }
//     });
//     }
//   }});
//   }

// }



// private setCurrentPosition() {
//   if ("geolocation" in navigator) {
//     navigator.geolocation.getCurrentPosition((position) => {
//       this.latitude = position.coords.latitude;
//       this.longitude = position.coords.longitude;
//       this.zoom = 12;
//     });
//   }
// }



onChoseLocation(event){////TO

  if (event==null){
    return ;
  }
 // console.log(event.coords);
  this.latTo=event.coords.lat;
  this.lngTo = event.coords.lng;
  for(let i =0;i<this.last3.length;i++){
    let arrLat=[];
    let arrLng=[];
      for(let j=0;j<this.last3[i].length;j++){
        arrLat.push(this.last3[i][j][0]);
        arrLng.push(this.last3[i][j][1]);
      }
      
      let maxLat = this.arrayMax(arrLat);
      let minLat= this.arrayMin(arrLat);
      let maxLng= this.arrayMax(arrLng);
      let minLng = this.arrayMin(arrLng);
    

      if(!(event.coords.lat < minLat || event.coords.lat > maxLat || event.coords.lng < minLng ||event.coords.lng > maxLng)) {
        let bool= this.insidePoly(this.last3[i],event.coords.lat,event.coords.lng);
        console.log(bool);
        if(bool){
          this.booking.zoneVarTo=this.zone_name[i]
          this.booking.chosenZoneToId=this.zone_id[i];
        break;
      }
      } else {
        this.booking.zoneVarTo=null;
      }

  }
  if(this.booking.zoneVarTo == null){
        console.log('Marker out of zones');
  }else{
    console.log('Marker is inside ' + this.booking.zoneVarTo);
      
    if(this.booking.chosenZoneToId !==null && this.booking.chosenZoneFromId!==null){
      this.ZonepricesService.check({from_id:this.booking.chosenZoneFromId, to_id:this.booking.chosenZoneToId}).subscribe((reply)=>{
        if(!reply){
          console.log('no price in db')

        }else{
        console.log(reply);
        this.newprice = reply[0].price;
        this.newpricepeak=reply[0].peak_hour_price;
        this.showprice = true;
      }})

    }

      }



  this.tripsService.getAddress({ lat :this.latTo , lng :this.lngTo }).subscribe((reply) => {

    // console

    if (!reply){
      // alert('Address not found');
      this.booking.AddressTo = " ";
    }
    this.booking.AddressTo = reply;
    
});



}

onChosenLocation(event){ /////from
     if (event==null){
       return ;
     }

   
      console.log('event.coords');
      console.log(event.coords);

      this.latFrom=event.coords.lat;
      this.lngFrom = event.coords.lng;
      // this.latitude=event.coords.lat;
      // this.longitude = event.coords.lng;      // this.latlngBounds = {lat:event.coords.lat,lng:event.coords.lng};
      for(let i =0;i<this.last3.length;i++){
        let arrLat=[];
        let arrLng=[];
          for(let j=0;j<this.last3[i].length;j++){
            arrLat.push(this.last3[i][j][0]);
            arrLng.push(this.last3[i][j][1]);
          }
          
          let maxLat = this.arrayMax(arrLat);
          let minLat= this.arrayMin(arrLat);
          let maxLng= this.arrayMax(arrLng);
          let minLng = this.arrayMin(arrLng);
        

          if(!(event.coords.lat < minLat || event.coords.lat > maxLat || event.coords.lng < minLng ||event.coords.lng > maxLng)) {
            let bool= this.insidePoly(this.last3[i],event.coords.lat,event.coords.lng);
            console.log(bool);
            if(bool){
            this.booking.zoneVarFrom=this.zone_name[i];
            this.booking.chosenZoneFromId=this.zone_id[i];

            break;
          }
          } else {
            this.booking.zoneVarFrom=null;
          }
  
      }
      if(this.booking.zoneVarFrom == null){
            console.log('Marker out of zones');
      }else{
        console.log('Marker is inside' + this.booking.zoneVarFrom);

          }
      this.tripsService.getAddress({ lat :this.latFrom , lng :this.lngFrom }).subscribe((reply) => {
        if (!reply){
          // alert('Address not found');
          this.booking.AddressFrom = " ";

        }
        this.booking.AddressFrom = reply;

    });



}

insidePoly(poly, pointx, pointy) {
  let i, j;
  let inside = false;
  for (i = 0, j = poly.length - 1; i < poly.length; j = i++) {
      if(((poly[i][1] > pointy) != (poly[j][1] > pointy)) && (pointx < (poly[j][0]-poly[i][0]) * (pointy-poly[i][1]) / (poly[j][1]-poly[i][1]) + poly[i][0]) ) inside = !inside;
  }
  return inside;
}
arrayMin(arr) {
  return arr.reduce(function (p, v) {
    return ( p < v ? p : v );
  });
}

arrayMax(arr) {
  return arr.reduce(function (p, v) {
    return ( p > v ? p : v );
  });
}
convertDigitIn(str){
  let string="";
  string+=str.split('-')[2]+"-"+str.split('-')[1]+"-"+str.split('-')[0]
  return string;
}
done(){
  this.booking.lat = this.latFrom+"";
  this.booking.lng = this.lngFrom+"";
  this.booking.fromConcat = this.latFrom+","+this.lngFrom;
  this.booking.latToo = this.latTo+"";
  this.booking.lngToo= this.lngTo+"";
  this.booking.toConcat = this.latTo+ "," +this.lngTo;
  if(!this.booking.flightNumber && ((this.booking.AddressTo.indexOf(this.stringB)==-1 || this.booking.AddressTo.toUpperCase().indexOf(this.stringA)==-1 )&&( this.booking.AddressFrom.indexOf(this.stringB)==-1 || this.booking.AddressFrom.toUpperCase().indexOf(this.stringA)==-1))){
    this.booking.flightNumber='---';
    // return;
  }
  console.log(this.booking.trip_time);
  
    
  if((this.booking.zoneVarFrom!==null && this.booking.zoneVarTo!==null)){
    console.log(this.booking.zoneVarFrom);
    console.log(this.booking.zoneVarTo);
  //  console.log(this.booking);  
  if(this.booking.trip_time.length ==0){
    this.alert = {
      visible: true,
      type: "danger",
      message: "Please Enter a valid time for the trip"
    }
  }
  else{
    if(Date.now()>Number(new Date(this.booking.trip_date+":"+this.booking.trip_time))){
      console.log(Date.now());
      console.log(Number(new Date(this.booking.trip_date+":"+this.booking.trip_time)));
      this.alert = {
        visible: true,
        type: "danger",
        message: "Please Enter a valid Date"
      }
      console.log('wrong date');
    }else{
      // console.log('this.convertDigitIn(this.booking.trip_date)');

      // this.convertDigitIn(this.booking.trip_date)

      this.tripsService.booking({ booking :this.booking}).subscribe((reply) => {
        if (reply=='false') {
          this.alert = {
           visible: true,
           type: "danger",
           message: "That car type is not available in the mean time"
         }
        }
        else{
          console.log('OKKKKKKKKKKK');
          let tripsRef = firebase.database().ref("/Trips");
          tripsRef.push().set({
            name: this.booking.name,
            car_id:reply,
            phone: this.booking.phone,
            email: this.booking.email,
            startTime: this.booking.trip_time,
            startDate: this.convertDigitIn(this.booking.trip_date),
            addressFrom: this.booking.AddressFrom,
            locationFrom: this.booking.fromConcat,
            addressTo: this.booking.AddressTo,
            locationTo: this.booking.toConcat,
            zoneFrom: this.booking.zoneVarFrom,
            zoneTo: this.booking.zoneVarTo,
            flightNumber: this.booking.flightNumber,
            price: this.newprice,
            peakPrice: this.newpricepeak,
            car_date:reply+"_"+this.convertDigitIn(this.booking.trip_date)
          });
          this.alert = {
            visible: true,
            type: "successful",
            message: "your car has been booked"
          }
          // this.router.navigate(['/trips/trips']);

    
    }});

     }
   

}
  
  }else{
    console.log(this.booking.trip_time);
    console.log('alert1!!!');
    this.alert = {
      visible: true,
      type: "danger",
      message: "Markers Out Of Zones"
    }
  }
}

}


interface alert {
  visible: boolean,
  type: string,
  message: string
}
