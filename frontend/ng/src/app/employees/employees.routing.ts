import { Routes } from '@angular/router';

import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewAll/viewAll.component';
import { ViewComponent } from './view/view.component';

export const EmployeesRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'Create employee'
      }
    },
    {
      path: '',
      component: ViewAllComponent,
      data: {
        heading: 'View All Employees'
      }
    },
    {
      path: 'view/:id',
      component: ViewComponent,
      data: {
        heading: 'View Employee'
      }
    }
  ]
  }
];
