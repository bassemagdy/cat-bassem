import { Component, Input, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { DepartmentService } from '../../services/department.service';
import { RoleService } from '../../services/role.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service'; 
// import {RlTagInputModule} from 'angular2-tag-input';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  
  id;
  employee = {role: '', department: ''};//to avoid silly console error
  roles;
  departments;
  managers;
  edit = false;
  alert: alert = {
      visible: false,
      type: "",
      message: ""
    };
  role;
  department;
 
  constructor(private router: Router,private activatedRoute: ActivatedRoute, private cookie: CookieService, private employeeService: EmployeeService, private departmentService: DepartmentService,private roleService: RoleService, private modalService: NgbModal) {
    
    this.activatedRoute.params.subscribe((params: Params) => {

        this.id = params['id'];
    });
    
  }

  ngOnInit(){
     this.employeeService.getEmployee({id: this.id}).subscribe((reply) => {
      console.log(reply);
      this.employee = reply;
      this.roleService.getData().subscribe((reply) => {
      console.log(reply);
      this.roles = reply.data;
      console.log(this.employee);
      for (var i = 0; i < this.roles.length; i++) {
        if (this.roles[i].id == this.employee.role) {
          this.role = this.roles[i].name;
        }
      }
      this.departmentService.index().subscribe((reply) => {
        this.departments = reply.data;
        for (var i = 0; i < this.departments.length; i++) {
          if (this.departments[i].id == this.employee.department) {
            this.department = this.departments[i].name;
          }
        }
      });
      this.employeeService.getData().subscribe((reply) => {
        console.log(reply);
        this.managers = reply.data;
      });
    });
    });
  }

  editEmployee(){
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/employees'] );
  }


  done(){
    console.log(this.employee);
    this.employeeService.editEmployee({ employee: this.employee}).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/employees'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    })
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




