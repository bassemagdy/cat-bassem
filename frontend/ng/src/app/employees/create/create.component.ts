import { Component, Input } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { DepartmentService } from '../../services/department.service';
import { RoleService } from '../../services/role.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
// import {RlTagInputModule} from 'angular2-tag-input';
import {SendpulseService} from '../../services/send-pulse.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  // name;
  // email;
  // username;
  // password;
  // address;
  // phone;
  // role;
  employee = {
    name: "",
    email: "",
    username: "",
    password: "",
    department: "",
    phone: "",
    role:  "",
    manager:""
  };
  roles;
  departments;
  managers;
  alert: alert = {
      visible: false,
      type: "",
      message: ""
    };
   root: RootObject = {
      html: "",
      text: "",
      subject: "",
      from: "",
      to: "",
      bcc: "",
  };
  end;
 
  constructor(private router: Router,private cookie: CookieService, private employeeService: EmployeeService, private departmentService: DepartmentService,private roleService: RoleService,private sendpulseService :SendpulseService, private modalService: NgbModal) {

    this.roleService.getData().subscribe((reply) => {
      console.log(reply);
      this.roles = reply.data;
      this.departmentService.index().subscribe((reply) => {
        console.log(reply);
        this.departments = reply.data;
      });
    });

    this.employeeService.getData().subscribe((reply) => {
      console.log(reply);
      this.managers = reply.data;
    });

  }

 back(){

  this.router.navigate ( [ '/employees'] );
  
 } 



  done(){
    console.log(this.employee);
   
    
   /*this.root = {
      html: "new",
      text: "hii ",
      subject: "sign",
      from: "info@cairoairport.travel",
      to: "mi2015do@hotmail.com",
      bcc: "no",
  };
  this.end = JSON.stringify(this.root);
  
 
  this.sendpulseService.send({ data: this.end }).subscribe((reply) => {
    
    console.log(reply); 
  })
    
    */
    this.alert = {
            visible: true,
            type: "info",
            message: "Please wait - this may take a while"
          }
    this.employeeService.createEmployee({ employee: this.employee,
                                          manager_id:this.cookie.get('id')  }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/employees'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    })
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}


  export interface From {
      name: string;
      email: string;
  }

  export interface To {
      name: string;
      email: string;
  }

  export interface Bcc {
      name: string;
      email: string;
  }

  interface RootObject {
      html: string;
      text: string;
      subject: string;
      from: string;
      to: string;
      bcc: string;
  }








