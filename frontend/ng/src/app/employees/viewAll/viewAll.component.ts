import {Component, Input, OnInit} from '@angular/core';
import {EmployeeService} from '../../services/employee.service';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { SecurityService } from '../../services/security.service';

@Component({
selector: 'app-employees-viewall',
templateUrl: './viewAll.component.html',
styleUrls: ['./viewAll.component.scss']
})
export class ViewAllComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;
  myid;



  constructor(
    private router: Router,
    private employeeService: EmployeeService,
    public modalService: NgbModal,
    public securityService: SecurityService,
    public cookie: CookieService
  ) {
    this.myid = cookie.get('id');
  }

  ngOnInit() {
    this.employeeService.getData().subscribe((reply) => {
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;
    });
  }




  onPager(event) {

      if(!isNaN(event))
      {
        if (((Math.floor(event / this.pageSize)) != this.group)) {
        this.employeeService.paginate({
          "page": event,
          "columns": this.headersSearch,
          "keyword": this.keyword,
          "column": this.selectedHeader,
          "order": this.order
        }).subscribe((reply) => {

          this.group = event % this.pageSize;
          this.data = reply;
          this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);

        });
      }
      else
        this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);
      }
      else
        this.page = 1;
  }

  search() {
    this.spinner = 1;
    this.employeeService.search({
      'columns': this.headersSearch,
      'keyword': this.keyword
    }).subscribe((reply) => {
      console.log(reply);
      this.data = reply.data;
      this.group = 0;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;
      console.log(reply.count.count);
      this.spinner = 0;
    })
  }

  sort(column) {
    if (this.selectedHeader == column && this.order == 'asc') {
      this.order = 'desc';
      this.asc = 0;
    } else if (this.selectedHeader == column && this.order == 'desc') {
      this.order = 'asc';
      this.asc = 1;
    } else
      this.selectedHeader = column;

    this.spinner = 1;
    this.employeeService.sort({
      'column': column,
      'order': this.order,
      'keyword': this.keyword,
      'columns': this.headersSearch
    }).subscribe((reply) => {
      this.data = reply;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.spinner = 0;
    });
  }

  open(employee) {
    this.router.navigate ( [ '/employees/view/' + employee.id] );
  }

  delete(id) {
    if (confirm('Are you sure you want to delete')) {
      this.spinner = 1;
      this.employeeService.deleteEmployee({id: id}).subscribe((reply) => {
        this.ngOnInit();
        this.spinner = 0;
      });
    }
  }

  create() {
    this.router.navigate ( [ '/employees/create'] );
  }
}
