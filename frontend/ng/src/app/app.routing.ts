import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthGuard } from './shared/guards/auth-guard.service'
// if(cookie.get("token") != null){
//   landing = './dashboard/dashboard.module#DashboardModule';
//   layout = AdminLayoutComponent
// }  
// else {
//   landing = './authentication/authentication.module#AuthenticationModule';
//   layout = AuthLayoutComponent
// }
export const AppRoutes: Routes =
[{
  path: '',
  component: AdminLayoutComponent,
  canActivate: [AuthGuard],
  children:
  [
    {
      path: '',
    loadChildren: './taskboard/taskboard.module#TaskboardModule' 
    //loadChildren: './app.module#AppModule'
    },
    {
      path: 'board',
      loadChildren: './taskboard/taskboard.module#TaskboardModule'
    },
    {
      path: 'products',
      loadChildren: './products/products.module#ProductsModule'
    },
    {
      path: 'leads',
      loadChildren: './leads/leads.module#LeadsModule'
    },
    {
      path: 'roles',
      loadChildren: './roles/roles.module#RolesModule'
    },
    {
      path: 'services',
      loadChildren: './cat-services/cat-services.module#CatServicesModule'
    },
    {
      path: 'employees',
      loadChildren: './employees/employees.module#EmployeesModule'
    },
    {
      path: 'departments',
      loadChildren: './departments/departments.module#DepartmentsModule'
    },
    {
      path: 'email',
      loadChildren: './email/email.module#EmailModule'
    },
    {
      path: 'pipeline',
      loadChildren: './taskboard/taskboard.module#TaskboardModule'
    },
    {
      path: 'resources',
      loadChildren: './resource/resources.module#ResourcesModule'
    },
    {
      path: 'stages',
      loadChildren: './stages/stages.module#StagesModule'
    },
    {
      path: 'logout',
      loadChildren: './logout/logout.module#LogOutModule'
    },
    {
      path: 'profile',
      loadChildren: './profile/profile.module#ProfileModule'
    },
    {
      path: 'payments',
      loadChildren: './payments/payments.module#PaymentsModule'
    },

    {
      path: 'customers',
      loadChildren: './customers/customers.module#CustomersModule'
    },
    {
      path: 'cars',
      loadChildren: './cars/cars.module#CarsModule'
    },
    {
      path: 'trips',
      loadChildren: './trips/trips.module#TripsModule'
    },

    {
      path: 'status',
      loadChildren: './car-status/carstatus.module#StatusModule'
    },

    {
      path: 'cartypes',
      loadChildren: './car-types/car-types.module#CarTypesModule'
    },

    {
      path: 'drivers',
      loadChildren: './drivers/drivers.module#DriversModule'
    } ,
    {
      path: 'trips/zone-prices',
      loadChildren: './zone-prices/zone-pricing.module#ZonespricesModule'
    },
    {
      path: 'settings',
      loadChildren: './settings/settings.module#SettingsModule'
    }


    
  ]
},
{
  path: '',
  component: AuthLayoutComponent,
  children:
  [
    {
      path: 'authentication',
      loadChildren: './authentication/authentication.module#AuthenticationModule'
    },
    {
      path: 'error',
      loadChildren: './error/error.module#ErrorModule'
    },
    {
      path: 'landing',
      loadChildren: './landing/landing.module#LandingModule'
    }
  ]
},
{
  path: '**',
  redirectTo: 'error/404'
}];

