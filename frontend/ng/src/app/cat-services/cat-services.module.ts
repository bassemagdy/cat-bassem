import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { RlTagInputModule } from 'angular2-tag-input';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';

import { TagInputModule } from 'ngx-chips';

import { CommonModule } from '@angular/common';
import { CatServicesRoutes } from './cat-services.routing';
import { ViewComponent } from './view/view.component';
import { ViewCategoriesComponent } from './viewCategories/viewCategories.component';
import { CreateComponent } from './create/create.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CatServicesRoutes),
    FormsModule,
    RlTagInputModule,
    NgbCollapseModule,
    TagInputModule
  ],
  declarations: [ViewComponent, ViewCategoriesComponent, CreateComponent]
})

export class CatServicesModule {}
