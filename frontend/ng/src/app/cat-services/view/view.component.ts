import {Router, ActivatedRoute, Params} from '@angular/router';
import {OnInit, Component} from '@angular/core';
import { CatServiceService } from '../../services/cat-service.service';
import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit{
  
  id;
  product;
  name;
  fields = [];
  fieldTypes = [];
  multivalued = [];
  input = [];
  output = [];
  data = [];
  i;
  alert: alert = {
    visible: false,
    type: "",
    message: ""
  };
  
 constructor(private router: Router, private activatedRoute: ActivatedRoute, public catServiceService:CatServiceService) {}

  ngOnInit() {
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['name'];
      });

    this.catServiceService.getCategory({id: this.id}).subscribe((reply) => {
      console.log(reply);
      this.name = reply[0].name;
      var definition = reply[0].definition;
      var seperateFields = definition.split(";");
      for(this.i = 0; this.i < seperateFields.length - 1; this.i++)
      {
          var seperateValues = seperateFields[this.i].split(":");
          this.data[this.i] = [];
          this.data[this.i][0] = seperateValues[0];
          this.data[this.i][1] = seperateValues[1];

          if(seperateValues.length > 2)
          {
            var values = seperateValues[2].split(',');
            this.data[this.i][2] = values;
          }  
      }
    })
  }

  showAdd(){
    this.data[this.i] = [];
    this.i++;
    console.log(this.i);
  }

  delete(j) {
    this.data.splice(j, 1);
    this.i--;
    console.log(this.i);
  }

  multiple(j){
    if(this.data[j][1] == 'select')
      this.data[j][2] = [];
    
  }

  back(){
    this.router.navigate  ( [ '/services'] );
  }

  done(){
    
    console.log(this.data);
    this.catServiceService.editServiceCategory({ id: this.id, name: this.name, data : this.data}).subscribe((reply) => {
      this.router.navigate ( [ '/services'] );
    }, (reply) => {
       this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }
    })
   
      
  }


}


interface alert {
  visible: boolean,
  type: string,
  message: string
}
