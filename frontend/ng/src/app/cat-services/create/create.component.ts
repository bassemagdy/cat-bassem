import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { CatServiceService } from '../../services/cat-service.service';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
// import {RlTagInputModule} from 'angular2-tag-input';
import {NgModule} from '@angular/core'
import {FormsModule} from '@angular/forms'
import { Validators } from '@angular/forms';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',  
  styleUrls: ['./create.component.scss']
})
export class CreateComponent { 

  add = [];
  data = [];
  name ="";
 alert: alert = {
    visible: false,
    type: "",
    message: ""
  };
 
  constructor(private router: Router, private catServiceService: CatServiceService, private modalService: NgbModal) {

     this.alert = {
      visible: true,
      type: "info",
      message: " price field exists by default"
    };

  }

  showAdd(){
    this.add.push(0);
    this.data[this.add.length-1] = [];

  }

  clearAll(idx){
    this.data[idx][2] = [];
  }

  delete(j) {
    this.add.pop();
     this.data.splice(j,1);
   }

  dismissAlert() {
    this.alert = {
      visible: false,
      type: "",
      message: ""
    };
  }

  multiple(i){
    console.log(i);
    if(this.data[i][1] == 'select')
      this.data[i][2] = [];
    
  }
  back(){
    this.router.navigate  ( [ '/services'] );
  }
  done(){
if (this.name.length==0){
  console.log("mokh");
  this.alert = {
    visible: true,
    type: "danger",
    message: "Something went wrong. Please try again"
  }
  return;
  
  
}
    this.catServiceService.createService({ name: this.name,
                                        data: this.data }).subscribe((reply) => {
      this.router.navigate ( [ '/services'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    });
  }


}





interface alert {
  visible: boolean,
  type: string,
  message: string
}
