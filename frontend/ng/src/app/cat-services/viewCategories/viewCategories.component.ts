import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CatServiceService } from '../../services/cat-service.service';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
// import {RlTagInputModule} from 'angular2-tag-input';
import { SecurityService } from '../../services/security.service';

@Component({
  selector: 'app-viewCategories',
  templateUrl: './viewCategories.component.html',
  styleUrls: ['./viewCategories.component.scss']
})
export class ViewCategoriesComponent implements OnInit {

  data = [];

  constructor(
    private router: Router,
    private catServiceService: CatServiceService,
    public securityService: SecurityService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.catServiceService.getAllCategories({privilege: 28}).subscribe((reply) => {
      console.log(reply);
      this.data = reply;
      for (var i = 0; i < this.data.length; i++){
        this.data[i].definition = this.data[i].definition.split(';');
        this.data[i].definition.pop();
        for (var j = 0; j < this.data[i].definition.length; j++) {
         this.data[i].definition[j] =  this.data[i].definition[j].replace(/:/g, ' | ')
         .replace('datetime', 'Date')
         .replace('select', 'Choice')
         .replace('varchar', 'Text')
         .replace('int', 'Number')
         .replace('boolean', 'True or False');
        }
      }

    });
  }

  edit(id) {
    this.router.navigate ( [ '/services/view/' + id] );
  }

  create() {
    this.router.navigate ([ '/services/create']);
  }

  delete(id) {
    if (confirm('Are you sure you want to delete')) {
      this.catServiceService.delete({id: id}).subscribe((reply) => {
        console.log(reply);
        this.ngOnInit();
      });
    }
  }
}


