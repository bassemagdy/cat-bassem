import { Routes } from '@angular/router';

import { CreateComponent } from './create/create.component';
import { ViewComponent } from './view/view.component';
import { ViewCategoriesComponent } from './viewCategories/viewCategories.component';

export const CatServicesRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'SERVICE'
      }
    }, {
      path: 'view/:name',
      component: ViewComponent,
      data: {
        heading: 'View'
      }
    }, {
      path: '',
      component: ViewCategoriesComponent,
      data: {
        heading: 'View All Categories'
      }
    }
  ]
  }
];
