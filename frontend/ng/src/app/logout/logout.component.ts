import {Component, OnInit} from '@angular/core';
import {TablesService} from '../services/tables.service';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogOutComponent implements OnInit {

  ngOnInit() {
    this.cookie.remove('id');
    this.cookie.remove('role');
    this.cookie.remove('name');
    this.cookie.remove('token');
    setTimeout(() => {
      this.router.navigate ( [ '/authentication/signin' ] );
    }, 0);
  }

  constructor(public cookie: CookieService, public router: Router) {
  }
}
