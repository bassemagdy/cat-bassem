import { Routes } from '@angular/router';

import { LogOutComponent } from './logout.component';

export const LogOutRoutes: Routes = [{
  path: '',
  component: LogOutComponent,
  data: {
    heading: 'Logout',
    removeFooter: true
  }
}];
