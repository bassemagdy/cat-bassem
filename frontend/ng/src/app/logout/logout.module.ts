import { NgModule, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import {  NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { LogOutComponent } from './logout.component';
import { LogOutRoutes } from './logout.routing';

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(LogOutRoutes),
            FormsModule,
            ReactiveFormsModule,
            JsonpModule,
            NgbModule],
            
  declarations: [LogOutComponent]
})

export class LogOutModule {}
