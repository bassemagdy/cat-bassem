import {Component, Input, OnInit} from '@angular/core';
import {ZonepricesService} from '../../services/zones-prices.service';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import { SecurityService } from '../../services/security.service';

@Component({
selector: 'app-zones-viewll',
templateUrl: './viewall.component.html',
styleUrls: ['./viewall.component.scss']
})
export class ViewAllComponent implements OnInit {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  keyword = '';
  spinner = 0;
  order = 'asc';
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;



  constructor(
    private router: Router,
    private zonepricesService: ZonepricesService,
    public securityService: SecurityService,
    public modalService: NgbModal
  ) {}

  ngOnInit() {
    this.zonepricesService.index().subscribe((reply) => {
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      console.log(reply);
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      console.log(this.rows);

      this.collectionSize = reply.count;
    });
  }

  delete(id) {
    if(confirm('Are you sure you want to delete')) {
      this.spinner = 1;
      this.zonepricesService.delete({id: id}).subscribe((reply) => {
        this.ngOnInit();
        this.spinner = 0;
      });
    }
  }

  open(zone) {
    console.log(zone);
    this.router.navigate ( [ 'trips/zone-prices/view/' + zone.id] );
  }

  create() {
    this.router.navigate ( [ 'trips/zone-prices/create/'] );
  }

  onPager(event) {
    if (!isNaN(event)) {
      if (((Math.floor(event / this.pageSize)) !== this.group)) {
      this.zonepricesService.paginate({
        'page': event,
        'columns': this.headersSearch,
        'keyword': this.keyword,
        'column': this.selectedHeader,
        'order': this.order
      }).subscribe((reply) => {

        this.group = event % this.pageSize;
        this.data = reply;
        this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);

      });
    } else {
      this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);
    }
    } else {
      this.page = 1;
    }
}

}  
