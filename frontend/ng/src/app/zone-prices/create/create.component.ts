import { Component, Input } from '@angular/core';
import { ZonesService } from '../../services/zones.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import {CarTypesService} from '../../services/car-types.service';
import { ZonepricesService } from '../../services/zones-prices.service';

@Component({
  selector: 'app-zones',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  
    zone = {
        from_id: '',
        to_id:'',
        price:'',
        peak_hour_price:'',
        car_type_id:0,
   
  };
zones = [];
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
    types;
  constructor(private router: Router, 
              private cookie: CookieService, 
              private cartypesService: CarTypesService, 
              private modalService: NgbModal , 
              public zonesService : ZonesService,
              public zonepricesService : ZonepricesService ) {
    this.zonesService.all().subscribe((reply) => {
      this.zones = reply.data;
      console.log(reply);
    });

    this.cartypesService.index().subscribe((reply) => {
      this.types = reply.data;
      console.log(reply.data);
    });
  }
  
back(){
  this.router.navigate( [ '/zones']);


}



  done() {
    console.log(this.zone);
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };
    this.zonepricesService.create({ zone : this.zone
    }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ 'trips/zone-prices/'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }

  // fetchzones(){

  // }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}





