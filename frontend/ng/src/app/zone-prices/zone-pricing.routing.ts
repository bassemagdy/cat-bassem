import { Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewall/viewall.component';
import { ViewComponent } from './view/view.component';

export const ZonespricesRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'Create zone'
      }
    },
    {
      path: '',
      component: ViewAllComponent,
      data: {
        heading: 'View All zones'
      }
    },
    
    {
      path: 'view/:id',
    component: ViewComponent,
      data: {
      heading: 'View driver'
      }
    },
  ],
  }
];
