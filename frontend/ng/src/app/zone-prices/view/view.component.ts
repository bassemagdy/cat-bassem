import { Component, Input, OnInit } from '@angular/core';
import {DriversService} from '../../services/drivers.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CarTypesService} from '../../services/car-types.service';
import { ZonepricesService } from '../../services/zones-prices.service';
import { ZonesService } from '../../services/zones.service';



@Component({
  selector: 'app-view-zone',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
    zone = {
      from_id:'',
      to_id:'',
      price:'',
      peak_hour_price:'',
      id:'',
      car_type_id:''
    };
  zones=[];
  types=[];
  otherZoneId=[];

  constructor(private router: Router,private activatedRoute: ActivatedRoute,
     private cookie: CookieService,
     public driversService : DriversService,
     public zonesService : ZonesService,
     public zonepricesService : ZonepricesService,
     public carTypesService: CarTypesService
    ) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });

    this.zonesService.all().subscribe((reply) => {
      this.zones = reply.data;
      console.log(reply);
    });

    this.carTypesService.index().subscribe((reply) => {
      this.types = reply.data;
      console.log(this.types);
    });
}

  ngOnInit() {
     this.zonepricesService.view({id: this.id}).subscribe((reply) => {
     console.log(this.id);
      console.log(reply);
      this.zone = reply;
      console.log("/////////////");
      // this.zonepricesService.otherzone({from_id:this.zone.from_id, to_id:this.zone.to_id,car_type:this.zone.car_type_id ,price:this.zone.price,peak_hour_price:this.zone.peak_hour_price}).subscribe((other)=>{
        this.zonepricesService.otherzone({from_id:this.zone.from_id, to_id:this.zone.to_id,car_type:this.zone.car_type_id}).subscribe((other)=>{

        console.log('line 68');
        console.log(other[0].id);
        this.otherZoneId.push(other[0].id);
        console.log('line 70');
      });
    });
    // console.log(this.zone);
    // console.log(this.zone.car_type_id);
    // console.log(this.zone.from_id);
    // console.log(this.zone.to_id);
    
  }

  editZone() {
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/trips/zone-prices'] );
  }


  done() 
  {

    console.log(this.zone);
    this.zonepricesService.edit({ 
      from_id:this.zone.from_id,
      to_id:this.zone.to_id,
      price : this.zone.price,
      peak_hour_price : this.zone.peak_hour_price,
      car_type: this.zone.car_type_id,
      id:this.zone.id }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/trips/zone-prices'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
    this.zonepricesService.edit({ 
      from_id:this.zone.to_id,
      to_id:this.zone.from_id,
      price : this.zone.price,
      peak_hour_price : this.zone.peak_hour_price,
      car_type: this.zone.car_type_id,
      id:this.otherZoneId[0] }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/trips/zone-prices'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });


  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




