import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { DateTimePickerModule } from 'ng-pick-datetime';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarModule } from 'ng-sidebar';
import { NgbPagination, NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { ModalChangeStage, ModalComment, ModalAddLead, ModalViewLead, ModalAssignEmployee } from './taskboard/taskboard.component';
import {  ModalViewLead2 } from './customers/viewAll/viewAll.component';

import { ModalViewRole, ModalDeleteRole } from './roles/viewAll/viewAll.component';
import { AgmCoreModule } from '@agm/core';

import {  ModalDeleteStage } from './stages/viewAll/viewAll.component';

import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SharedModule } from './shared/shared.module';
import { AuthGuard } from './shared/guards/auth-guard.service';

import { SigninService } from './services/signin.service';
import { TablesService } from './services/tables.service';
import { DataService } from './services/data.service';
import { ProductService } from './services/product.service';
import { DynamictableService } from './services/dynamictable.service';
import { CatServiceService } from './services/cat-service.service';
import { LeadService } from './services/lead.service';
import { EmployeeService } from './services/employee.service';
import { RoleService } from './services/role.service';
import { DepartmentService } from './services/department.service';
import { ResourcesService } from './services/resources.service';
import { StagesService } from './services/stages.service';
import { ExtendedHttpService } from './extended-http.service';
import { SecurityService } from './services/security.service';
import { Validators } from '@angular/forms';
import {SendpulseService} from './services/send-pulse.service';
import {PaymentsService} from './services/payments.service';
import {CustomersService} from './services/customers.service';
import {CarsService} from './services/car.service';
import {TripsService} from './services/trip.service';
import {StatusService} from './services/car-status.service';
import {CarTypesService} from './services/car-types.service';
import {DriversService} from './services/drivers.service';
import { ZonepricesService } from './services/zones-prices.service';
import { ZonesService } from './services/zones.service';
import {SettingsService} from './services/settings.service';
// import { CookieService, CookieOptions } from 'angular2-cookie';
import { CookieService } from 'angular2-cookie/core';






export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    ModalChangeStage,
    ModalComment,
    ModalAddLead,
    ModalViewLead,
    ModalAssignEmployee,
    ModalViewRole,
    ModalDeleteRole,
    ModalDeleteStage,
    ModalViewLead2
    ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpModule,
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }
    }),
    NgbModule.forRoot(),
    SidebarModule.forRoot(),
    DateTimePickerModule,
    ScrollToModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBHLjuOnHBSw1Gce39rmbYzmFqlTQbaefQ',
      libraries: ["places"]
    })
  ],
  //prod

  // providers: [{ provide: Http, useClass: ExtendedHttpService }, AuthGuard, TablesService, DataService, RoleService, DynamictableService,
  //   SigninService, ProductService, CatServiceService, LeadService, CookieService, CookieOptions ,EmployeeService, DepartmentService, ResourcesService,
  //   StagesService, SecurityService,SendpulseService,PaymentsService,CustomersService,CarsService,TripsService, StatusService,CarTypesService,
  //   DriversService,ZonepricesService , ZonesService,SettingsService],
  // bootstrap: [AppComponent],
  // entryComponents: [ModalChangeStage, ModalComment, ModalAddLead, ModalViewLead,ModalViewLead2,
  //  ModalAssignEmployee, ModalViewRole, ModalDeleteRole,ModalDeleteStage]

   //local
   providers: [{ provide: Http, useClass: ExtendedHttpService }, AuthGuard, TablesService, DataService, RoleService, DynamictableService,
    SigninService, ProductService, CatServiceService, LeadService, CookieService ,EmployeeService, DepartmentService, ResourcesService,
    StagesService, SecurityService,SendpulseService,PaymentsService,CustomersService,CarsService,TripsService, StatusService,CarTypesService,
    DriversService,ZonepricesService , ZonesService,SettingsService],
  bootstrap: [AppComponent],
  entryComponents: [ModalChangeStage, ModalComment, ModalAddLead, ModalViewLead,ModalViewLead2,
   ModalAssignEmployee, ModalViewRole, ModalDeleteRole,ModalDeleteStage]
})
export class AppModule { }
