import { Component } from '@angular/core';
import {NgbModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {NgModule, ViewChild, ViewContainerRef} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule, Validators, FormBuilder, FormGroup} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {OnInit, Input, Renderer2, Pipe, PipeTransform,OnChanges} from '@angular/core';
import {DataService} from '../../services/data.service';
import {DragulaService} from 'ng2-dragula/ng2-dragula';
import { CatServiceService } from '../../services/cat-service.service';
import { ProductService } from '../../services/product.service';
import { LeadService } from '../../services/lead.service';
import { DepartmentService } from '../../services/department.service';
import { ScrollToService, ScrollToConfig } from '@nicky-lenaers/ngx-scroll-to';
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { ModalComment, ModalAddLead , ModalViewLead} from '../taskboard.component';
import { PaymentsService } from '../../services/payments.service';
import { CustomersService } from '../../services/customers.service';
import { RouteReuseStrategy } from '@angular/router';
import {} from '@types/googlemaps';
import {TripsService} from '../../services/trip.service';
import { AgmCoreModule } from '@agm/core';



@Component({
  selector: 'app-vieww',
  templateUrl: './viewphone.component.html',
  styleUrls: ['./viewphone.component.scss']
})
export class PhoneComponent  {
  images: any[] = [];
  num = 1;

  id;
  idd='15';
  stages;
  answer;
  edit=false;
  customerName = '';
  customerAddress='';
  customerNumber='';
  customerNumber2='';
  customerEmail='';
  customerEmail2='';
  customerCompany='';
  customer;
  existCustomer = true;
  vip;
  payments=[];
  lead_payment;
  numberOfLeads;
  comments=[];
  // title: string = 'My first AGM project';
  lat1 = 30.0326867;
  lng1 = 31.2380511;
  // lat1;
  // lng1;
  booking = {

    Address:'',
  };
      constructor(public leadService: LeadService,public cookie: CookieService,
          public catServiceService: CatServiceService, 
          public productService: ProductService,
          private activatedRoute: ActivatedRoute,  
          public modalService: NgbModal,
          public paymentsService: PaymentsService,
          public customerService : CustomersService,
          private router: Router,
          route:ActivatedRoute, 
        public tripsService:TripsService) {
       

            this.router.routeReuseStrategy.shouldReuseRoute = function(){
              return false;
           }
       
        this.activatedRoute.params.subscribe((params: Params) => {
          this.id = params['id'];
          this.customerNumber= this.id;

         // console.log(this.id);
      });
  
      this.paymentsService.index().subscribe((reply) => {
        this.payments = reply.data;
        console.log(reply.data);
      });
       
        this.leadService.getPhoneNumber({phone : this.id}).subscribe((reply) =>{
          this.answer=reply;
        //  console.log(this.answer.leads.length);
           

      if (this.answer==-1){
        console.log("enter");
        this.existCustomer=false;

      //  const modalRef = this.modalService.open(ModalAddLead);
 }

      else {
        let count = 0;
           for(let i = 0; i < this.answer.leads.length; i++){
              
                count++;
           }
           this.numberOfLeads=count;
        // console.log( this.numberOfLeads) ;
        this.edit=true;
        this.leadService.getLead({id:this.idd, customer:this.answer.customer[0].Id}).subscribe((reply) => {
         
       console.log(reply);
       this.customer = reply;
       this.customerName=reply.customer.name;
       this.customerEmail=reply.customer.email;
       this.customerEmail2=reply.customer.email2;
       this.customerNumber= reply.customer.phone;
       this.customerNumber2= reply.customer.phone2;
       this.customerCompany=reply.customer.company;
       this.customerAddress=reply.customer.address;
       this.comments= reply.comment;
       console.log(this.comments);


        
        });
        
      }

    });
  
  


  }

  geo(x){
    if (x=='from'){
      this.tripsService.geo({from : this.booking.Address }).subscribe((reply)=>{
  
      console.log(reply);
      this.lat1=reply.latitude;
      this.lng1 = reply.longitude;
      if (reply==null){
        this.booking.Address = " ";

      }
      // this.booking.Address= reply;
      });
  
    }

  }

  onChosenLocation(event){ /////from
    if (event==null){
      return ;
    }
     console.log(event.coords);
     this.lat1=event.coords.lat;
     this.lng1 = event.coords.lng;
     this.tripsService.getAddress({ lat :this.lat1 , lng :this.lng1 }).subscribe((reply) => {

       console.log(reply);
       if (reply==null){
         this.booking.Address = " ";
       }
       this.booking.Address = reply;

   });
}
open(){


  this.router.navigate ( [ '/viewLead/'+this.answer.customer[0].Id] );
  

}

submit(){


  this.customerService.create({
    customerName: this.customerName,
    customerPayment: this.lead_payment,
    customerAddress: this.booking.Address,
    customerCompany: this.customerCompany,
    customerEmail: this.customerEmail,
    customerPhone: this.customerNumber,
    customerEmail2: this.customerEmail2,
    customerPhone2: this.customerNumber2,
    vip: this.vip
  }).subscribe((reply)=>{
    console.log('rrep');
    console.log(reply);
  });
  console.log(this.customerNumber);
  this.router.navigate ( [ '/viewphone/'+this.customerNumber] );
  //window.location.reload();
}


}
