import {Component, OnInit, Input, Renderer2, Pipe, PipeTransform,OnChanges}  from '@angular/core';
import {DataService} from '../../services/data.service';
import {DragulaService} from 'ng2-dragula/ng2-dragula';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import { CatServiceService } from '../../services/cat-service.service';
import { ProductService } from '../../services/product.service';
import { LeadService } from '../../services/lead.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { EmployeeService } from '../../services/employee.service';
import { DepartmentService } from '../../services/department.service';
import { ScrollToService, ScrollToConfig } from '@nicky-lenaers/ngx-scroll-to';
import { ActivatedRoute, Params } from '@angular/router';
import { ResourcesService } from '../../services/resources.service';
import { PaymentsService } from '../../services/payments.service';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CustomersService } from '../../services/customers.service';

import { StagesService } from '../../services/stages.service';
import { SecurityService } from '../../services/security.service';
import {FormsModule} from "@angular/forms";
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';
import {Router} from '@angular/router';

@Component({
    selector: 'app-lead',
    templateUrl: './viewLead.component.html',
    styleUrls: ['./viewLead.component.scss']
  })
  export class ViewleadComponent {    
    stage;
    chosen;
    lead_payment;
    chosenproducts = [];
    products = [];
    input = [];
    output = [];
    productObjects = [];
    count = [];
    serviceID = [];
    services;
    fields = [];
    fieldTypes = [];
    multivalued = [];
    name = [];
    leadName;
    customerName;
    customerPayment;
    customerAddress;
    customerCompany;
    customerEmail;
    customerPhone;
    customerEmail2;
    customerPhone2;
    vip;
    department;
    lead_source;
    addProducts = [];
    addServices = [];
    dates = [];
    departments = [];
    sources = [];
    stages = [];
    download;
    progress;
    total= false;
    payments=[];
    employees=[];
    assignedEmployee;
    leads = [];
    id = null;
    myid; 
customer;
  
alert: alert = {
  visible: false,
  type: '',
  message: ''
}; 
    constructor(public cookie: CookieService, 
      public resourcesService: ResourcesService,
      public paymentsService: PaymentsService,
      public stagesService: StagesService,
      public departmentService: DepartmentService, 
      public catServiceService: CatServiceService,
      public productService: ProductService,
      public modalService: NgbModal, 
      public securityService: SecurityService,
      public employeeService: EmployeeService,
      private router: Router, 
      public leadService :LeadService ,
      public customersService: CustomersService,
      private activatedRoute: ActivatedRoute
    )
        
    
      {

        this.activatedRoute.params.subscribe((params: Params) => {
          this.id = params['id'];
         // console.log(this.id);
      });
      this.customersService.viewCustomer({id:this.id}).subscribe((reply)=>{
      this.customer=reply[0];
      console.log(this.id);
      console.log(reply);


         this.customerName=this.customer.name;
         this.customerAddress=this.customer.address;
         this.customerCompany=this.customer.company;
         this.customerEmail=this.customer.email;
         this. customerPhone=this.customer.phone;
         this.customerEmail2=this.customer.email2;
         this.customerPhone2=this.customer.phone2;







      });



     
      this.productService.getAllProducts().subscribe((reply) => {
          this.productObjects = reply;
        });
      this.catServiceService.getAllCategories({}).subscribe((reply) => {
          console.log(reply);
          this.services = reply;
        });
      this.departmentService.all().subscribe((reply) => {
          this.departments = reply;
        });
  
        this.resourcesService.index().subscribe((reply) => {
          this.sources = reply.data;
          console.log(reply.data);
        });
  
        this.paymentsService.index().subscribe((reply) => {
          this.payments = reply.data;
          console.log(reply.data);
        });
  
        this.stagesService.all().subscribe((reply) => {
          this.stages = reply;
          console.log(reply);
        });
  
        var manager = this.cookie.get('id');
        console.log("manager"+manager);
        this.employeeService.getEmployeesOfManager({id: manager}).subscribe((reply) => {
        console.log(reply);
        this.employees=reply;
    });
  
    this.myid = cookie.get('id');

    
    }
  
  
    close() {
      
  console.log(this.assignedEmployee);
      var date1 = new Date()
      date1.setTime(date1.getTime() + (2*60 * 60 * 1000));     
      var date = date1.toISOString().slice(0, 19).replace('T', ' ');
      this.output = [];
      for (var i = 0; i < this.input.length; i++)
      {
        this.output[i] = [];
          for (var j = 0; j < this.input[i].length; j++)
          {
           
         
            this.output[i].push(this.fields[i][j] + ':' + this.input[i][j] + ';');
          }
            
      }  
  
  
      for (var i = 0; i < this.products.length; i++)
          this.chosenproducts.push(this.products[i].id);
  
        
          this.leadService.createLead({name: this.leadName,
            customerName: this.customerName,
            customerPayment: this.lead_payment,
            customerAddress: this.customerAddress,
            customerCompany: this.customerCompany,
            customerEmail: this.customerEmail,
            customerPhone: this.customerPhone,
            customerEmail2: this.customerEmail2,
            customerPhone2: this.customerPhone2,
            vip: this.vip,
            created_by: this.cookie.get('id'),
            date_created: date,
            stage: this.stage,
            department: this.department,
            lead_source: this.lead_source,
            serviceID: this.serviceID,
            output: this.output,
            chosenproducts:  this.chosenproducts,
            count: this.count,
            assignedEmployee : this.assignedEmployee+"",}).subscribe((reply) => {
            console.log(reply);
            this.updateleads();
              this.alert = {
                visible: true,
                type: 'success',
                message: 'Successfully added!'
              }
              setTimeout(() => {
              }, 1000);
          }, (reply) => {
            this.alert = {
                visible: true,
                type: 'danger',
                message: 'Something went wrong. Please try again'
              }
          })
        
        
          this.router.navigate ( [ '/leads/'] );

          
      
  
    }
    
    show(chosen)
    {
      this.total=true;
      this.chosen = chosen;
      if (this.chosen == 'Product')
      {
        this.addProducts.push('0');
      }
      if (this.chosen == 'Service')
      {
        this.addServices.push('0');
      }
  
    }
  
  
    populate(x) {
      var idx = -1;
      this.fields[x] = [];
      this.fieldTypes[x] = [];
      this.multivalued[x] = [];
      this.input[x] = [];
  
      console.log(this.serviceID);
  
      for (var i = 0; i < this.services.length; i++)
        if (this.services[i].id == this.serviceID[x])
          idx = i;
  
      console.log(idx);
      if (idx > -1)
      {
         this.name[x] = this.services[idx].name;
         var definition = this.services[idx].definition;
         var seperateFields = definition.split(';');
         this.dates[idx] = [];
         for (var i = 0; i < seperateFields.length - 1; i++)
         {
            var seperateValues = seperateFields[i].split(':');
            this.fields[x].push(seperateValues[0]);
            this.fieldTypes[x].push(seperateValues[1]);
            
            if (seperateValues.length > 2)
            {
              var values = seperateValues[2].split(',');
              this.multivalued[x].push(values);
            }            
            else
              this.multivalued[x].push([]);
  
        }
      }
  
      
    }

    updateleads() {

    
      console.log('update');
      this.leads = [];
  
      this.stages.forEach(stage => {
        this.leads[stage.name] = [];
      });
  
      if (this.employees) {
        if (this.id) {
          this.leadService.getLeads({id: this.id}).subscribe((leads) => {
            for (let i = 0; i < leads.length; i++) {
              if (leads[i].stage_name && this.leads[leads[i].stage_name]) {
  
                console.log(leads[i].stage_name);            
                this.leads[leads[i].stage_name].push(leads[i]);
              }
            }
          });
        }else {
          this.leadService.getLeads({}).subscribe((leads) => {
            for (let i = 0; i < leads.length; i++) {
              if (leads[i].stage_name && this.leads[leads[i].stage_name]) {
                console.log(leads[i].stage_name);
                this.leads[leads[i].stage_name].push(leads[i]);
              }
            }
  
          });
        }
      }else {
        this.leadService.getLeads({id: this.myid}).subscribe((leads) => {
          for (let i = 0; i < leads.length; i++) {
            if (leads[i].stage_name) {
              this.leads[leads[i].stage_name].push(leads[i]);
            }
          }
        });
      }
    }
    
  }
  
  interface alert {
    visible: boolean;
      type: string;
      message: string;
  }
  