import {Component, OnInit, Input, Renderer2, Pipe, PipeTransform,OnChanges}  from '@angular/core';
import {DataService} from '../../services/data.service';
import {DragulaService} from 'ng2-dragula/ng2-dragula';
import {NgbModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import { CatServiceService } from '../../services/cat-service.service';
import { ProductService } from '../../services/product.service';
import { LeadService } from '../../services/lead.service';
import { DepartmentService } from '../../services/department.service';
import { ScrollToService, ScrollToConfig } from '@nicky-lenaers/ngx-scroll-to';
import {FormsModule} from "@angular/forms";
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';


@Component({
  selector: 'app-history',
  templateUrl: './viewHistory.component.html',
  styleUrls: ['./viewHistory.component.scss']
})
export class HistoryComponent {

idd;
stages;
spinner;
    constructor(public leadService: LeadService,public cookie: CookieService, public departmentService: DepartmentService, public catServiceService: CatServiceService, public productService: ProductService,private activatedRoute: ActivatedRoute) {
      this.activatedRoute.params.subscribe((params: Params) => {
        this.idd = params['id'];
    });

    this.leadService.getLeadStages({id: this.idd}).subscribe((reply) => {
        console.log("history");
  
        console.log(reply);
        
      this.stages=reply; 
      console.log(this.stages);
      });
}
}