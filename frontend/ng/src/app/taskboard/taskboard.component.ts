import {Component, OnInit, Input, Renderer2, Pipe, PipeTransform,OnChanges}  from '@angular/core';
import {DataService} from '../services/data.service';
import {DragulaService} from 'ng2-dragula/ng2-dragula';
import {NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import { CatServiceService } from '../services/cat-service.service';
import { ProductService } from '../services/product.service';
import { LeadService } from '../services/lead.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { EmployeeService } from '../services/employee.service';
import { DepartmentService } from '../services/department.service';
import { ScrollToService, ScrollToConfig } from '@nicky-lenaers/ngx-scroll-to';
import { ActivatedRoute, Params } from '@angular/router';
import { ResourcesService } from '../services/resources.service';
import { PaymentsService } from '../services/payments.service';

import { StagesService } from '../services/stages.service';
import { SecurityService } from '../services/security.service';
import {FormsModule} from "@angular/forms";
import { SimpleChange } from '@angular/core/src/change_detection/change_detection_util';
import {Router} from '@angular/router';


@Component({
  selector: 'app-taskboard',
  templateUrl: './taskboard.component.html',
  styleUrls: ['./taskboard.component.scss']
})
export class TaskboardComponent {

  leads = [];
  posts: any[][] = [];
  stages = [];
  data: drag;
  closeResult: string;
  alert: alert = {
    visible: false,
    type: '',
    message: ''
  };
  dates = [];
  modalopened = false;
  id = null;
  employees = null;
  myid;
  modalcomm=false;
  
  constructor(
    public employeeService: EmployeeService,
    public stagesService: StagesService,
    private activatedRoute: ActivatedRoute,
    public cookie: CookieService,
    public dataService: DataService,
    public DragulaService: DragulaService,
    public modalService: NgbModal,
    public securityService: SecurityService,
    public leadService: LeadService) {
    //console.log('constructor');

    this.myid = cookie.get('id');

    this.stagesService.all().subscribe((reply) => {
      this.stages = reply;
   //   console.log(reply);
    });

    this.employeeService.getEmployeesOfManager({id: this.cookie.get('id')}).subscribe((employees) => {
      if (employees.length !== 0) {
        this.employees = employees;
      }
     // console.log(employees);
      this.activatedRoute.params.subscribe((params: Params) => {
        if (params['id']) {
          this.id = params['id'];
          this.updateleads();
        }else {
          this.updateleads();
        }
      });
    });
    console.log('constru');

    var mod =0;

    DragulaService.drop.subscribe((value) => {
mod++;
      //console.log('drag');
      if (!this.modalopened) {
        this.modalopened = true;
        console.log(mod);


       // console.log(mod);
      
        const modalRef = this.modalService.open(ModalComment);
mod--;
console.log(mod);

        modalRef.result.then((result) => {

          var reminder = (result) ? (result.date) ? result.date.toISOString().slice(0, 19).replace('T', ' ') : null : null;

          this.leadService.updateLead({
            id: value[1].id,
            origin: value[3].id,
            destination: value[2].id,
            comment: (result) ? result.comment : null,
            reminder: reminder
          }).subscribe((reply) => {
            console.log('W:' + reply);
            this.alert = {
              visible: true,
              type: 'success',
              message: 'Successfully moved'
            };
            this.modalopened = false;
            setTimeout(() => {
              this.dismissAlert();
            }, 1000);
            this.modalcomm= false;

          }, (reply) => {
            this.alert = {
              visible: true,
              type: 'danger',
              message: 'Something went wrong. Please try again'
            };
          });
        }, (result) => {
          // cancelling the addition of a comment will cause the entire operation to be aborted
          // this.dataService.updateTasks({
          //   id: value[1].id,
          //   origin: value[3].id,
          //   destination: value[2].id
          // }).subscribe((reply) => {
          //   console.log(reply);
          // });
        });
      }
    
  
    });
  }
  
  updateleads() {

    
    console.log('update');
    this.leads = [];

    this.stages.forEach(stage => {
      this.leads[stage.name] = [];
    });

    if (this.employees) {
      if (this.id) {
        this.leadService.getLeads({id: this.id}).subscribe((leads) => {
          for (let i = 0; i < leads.length; i++) {
            if (leads[i].stage_name && this.leads[leads[i].stage_name]) {

              console.log(leads[i].stage_name);            
              this.leads[leads[i].stage_name].push(leads[i]);
            }
          }
        });
      }else {
        this.leadService.getLeads({}).subscribe((leads) => {
          for (let i = 0; i < leads.length; i++) {
            if (leads[i].stage_name && this.leads[leads[i].stage_name]) {
              console.log(leads[i].stage_name);
              this.leads[leads[i].stage_name].push(leads[i]);
            }
          }

        });
      }
    }else {
      this.leadService.getLeads({id: this.myid}).subscribe((leads) => {
        for (let i = 0; i < leads.length; i++) {
          if (leads[i].stage_name) {
            this.leads[leads[i].stage_name].push(leads[i]);
          }
        }
      });
    }
  }


  viewLead(lead) {
    this.leadService.getLead({id:lead.id, customer:lead.customer_id}).subscribe((reply) => {
      console.log("---------////////----");
      console.log(lead.id);
      console.log(reply);
      const modalRef = this.modalService.open(ModalViewLead);
      console.log(lead);
      modalRef.componentInstance.lead = lead;
      modalRef.componentInstance.products = reply.products;
      modalRef.componentInstance.services = reply.services;
      modalRef.componentInstance.customer = reply.customer;
      modalRef.result.then((result) => {
        this.updateleads();

      }, (reply) => {});

      }, (result) => {

    });
  }

  dismissAlert() {
    this.alert = {
      visible: false,
      type: '',
      message: ''
    };
  }
  archiveLead(id){
    console.log(id);
    // return;
    
    if(confirm('are you sure you want to arcive this Lead?')){
    if(id){
      let arch = 1;
      this.leadService.archiveLead({check:arch,id:id.id }).subscribe((reply)=>{ 
        if(reply){
          console.log(reply)
           location.reload() ;      
        }else{
          alert('something went wrong');
        }
      })
    }
  
  }
}

  open(stage, task, i) {
    const modalRef = this.modalService.open(ModalChangeStage);
    modalRef.componentInstance.origin = stage;
    modalRef.result.then((result) => {
      if (result != 'Close click') {

        var reminder = (result.date) ? result.date.toISOString().slice(0, 19).replace('T', ' ') : null;
        var lead = this.leads[stage].splice(i, 1);
        this.leads[result.chosen].push(lead[0]);

        this.leadService.updateLead({
          id: task.id,
          origin: stage,
          destination: result.chosen,
          comment: (result) ? result.comment : null,
          reminder: reminder
        }).subscribe((reply) => {
          console.log(reply);
          this.alert = {
            visible: true,
            type: 'success',
            message: 'Successfully moved'
          }
          setTimeout(() => {
            this.dismissAlert();
          }, 1000);
        }, (reply) => {
           this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          }
        });
      }
    }, (result) => {
    });
  }

  newLead(){
    console.log(this.cookie.get('id'));
    const modalRef = this.modalService.open(ModalAddLead);
    modalRef.result.then((result) => {
      console.log(result);
      this.leadService.createLead(result).subscribe((reply) => {
        console.log(reply);
        this.updateleads();
          this.alert = {
            visible: true,
            type: 'success',
            message: 'Successfully added!'
          }
          setTimeout(() => {
            this.dismissAlert();
          }, 1000);
      }, (reply) => {
        this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          }
      })

    }, (result) => {

    });
  }



  settings(lead) {
    var manager = this.cookie.get('id');
    console.log(manager);
    this.employeeService.getEmployeesOfManager({id: manager}).subscribe((reply) => {
      console.log(reply);
      const modalRef = this.modalService.open(ModalAssignEmployee);
      modalRef.componentInstance.employees = reply;
      modalRef.result.then((result) => {
        this.leadService.assignLead({lead: lead.id, employee: result}).subscribe((reply) => {
          console.log(reply);
          this.updateleads();
          this.alert = {
            visible: true,
            type: 'success',
            message: 'Successfully changed!'
          }
          setTimeout(() => {
            this.dismissAlert();
          }, 1000);
      }, (reply) => {
        this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          }
      })

    }, (result) => {

    });
    });
  }
}



@Component({
  selector: 'ngbd-modal-add-lead',
  templateUrl: './modalAddLead.html',
  styleUrls: ['./taskboard.component.scss']

})

export class ModalAddLead {
  stage;
  chosen;
  lead_payment;
  chosenproducts = [];
  products = [];
  input = [];
  output = [];
  productObjects = [];
  count = [];
  serviceID = [];
  services;
  fields = [];
  fieldTypes = [];
  multivalued = [];
  name = [];
  leadName;
  customerName;
  customerPayment;
  customerAddress;
  customerCompany;
  customerEmail;
  customerPhone;
  customerEmail2;
  customerPhone2;
  vip;
  department;
  lead_source;
  addProducts = [];
  addServices = [];
  dates = [];
  departments = [];
  sources = [];
  stages = [];
  download;
  progress;
  total= false;
  payments=[];
  employees=[];
  assignedEmployee;


  constructor(public cookie: CookieService, public resourcesService: ResourcesService, public paymentsService: PaymentsService,public stagesService: StagesService, public departmentService: DepartmentService, public catServiceService: CatServiceService, public productService: ProductService,public activeModal: NgbActiveModal, public securityService: SecurityService,
    public employeeService: EmployeeService) {

    this.productService.getAllProducts().subscribe((reply) => {
        this.productObjects = reply;
      });
    this.catServiceService.getAllCategories({}).subscribe((reply) => {
        console.log(reply);
        this.services = reply;
      });
    this.departmentService.all().subscribe((reply) => {
        this.departments = reply;
      });

      this.resourcesService.index().subscribe((reply) => {
        this.sources = reply.data;
        console.log(reply.data);
      });

      this.paymentsService.index().subscribe((reply) => {
        this.payments = reply.data;
        console.log(reply.data);
      });

      this.stagesService.all().subscribe((reply) => {
        this.stages = reply;
        console.log(reply);
      });

    var manager = this.cookie.get('id');
    console.log("manager"+manager);
    this.employeeService.getEmployeesOfManager({id: manager}).subscribe((reply) => {
    console.log(reply);
    this.employees=reply;
  });


  
  }


  close() {
    console.log(this.assignedEmployee);
    var date1 = new Date()
    date1.setTime(date1.getTime() + (2*60 * 60 * 1000));     
    var date = date1.toISOString().slice(0, 19).replace('T', ' ');
    this.output = [];
    for (var i = 0; i < this.input.length; i++)
    {
      this.output[i] = [];
        for (var j = 0; j < this.input[i].length; j++)
        {
          
          this.output[i].push(this.fields[i][j] + ':' + this.input[i][j] + ';');
        }
          
    }  


    for (var i = 0; i < this.products.length; i++)
        this.chosenproducts.push(this.products[i].id);

      this.activeModal.close({ name: this.leadName,
                               customerName: this.customerName,
                               customerPayment: this.lead_payment,
                               customerAddress: this.customerAddress,
                               customerCompany: this.customerCompany,
                               customerEmail: this.customerEmail,
                               customerPhone: this.customerPhone,
                               customerEmail2: this.customerEmail2,
                               customerPhone2: this.customerPhone2,
                               vip: this.vip,
                               created_by: this.cookie.get('id'),
                               date_created: date,
                               stage: this.stage,
                               department: this.department,
                               lead_source: this.lead_source,
                               serviceID: this.serviceID,
                               output: this.output,
                               chosenproducts:  this.chosenproducts,
                               count: this.count,
                               assignedEmployee : this.assignedEmployee+"",
      });
     
    

  }
  
  show(chosen)
  {
    this.total=true;
    this.chosen = chosen;
    if (this.chosen == 'Product')
    {
      this.addProducts.push('0');
    }
    if (this.chosen == 'Service')
    {
      this.addServices.push('0');
    }

  }


  populate(x) {
    var idx = -1;
    this.fields[x] = [];
    this.fieldTypes[x] = [];
    this.multivalued[x] = [];
    this.input[x] = [];

    console.log(this.serviceID);

    for (var i = 0; i < this.services.length; i++)
      if (this.services[i].id == this.serviceID[x])
        idx = i;

    console.log(idx);
    if (idx > -1)
    {
       this.name[x] = this.services[idx].name;
       var definition = this.services[idx].definition;
       var seperateFields = definition.split(';');
       this.dates[idx] = [];
       for (var i = 0; i < seperateFields.length - 1; i++)
       {
          var seperateValues = seperateFields[i].split(':');
          this.fields[x].push(seperateValues[0]);
          this.fieldTypes[x].push(seperateValues[1]);
          
          if (seperateValues.length > 2)
          {
            var values = seperateValues[2].split(',');
            this.multivalued[x].push(values);
          }            
          else
            this.multivalued[x].push([]);

      }
    }

    
  }
  
}



@Component({
  selector: 'ngbd-modal-view-lead',
  templateUrl: './modalViewLead.html',
  styleUrls: ['./taskboard.component.scss']

})

export class ModalViewLead implements OnInit {

  @Input() lead;
  @Input() products;
  @Input() services;
  @Input() customer;
  currentProduct;
  currentService;
  edit = false;
  addProducts = [];
  addServices = [];
  chosen;
  dates = [];
  count = [];
  input = [];
  serviceID = [];
  name = [];
  fields = [];
  fieldTypes = [];
  multivalued = [];
  servicetypes = [];
  productObjects = [];
  newproducts = [];
  output = [];
  chosenproducts = [];
  deletedProducts = [];
  deletedServices = [];
  stages=[];
  sources=[];
  departments=[];
  sourcee="me";
  departmentt;
  stagee;
  leadId;
  leadIdd;
  constructor(private _scrollToService: ScrollToService,
    private router: Router,
    public activeModal: NgbActiveModal,
    public productService: ProductService,
    public catServiceService: CatServiceService,
    public securityService: SecurityService,
    public leadService: LeadService,public resourcesService: ResourcesService, public stagesService: StagesService, public departmentService: DepartmentService) {

     this.productService.getAllProducts().subscribe((reply) => {
        this.productObjects = reply;
      });
    this.catServiceService.getAllCategories({}).subscribe((reply) => {
        this.servicetypes = reply;
      });
      this.departmentService.all().subscribe((reply) => {
        this.departments = reply;
      });

      this.resourcesService.index().subscribe((reply) => {
        this.sources = reply.data;
        console.log(reply.data);
      });

      this.stagesService.all().subscribe((reply) => {
        this.stages = reply;
        console.log(reply);
      });

      
    
     

  }

  ngOnInit() {
    this.resourcesService.view({id: this.lead.source}).subscribe((reply) => {

      console.log(reply);
      this.sourcee=reply;
      console.log(this.customer)
     
    });



    this.leadService.getLeadStages({id: this.lead.id}).subscribe((reply) => {

      console.log(reply);
      
     
    });
    this.productService.getTotalInstance({id:this.lead.id }).subscribe((reply) => {
      this.leadId = reply;
      console.log(reply);
      var sum=0;

      for (var j = 0; j < reply.length; j++)
      {
       sum = sum +reply[j].limited*reply[j].price;
      }

      this.catServiceService.getPrice({id:this.lead.id }).subscribe((reply) => {
        
        console.log(reply);
  
        for (var j = 0; j < reply.length; j++)
        {
         sum = sum +reply[j].price;

        }
        this.leadIdd=sum;

      });

    });

    

    this.departmentService.view({id: this.lead.department}).subscribe((reply) => {

      console.log(reply);
      this.departmentt=reply.name;
     
    });

   
      this.stagee=this.lead.stage_name;
     
   


    console.log(this.products);
    console.log(".////////");
    console.log(this.lead);

    for (var i = 0; i < this.products.length; i++){
      this.products[i].Data = this.products[i].Data.split(';');
      this.products[i].Data.pop();
    }

    this.services.forEach(service => {

      service.Data = service.Data.split(';');
      service.Definition = service.Definition.split(';');
      service.Data.pop();
      service.Definition.pop();
      for (var i = 0; i < service.Data.length; i++)
      {
        service.Definition[i] = service.Definition[i].split(':');
        service.Data[i] = service.Data[i].split(':');
        if (service.Definition[i][1] == 'datetime')
          service.Data[i][1] += ':' + service.Data[i][2];
        if (service.Definition[i][1] == 'select')
          service.Definition[i][2] = service.Definition[i][2].split(',');
      }

      console.log(service.Definition);
     
    });

    


  }

  open(){

    this.router.navigate(['/board/viewHistory/'+this.lead.id]);
    this.activeModal.close({

    });
}


  
  
  close() {
    this.activeModal.close({

    });
  }

  editLead() {
    this.edit = !this.edit;
    if (!this.edit)
    {
      this.output = [];
    for (var i = 0; i < this.input.length; i++)
    {
      this.output[i] = [];
        for (var j = 0; j < this.input[i].length; j++)
        {
          // if(this.fieldTypes[i][j] == 'datetime')
          //   this.input[i][j].toLocaleString().slice(0, 19).replace(',', '');

          this.output[i].push(this.fields[i][j] + ':' + this.input[i][j] + ';');
        }
          
    }  

   
    for (var i = 0; i < this.newproducts.length; i++)
        this.chosenproducts.push(this.newproducts[i].id);
     console.log(this.chosenproducts);
     console.log(this.lead);
     this.leadService.edit({products: this.products,
                            count: this.count,
                            
                            chosenproducts: this.chosenproducts,
                            serviceID: this.serviceID, 
                            servicedata: this.output, 
                            services: this.services, 
                            id: this.lead.id, 
                            customer: this.customer, 
                            deletedProducts: this.deletedProducts,
                            deletedServices: this.deletedServices,
                            lead: this.lead}).subscribe((reply) => {
         console.log(reply);
       //  this.close();
       })

    
    }
     this.ngOnInit();
  }

  deleteProduct(id, idx){
    this.deletedProducts.push(id);
    this.products.splice(idx, 1);

  }

  deleteService(id, idx){
    this.deletedServices.push(id);
    this.services.splice(idx, 1);

  }

  delete() {
    this.leadService.delete({id: this.lead.id}).subscribe((reply) => {
         console.log(reply);
         this.close();
       }) 
  }


  public triggerScrollToProduct($event: Event) {
    
    const config: ScrollToConfig = {
      target: 'product' + this.currentProduct
    }
 
    this._scrollToService.scrollTo($event, config);
  }

  public triggerScrollToService($event: Event) {
    
    const config: ScrollToConfig = {
      target: 'service' + this.currentService
    }
 
    this._scrollToService.scrollTo($event, config);
  }

    show(chosen, $event)
  {
    this.chosen = chosen;
    if (this.chosen == 'Product')
    {
      this.addProducts.push('0');
      this.currentProduct = this.addProducts.length - 1;
      this.triggerScrollToProduct($event);
    }
    if (this.chosen == 'Service')
    {
      this.addServices.push('0');
      this.currentService = this.addServices.length - 1;
       this.triggerScrollToService($event);
    }

  }


  populate(x)
  {
    var idx = -1;
    this.fields[x] = [];
    this.fieldTypes[x] = [];
    this.multivalued[x] = [];
    this.input[x] = [];

    console.log(this.serviceID)

    for (var i = 0; i < this.servicetypes.length; i++)
      if (this.servicetypes[i].id == this.serviceID[x])
        idx = i;

    console.log(idx);
    if (idx > -1)
    {
       this.name[x] = this.servicetypes[idx].name;
       var definition = this.servicetypes[idx].definition;
       var seperateFields = definition.split(';');
       this.dates[idx] = [];
       for (var i = 0; i < seperateFields.length - 1; i++)
       {
          var seperateValues = seperateFields[i].split(':');
          this.fields[x].push(seperateValues[0]);
          this.fieldTypes[x].push(seperateValues[1]);
          
          if (seperateValues.length > 2)
          {
            let values = seperateValues[2].split(',');
            this.multivalued[x].push(values);
          }
          else
            this.multivalued[x].push([]);

      }
    }


  }


}


@Component({
  selector: 'ngbd-modal-assign-employee',
  templateUrl: './modalAssignEmployee.html'

})
export class ModalAssignEmployee {

  @Input() employees;
  assignedEmployee;
  myid;

  constructor(public activeModal: NgbActiveModal,public cookie: CookieService, public securityService: SecurityService,
    public employeeService: EmployeeService) {
    

  }

  
  close() {
    this.activeModal.close(this.assignedEmployee);
  }
}





@Component({
  selector: 'ngbd-modal-comment',
  templateUrl: './modalComment.html',
  styleUrls: ['./taskboard.component.scss']


})
export class ModalComment {

  @Input() comment;
  rem = false;
  date;

  constructor(public activeModal: NgbActiveModal) {}

  close() {

    console.log("mmm");
    this.activeModal.close({
      comment: this.comment,
      date: this.date
    });
  }

  reminder() {
    this.rem = !this.rem;
  }
}




@Component({
  selector: 'ngbd-modal-content',
  templateUrl: 'modalChangeStage.html',
  styleUrls: ['./taskboard.component.scss']
})
export class ModalChangeStage {

  @Input() rows;
  @Input() chosen;
  @Input() comment;
  @Input() origin;
  rem = false;
  date;
  stages;

  constructor(public activeModal: NgbActiveModal, public stagesService: StagesService) {
    this.stagesService.all().subscribe((reply) => {
      this.stages = reply;
      console.log(reply);
    });
  }

  close() {
    this.activeModal.close({
      chosen: this.chosen,
      comment: this.comment,
      date: this.date
    });
  }

  reminder() {
    this.rem = !this.rem;
  }
}



interface Task {
  id: string;
    text: string;
}

interface drag {
  id: string;
    origin: string;
    destination: string;
}

interface alert {
  visible: boolean;
    type: string;
    message: string;
}
