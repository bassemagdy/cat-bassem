import { Routes } from '@angular/router';

import { TaskboardComponent } from './taskboard.component';
import { HistoryComponent } from './viewHistory/viewHistory.component';
import { PhoneComponent } from './viewphone/viewphone.component';
import { ViewleadComponent } from './viewLead/viewLead.component';


export const TaskboardRoutes: Routes = [{
  path: '',
  component: TaskboardComponent,
  data: {
    heading: 'Pipeline',
    removeFooter: true
  }
},
{
  path: 'board/:id',
  component: TaskboardComponent,
  data: {
    heading: 'Employee Pipeline',
    removeFooter: true
  }
},

{
  path: 'viewHistory/:id',
  component: HistoryComponent,
  data: {
    heading: 'history',
    removeFooter: true
  }
},
{
  path: 'viewphone/:id',
  component: PhoneComponent,
  data: {
    heading: 'new Customer',
    removeFooter: true
  }
},
{
  path: 'viewLead/:id',
  component: ViewleadComponent,
  data: {
    heading: 'Lead',
    removeFooter: true
  }
}

];
