import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DragulaModule, DragulaService } from 'ng2-dragula/ng2-dragula';
import { NgbModal,NgbActiveModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { PhoneComponent } from './viewphone/viewphone.component';
import {ViewleadComponent} from './viewLead/viewLead.component';
//import { SocialComponent } from './viewphone/viewphone.component';

import { TaskboardComponent } from './taskboard.component';
import { TaskboardRoutes } from './taskboard.routing';
import { HistoryComponent } from './viewHistory/viewHistory.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(TaskboardRoutes), DragulaModule, FormsModule,  AgmCoreModule.forRoot({
    apiKey: 'AIzaSyBHLjuOnHBSw1Gce39rmbYzmFqlTQbaefQ'
  }) ],
  declarations: [TaskboardComponent,HistoryComponent,PhoneComponent,ViewleadComponent]
})

export class TaskboardModule {



}
