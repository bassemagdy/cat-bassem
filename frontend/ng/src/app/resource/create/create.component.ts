import { Component, Input } from '@angular/core';
import { ResourcesService } from '../../services/resources.service';
import { EmployeeService } from '../../services/employee.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';

@Component({
  selector: 'app-resources',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  
  resource = {
    name: '',

  };
  
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };

  constructor(private router: Router, private cookie: CookieService,private resourcesService:ResourcesService, private modalService: NgbModal) {
 
  }
back(){
  this.router.navigate( [ '/resources']);
}



  done() {
    console.log(this.resource);
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };
    this.resourcesService.create({ name: this.resource.name
    }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/resources'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}





