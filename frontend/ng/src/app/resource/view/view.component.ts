import { Component, Input, OnInit } from '@angular/core';
import { ResourcesService } from '../../services/resources.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-view-resource',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id: string;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
  resource;

  constructor(private router: Router,private activatedRoute: ActivatedRoute, private cookie: CookieService, private resourcesService: ResourcesService) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });
}

  ngOnInit() {
     this.resourcesService.view({id: this.id}).subscribe((reply) => {
     console.log(this.id);
      console.log(reply);
      this.resource = reply;
      console.log(this.resource);
    });
  }

  editResources() {
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/resources'] );
  }


  done() {
    console.log(this.resource);
    this.resourcesService.edit({ name: this.resource.name,id:this.resource.id }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/resources'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




