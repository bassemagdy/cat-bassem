import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class TripsService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

index(){

  return this.http_post_auth('/trips');

}

paginate(data){
    return this.http_post_auth('/paginateTrip',data);
  
    
  }

  sort(data){
    return this.http_post_auth('/sortCars',data);
 }

 booking(data){

  return this.http_post_auth('/booking',data);


 }

 geo(data){
  return this.http_post_auth('/geo',data);

}

getAddress(data){

  return this.http_post_auth('/getAddress',data);

}


cancelTrip(data){
  return this.http_post_auth('/cancelTrip',data);
}

}