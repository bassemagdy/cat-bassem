import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class EmployeeService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  getEmployeesOfManager(data) {
    return this.http_post_auth('/getEmployeesOfManager', data);
  }

  createEmployee(data) {
    return this.http_post_auth('/createEmployee', data);
  }

  getData() {
    return this.http_get_auth('/viewAllEmployees');
  }

  paginate(data) {
    return this.http_post_auth('/paginateEmployees', data);
  }

  search(data) {
    return this.http_post_auth('/searchEmployees', data);
  }

  sort(data) {
    return this.http_post_auth('/sortEmployees', data);
  }

  getEmployee(data) {
    return this.http_post_auth('/getEmployee', data);
  }

  editEmployee(data) {
    return this.http_post_auth('/editEmployee', data);
  }

  editProfile(data) {
    return this.http_post_auth('/editProfile', data);
  }

  deleteEmployee(data) {
    return this.http_post_auth('/deleteEmployee', data);
  }

  // archiveLead(data){
  //   return this.http_post_auth('/archiveLead',data);
  // }

}
