import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { HttpService } from './service';

@Injectable()
export class SigninService extends HttpService {

  constructor( public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  login(data) {
    return this.http_post('/login', data);
  }

  forgotPassword(data) {
    return this.http_post('/forgotPassword', data);
  }

  resetPassword(data) {
    return this.http_post(environment.APIENDPOINT + '/resetPassword', data);
  }

}
