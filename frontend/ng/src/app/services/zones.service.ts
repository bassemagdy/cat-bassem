import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class ZonesService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  all() {
    return this.http_post_auth('/allZones');
  }

  index(){
    return this.http_post_auth('/zones');
  
  }
  
  create(data){
    
    return this.http_post_auth('/createZone',data);
  }
  
  view(data){
    return this.http_post_auth('/viewZone',data);
  }
  
  edit(data){
    return this.http_post_auth('/editZone',data);
  }
  
  delete(data){
    return this.http_post_auth('/deleteZone',data);
  }
  
  paginate(data){
    return this.http_post_auth('/paginateZone',data);  
  }
  

}
