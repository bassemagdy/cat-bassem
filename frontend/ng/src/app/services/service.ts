import { Injectable } from '@angular/core';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

@Injectable()
export class HttpService {

    constructor(public cookie: CookieService, public http: Http) { }

    getToken() {
        if (this.cookie.get('token') == undefined) {
            window.location.href = '/logout';
        }
        return this.cookie.get('token');
    }

    http_post_auth(url: string, data: any = []) {
        return this.http.post(environment.APIENDPOINT + url + '?token=' + this.getToken(), data)
        .map(res => res.json());
    }

    http_get_auth(url: string, data: any = []) {
        data.token = this.getToken();
        return this.http.get(environment.APIENDPOINT + url, {params: data})
        .map(res => res.json());
    }

    http_post(url: string, data: any) {
        return this.http.post(environment.APIENDPOINT + url, data)
        .map(res => res.json());
    }

    http_get(url: string, data: any) {
        return this.http.get(environment.APIENDPOINT + url, data)
        .map(res => res.json());
    }
}
