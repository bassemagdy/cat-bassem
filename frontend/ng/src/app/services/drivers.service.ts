import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class DriversService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  index() {
    return this.http_get_auth('/drivers');
  }

  
  create(data) {
    return this.http_post_auth('/createDriver', data);
  }

  view(data) {
    return this.http_post_auth('/driver', data);
  }

  edit(data) {
    return this.http_post_auth('/editDriver', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteDriver', data);
  }

}
