import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class DepartmentService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  index() {
    return this.http_get_auth('/departments');
  }

  all() {
    return this.http_get_auth('/departments/all');
  }

  create(data) {
    return this.http_post_auth('/createDepartment', data);
  }

  view(data) {
    return this.http_post_auth('/department', data);
  }

  edit(data) {
    return this.http_post_auth('/editDepartment', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteDepartment', data);
  }

  check(data) {
    return this.http_post_auth('/checkDepartment', data);
  }

}
