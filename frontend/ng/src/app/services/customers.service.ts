import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';


@Injectable()
export class CustomersService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  index() {
    return this.http_post_auth('/customers');
  }

  getData() {
    return this.http_post_auth('/customers');
  }
  
  paginate(data) {
    return this.http_post_auth('/paginateCustomer', data);
  }

  getLeads(data) {
    return this.http_post_auth('/getLeads', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteCustomer', data);
  }

  viewCustomer(data){

    return this.http_post_auth('/viewCustomer', data);


  }

  update(data){

    return this.http_post_auth('/updateCustomer',data);
  }

create(data){


  return this.http_post_auth('/create',data);
}


}