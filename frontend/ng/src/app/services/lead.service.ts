import { Injectable } from '@angular/core';
import {RequestOptions, Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';

@Injectable()
export class LeadService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  createLead(data) {
    return this.http_post_auth('/createLead', data);
  }

  getLeads(data) {
    if (data.id) {
      return this.http_get_auth('/getLeads', data);
    }else {
      return this.http_get_auth('/getLeads');
    }
  }    

  updateLead(data) {
    return this.http_post_auth('/updateLead', data);
  }

  getLead(data) {
    return this.http_post_auth('/getLead', data);
  }

  assignLead(data) {
    return this.http_post_auth('/assignLead', data);
  }

  getLeadsFull() {
    return this.http_get_auth('/getLeadsFull');
  }

  search(data) {
    return this.http_post_auth('/search', data);
  }

  edit(data) {
    return this.http_post_auth('/editLead', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteLead', data);
  }

  

  getLeadStages(data) {
    return this.http_post_auth('/getLeadStages', data);
  }


  getPhoneNumber(data){
    return this.http_post_auth('/getPhoneNumber', data);

  }
  archiveLead(data){
    return this.http_post_auth('/archiveLead', data);

  }
  

}
