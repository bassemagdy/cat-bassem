import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class CarTypesService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  index() {
    return this.http_get_auth('/carTypes');
  }

  all() {
    return this.http_get_auth('/status/all');
  }

  create(data) {
    return this.http_post_auth('/createType', data);
  }

  view(data) {
    return this.http_post_auth('/carType', data);
  }

  edit(data) {
    return this.http_post_auth('/editType', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteType', data);
  }

}
