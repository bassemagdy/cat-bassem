import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';

@Injectable()
export class RoleService extends HttpService {

  constructor( public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  getRole(data) {
    return this.http_get_auth('/getRole', data);
  }

  getPrivileges() {
      return this.http_get_auth('/getPrivileges');
  }

  createRole(data) {
      return this.http_post_auth('/createRole', data);
  }

  getData() {
    return this.http_get_auth('/viewAllRoles');
  }

  paginate(data) {
    return this.http_post_auth('/paginateRoles', data);
  }

  search(data) {
    return this.http_post_auth('/searchRoles', data);
  }

  sort(data) {
    return this.http_post_auth('/sortRoles', data);
  }

  edit(data) {
    return this.http_post_auth('/editRole', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteRole', data);
  }

  countRole(data){
    return this.http_post_auth('/countRole', data);
  }
}
