import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class ZonepricesService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  all() {
    return this.http_get_auth('/allZones');
  }

 
  create(data) {
    return this.http_post_auth('/createzoneprice', data);
  }

  index(){

    return this.http_post_auth('/zoneprices');

  }

  view(data) {
    return this.http_post_auth('/zoneprice', data);
  }

  edit(data) {
    return this.http_post_auth('/editzoneprice', data);
  }

  delete(data) {
    return this.http_post_auth('/deletezoneprice', data);
  }
  paginate(data){
    return this.http_get_auth('/paginatezoneprices');
  }
  check(data){
    return this.http_get_auth('/getprice',data);

  }
  otherzone(data){
    return this.http_get_auth('/otherzone',data);
  }

}
