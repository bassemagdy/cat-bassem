import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class SettingsService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  index() {
    return this.http_post_auth('/settings');
  }

  
  create(data) {
    return this.http_post_auth('/createSettings', data);
  }

  view(data) {
    return this.http_post_auth('/Setting', data);
  }

  edit(data) {
    return this.http_post_auth('/editSettings', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteSettings', data);
  }

}
