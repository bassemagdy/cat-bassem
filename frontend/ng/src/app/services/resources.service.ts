import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class ResourcesService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  index() {
    return this.http_get_auth('/resources');
  }

  all() {
    return this.http_get_auth('/resources/all');
  }

  create(data) {
    return this.http_post_auth('/createResource', data);
  }

  view(data) {
    return this.http_post_auth('/resource', data);
  }

  edit(data) {
    return this.http_post_auth('/editresource', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteresource', data);
  }

}
