import { TestBed, inject } from '@angular/core/testing';

import { DynamictableService } from './dynamictable.service';

describe('DynamictableService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DynamictableService]
    });
  });

  it('should be created', inject([DynamictableService], (service: DynamictableService) => {
    expect(service).toBeTruthy();
  }));
});
