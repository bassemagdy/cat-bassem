import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';

@Injectable()
export class DynamictableService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  createTable(data) {
    return this.http_post_auth('/createTable', data);
  }

  addColumn(data) {
    return this.http_post_auth('/addColumn', data);
  }

  deleteColumn(data) {
    return this.http_post_auth('/deleteColumn', data);
  }


}
