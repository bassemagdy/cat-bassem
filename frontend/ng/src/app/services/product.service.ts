import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';

@Injectable()
export class ProductService extends HttpService {

  constructor( public http: Http, public cookie: CookieService ) {
    super(cookie, http);
  }

  createProduct(data) {
    return this.http_post_auth('/createProduct', data);
  }

  addProductInstance(data) {
    return this.http_post_auth('/addProductInstance', data);

  }

  editProductInstance(data) {
    return this.http_post_auth('/editProductInstance', data);

  }

  editProductCategory(data) {
    return this.http_post_auth('/editProductCategory', data);

  }

  getInstance(data) {
    return this.http_post_auth('/getProductInstance', data);
  }

  getCategory(data) {
    return this.http_post_auth('/getProductCategory', data);
  }

  getAllProducts() {
    return this.http_get_auth('/getAllProducts');
  }

  getAllCategories() {
    return this.http_get_auth('/getAllProductCategories');
  }


  deleteCategory(data) {
    return this.http_post_auth('/deleteProductCategory', data);
  }

  deleteInstance(data) {
    return this.http_post_auth('/deleteProductInstance', data);
  }

  getTotalInstance(data){
    return this.http_post_auth('/total', data);
}


}
