import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class CarsService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

index(){

  return this.http_post_auth('/cars');

}

create(data){
  
  return this.http_post_auth('/createCar',data);
}

view(data){

  return this.http_post_auth('/viewCar',data);

}

edit(data){

  return this.http_post_auth('/editCar',data);

}

delete(data){

  return this.http_post_auth('/deleteCar',data);

}

paginate(data){
  return this.http_post_auth('/paginateCar',data);

  
}


sort(data){
  return this.http_post_auth('/sortCars',data);



}

search(data){
  return this.http_post_auth('/searchcars',data);



}









}