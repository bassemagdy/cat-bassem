import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';
import { HttpService } from './service';

@Injectable()
export class TablesService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }
  getProjectData() {
    return this.http_get_auth('/projectinfra');
  }

  getData() {
    return this.http_get_auth('/');
  }

  paginate(data) {
    return this.http_post_auth('/paginate', data);
  }
  searchbyid(data) {
        return this.http_post_auth('/filterLeads', data);

  }
  search(data) {
    return this.http_post_auth('/search', data);
  }
  searchDates(data) {
    return this.http_post_auth('/searchDate', data);
  }

  sort(data) {
    return this.http_post_auth('/sort', data);
  }

  indexx(data){
    return this.http_get_auth('/indexx', data);


  }

  index(data){
    return this.http_get_auth('/index', data);


  }

}
