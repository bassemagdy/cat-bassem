import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';

@Injectable()
export class SecurityService extends HttpService {

  constructor( public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  checkPrivilage(pid) {

    if (pid === '-1') {
      return true;
    }

    const privilages = this.cookie.get('privilages');
    
    if (this.cookie.get('privilages') === undefined) {
        return false;
    }else if ((privilages.search(',' + pid ) !== -1)||(privilages.search( pid+',' ) !== -1)||(privilages.search(','+pid+',' ) !== -1) || (privilages.search(';' + pid + ';') !== -1)|| (privilages.search(';' + pid) !== -1)||(privilages.search( pid+';' ) !== -1))   {
      // console.log('true');
        return true;
    }else {
      // console.log('false');  
      return false;

    }
  }

}
