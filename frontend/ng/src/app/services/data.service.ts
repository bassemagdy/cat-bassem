import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';

@Injectable()
export class DataService extends HttpService {

  constructor(public http: Http, public cookie: CookieService ) {
    super(cookie, http);
  }

  getTasks() {
    return this.http_get_auth('/taskboard');
  }

  updateTasks(data) {
    return this.http_post_auth('/update', data);
  }

}
