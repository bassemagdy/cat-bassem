import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';
import { Router } from '@angular/router';

@Injectable()
export class StagesService extends HttpService {

  constructor(public http: Http, public cookie: CookieService) {
    super(cookie, http);
  }

  index() {
    return this.http_get_auth('/stages');
  }

  all() {
    return this.http_get_auth('/stages/all');
  }

  create(data) {
    return this.http_post_auth('/createStage', data);
  }

  view(data) {
    return this.http_post_auth('/stage', data);
  }

  edit(data) {
    return this.http_post_auth('/editstage', data);
  }

  delete(data) {
    return this.http_post_auth('/deletestage', data);
  }

  countstage(data) {
    return this.http_post_auth('/countstage', data);
  }

}
