import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { environment } from '../../environments/environment';
import { HttpService } from './service';

@Injectable()
export class CatServiceService extends HttpService {

  constructor( public http: Http, public cookie: CookieService ) {
    super(cookie, http);
  }

  createService(data) {
    return this.http_post_auth('/createService', data);
  }

  getAllCategories(data) {
    return this.http_post_auth('/getAllServiceCategories', data);
  }

  getCategory(data) {
     return this.http_post_auth('/getServiceCategory', data);
  }


  editServiceCategory(data) {
    return this.http_post_auth('/editServiceCategory', data);
  }

  delete(data) {
    return this.http_post_auth('/deleteService', data);
  }

  getPrice(data) {
    return this.http_post_auth('/priceee', data);
  }


}
