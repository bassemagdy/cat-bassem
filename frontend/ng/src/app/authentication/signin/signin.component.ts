import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { SigninService } from '../../services/signin.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent  {

  username;
  password;
  error = '';

  constructor( public cookie: CookieService, public data: SigninService, public router: Router ) {}

  login() {
    this.data.login({username: this.username, password: this.password}).subscribe((reply) => {
      let expireDate = new Date();
      expireDate.setDate(expireDate.getDate() + 0.2);

      this.cookie.put('token', reply.token, {'expires': expireDate});
      this.cookie.put('id', reply.id, {'expires': expireDate});
      this.cookie.put('role', reply.role, {'expires': expireDate});
      this.cookie.put('name', reply.name, {'expires': expireDate});
      this.cookie.put('privilages', reply.privilages, {'expires': expireDate});
      console.log(reply);
      this.router.navigate ( [ '/' ] );
    }, (reply) => {
      if(reply){
      console.log(reply);
      this.error = JSON.parse(reply._body).error;
    }
  });
  }
}
