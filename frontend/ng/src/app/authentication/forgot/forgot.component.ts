import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SigninService } from '../../services/signin.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  email;
  response;
  public form: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private signInService: SigninService) {}

  ngOnInit() {
    this.form = this.fb.group ( {
      uname: [null , Validators.compose ( [ Validators.required ] )]
    } );
  }

  onSubmit() {
    console.log("reply");

    this.signInService.forgotPassword({email: this.email}).subscribe((reply) => {
      this.response = reply;
      console.log(reply);

    })
    
  }
}
