import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { SigninService } from '../../services/signin.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {

  password;
  code;
  response;

  public form: FormGroup;
  constructor(private fb: FormBuilder, private activatedRoute: ActivatedRoute, private router: Router, private signInService: SigninService) {}

  ngOnInit() {
    this.form = this.fb.group ( {
      uname: [null , Validators.compose ( [ Validators.required ] )]
    } );
    this.activatedRoute.params.subscribe((params: Params) => {
        this.code = params['vercode'];
      });
  }

  onSubmit() {
    this.signInService.resetPassword({password: this.password, code: this.code}).subscribe((reply) => {

    console.log(reply);
     if(reply == "okay")
    {
      this.response = "Successful reset. You shall be shortly redirected to the login page.";
      setTimeout(() => {
            this.router.navigate ( [ '/' ] );
          }, 2000);

    }
    else
    {
      this.response = "Invalid.";
    }

    }, (reply) => {
      this.response = "Something went wrong. Please try again later.";
    })
    
  }
}
