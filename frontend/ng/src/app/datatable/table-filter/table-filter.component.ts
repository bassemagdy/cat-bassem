import {
  Component
} from '@angular/core';
import {
  TablesService
} from '../../services/tables.service';


@Component({
  selector: 'app-table-filter',
  templateUrl: './table-filter.component.html',
  styleUrls: ['./table-filter.component.scss']
})
export class TableFilterComponent {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  fulltext = [];
  keyword = '';
  spinner = 0;
  order = "asc";
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;



  constructor(private tablesService: TablesService) {

    this.tablesService.getData().subscribe((reply) => {
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;

      for (var i = 0; i < this.headersType.length; i++) {
        if (this.headersType[i] == "string")
          this.fulltext.push(this.headers[i]);
      }

    });


  }

  onPager(event) {

      if(!isNaN(event))
      {
        if (((Math.floor(event / this.pageSize)) != this.group)) {
        this.tablesService.paginate({
          "page": event,
          "columns": this.headersSearch,
          "keyword": this.keyword,
          "column": this.selectedHeader,
          "order": this.order
        }).subscribe((reply) => {

          this.group = event % this.pageSize;
          this.data = reply;
          this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);

        });
      } 
      else
        this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);
      }
      else
        this.page = 1;
  }

  search() {
    this.spinner = 1;
    this.tablesService.search({
      "columns": this.headersSearch,
      "keyword": this.keyword
    }).subscribe((reply) => {
      console.log(reply);
      this.data = reply.data;
      this.group = 0;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;
      console.log(reply.count.count);
      this.spinner = 0;
    })
  }

  sort(column) {
    if (this.selectedHeader == column && this.order == "asc") {
      this.order = "desc";
      this.asc = 0;
    } else if (this.selectedHeader == column && this.order == "desc") {
      this.order = "asc";
      this.asc = 1;
    } else
      this.selectedHeader = column;

    this.spinner = 1;
    this.tablesService.sort({
      "column": column,
      "order": this.order,
      "keyword": this.keyword,
      "columns": this.headersSearch
    }).subscribe((reply) => {
      this.data = reply;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.spinner = 0;
    })
  }


}
