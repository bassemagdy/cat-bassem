import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  // templateUrl: 'app.component.html',
  styleUrls:['app.component.scss'],
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {


    
  //  mapProp = {
  //   center: new google.maps.LatLng(51.508742, -0.120850),
  //   zoom: 5,
  //   mapTypeId: google.maps.MapTypeId.ROADMAP};
  //  map = new google.maps.Map(document.getElementById("googleMap"), this.mapProp);


  title: string = 'My first AGM project';
  lat: number = 30.0326867;
  lng: number = 31.2380511;
  constructor(translate: TranslateService) {
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang('en');

    const browserLang: string = translate.getBrowserLang();
    translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    // Initialize Firebase

    firebase.initializeApp({
      apiKey: "AIzaSyDbn0bLyNh4emJXjVAKb_BsOyH7ahiY-3U",
      authDomain: "cat-drivers.firebaseapp.com",
      databaseURL: "https://cat-drivers.firebaseio.com",
      projectId: "cat-drivers",
      storageBucket: "cat-drivers.appspot.com",
      messagingSenderId: "444330551162"
    });
    
  }
}
