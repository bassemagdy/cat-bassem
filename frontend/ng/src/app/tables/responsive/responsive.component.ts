import {Router, ActivatedRoute, Params} from '@angular/router';
import {OnInit, Component} from '@angular/core';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-responsive',
  templateUrl: './responsive.component.html',
  styleUrls: ['./responsive.component.scss']
})
export class ResponsiveComponent implements OnInit{
  
  id;
  product;
  details;
  fields = [];
  fieldTypes = [];
  multivalued = [];
  input = [];
  output = [];
  
 constructor(private activatedRoute: ActivatedRoute, public productService:ProductService) {}

  ngOnInit() {
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['name'];
      });

    this.productService.getCategory({id: this.id}).subscribe((reply) => {
      console.log(reply);
      this.product = reply[0].name;
      var definition = reply[0].definition;
      var seperateFields = definition.split(";");
      for(var i = 0; i < seperateFields.length - 1; i++)
      {
          var seperateValues = seperateFields[i].split(":");
          this.fields.push(seperateValues[0]);
          this.fieldTypes.push(seperateValues[1]);
          if(seperateValues.length > 2)
          {
            var values = seperateValues[2].split(',');
            this.multivalued.push(values);
          }            
          else
            this.multivalued.push([]);

      }
    })
  }



  done(){
    
    for(var i = 0; i < this.input.length; i++)
    {
      this.output.push(this.fields[i] + ':' + this.input[i]);
    }
    console.log(this.output);
    this.productService.addProductInstance({ id: this.id, data: this.output }).subscribe((reply) => {
      console.log(reply);

    })
      
  }


}
