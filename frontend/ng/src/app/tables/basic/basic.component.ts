import { Component, Input } from '@angular/core';
import { ProductService } from '../../services/product.service';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
// import {RlTagInputModule} from 'angular2-tag-input';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class BasicComponent { 

  add = [];
  data = [];
  name;
  details;
 
  constructor(private productService: ProductService, private modalService: NgbModal) {}

  showAdd(){
    this.add.push(0);
    this.data[this.add.length-1] = [];

  }

  multiple(i){
    console.log(i);
    if(this.data[i][1] == 'select')
      this.data[i][2] = [];
    
  }

  done(){

    this.productService.createProduct({ name: this.name,
                                        details: this.details,
                                        data: this.data }).subscribe((reply) => {
      console.log(reply);
    })
  }


}



