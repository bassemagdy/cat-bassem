import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import {RlTagInputModule} from 'angular2-tag-input';

import { CommonModule } from '@angular/common';
import { TablesRoutes } from './tables.routing';
import { BasicComponent } from './basic/basic.component';
import { ResponsiveComponent } from './responsive/responsive.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TablesRoutes),
    FormsModule,
    RlTagInputModule
  ],
  declarations: [BasicComponent, ResponsiveComponent]
})

export class TablesModule {}
