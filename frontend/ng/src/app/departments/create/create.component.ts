import { Component, Input } from '@angular/core';
import { DepartmentService } from '../../services/department.service';
import { EmployeeService } from '../../services/employee.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
// import {RlTagInputModule} from 'angular2-tag-input';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  
  department = {
    name: '',
    manager: '',
  };
  managers;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };

  constructor(private router: Router, private cookie: CookieService,private employeeService:EmployeeService, private departmentService: DepartmentService, private modalService: NgbModal) {
    this.employeeService.getData().subscribe((reply) => {
      console.log(reply);
      this.managers = reply.data;
    });
  }

back(){
  this.router.navigate  ( [ '/departments'] );
}


  done() {
    console.log(this.department);
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };
     
    this.departmentService.create({ department: this.department
    }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/departments'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'duplicate department name. Please try another one'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}





