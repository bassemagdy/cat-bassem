import { Component, Input, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { DepartmentService } from '../../services/department.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
// import {RlTagInputModule} from 'angular2-tag-input';

@Component({
  selector: 'app-view-department',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id;
  department = {manager_id: ''};
  managers;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
  manager;

  constructor(private router: Router,private activatedRoute: ActivatedRoute, private cookie: CookieService, private employeeService: EmployeeService,private departmentService: DepartmentService) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });
}

  ngOnInit() {
     this.departmentService.view({id: this.id}).subscribe((reply) => {
      console.log(reply);
      this.department = reply;
      this.employeeService.getData().subscribe((reply) => {
      console.log(reply);
      this.managers = reply.data;
      console.log(this.department);
      for (var i = 0; i < this.managers.length; i++) {
        if (this.managers[i].id == this.department.manager_id) {
          this.manager = this.managers[i].name;
        }
      }
    });
    });
  }

  editDepartment() {
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/departments'] );
  }

  done() {
    console.log(this.department);
    this.departmentService.edit({ department: this.department}).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/departments'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




