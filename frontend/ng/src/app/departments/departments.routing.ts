import { Routes } from '@angular/router';

import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewAll/viewAll.component';
import { ViewComponent } from './view/view.component';

export const DepartmentsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'Create Department'
      }
    },
    {
      path: '',
      component: ViewAllComponent,
      data: {
        heading: 'View All Departments'
      }
    },
    {
      path: 'view/:id',
      component: ViewComponent,
      data: {
        heading: 'View Department'
      }
    }
  ]
  }
];
