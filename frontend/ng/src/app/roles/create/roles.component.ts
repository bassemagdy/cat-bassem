import {
  Component
} from '@angular/core';
import {
  RoleService
} from '../../services/role.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent {

  role="";
  privileges = [];
  priv = [];
  alert: alert = {
      visible: false,
      type: "",
      message: ""
    };
    services=[];
    roles=[];
    leads=[];
    product=[];
    department=[];
  

  constructor(private router: Router, private roleService: RoleService) {
    this.roleService.getPrivileges().subscribe((reply) => {
      console.log(reply);
      this.privileges = reply;

for (var i=1;i<5;i++){

this.services[i]=this.privileges[i];
}
console.log(this.services);

for (var i=5;i<9;i++){
var m=0;
  this.roles[m]=this.privileges[i];
m++;  
}

for (var i=9;i<20;i++){
  var m=0;

    this.leads[m]=this.privileges[i];
}

for (var i=20;i<27;i++){
var m=0;
      this.services[m]=this.privileges[i];
 }

 for (var i=27;i<31;i++){
var m=0;
        this.department[m]=this.privileges[i];
}
   
   
      });

  }
  back(){
    
      this.router.navigate ( [ '/roles'] );
      
     }


  done(){

    if (this.role.length==0){
      this.alert = {
        visible: true,
        type: "danger",
        message: "Something went wrong. Please try again"
      }
      return;
    }
    var name = [];
    var value = [];
    console.log(this.role);

    for(var i = 0; i < this.priv.length; i++)
    {
      if(this.priv[i])
      {
        name.push(this.privileges[i].name+';');
        value.push(this.privileges[i].value);
      }
    }
    
    this.roleService.createRole({ rolename: this.role, names: name, values: value }).subscribe((reply) => {
      this.router.navigate ( [ '/roles'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    });

  
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}

