import {
Component, Input, OnInit
} from '@angular/core';
import {
RoleService
} from '../../services/role.service';
import {
  NgbModal,
  NgbActiveModal,
  ModalDismissReasons,
  NgbTimeStruct
} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { SecurityService } from '../../services/security.service';

@Component({
selector: 'app-roles-viewall',
templateUrl: './viewAll.component.html',
styleUrls: ['./viewAll.component.scss']
})
export class ViewAllComponent {
  rows = [];
  data = [];
  headers = [];
  headersSearch = [];
  headersType = [];
  fulltext = [];
  keyword = '';
  spinner = 0;
  order = "asc";
  asc = 1;
  selectedHeader;
  collectionSize;
  pageSize;
  page;
  group;
  limit;
  noemployees;



  constructor(
    private router: Router,
    private roleService: RoleService,
    public securityService: SecurityService,
    public modalService: NgbModal
  ) {

    this.roleService.getData().subscribe((reply) => {
      this.headers = reply.headers;
      this.headersType = reply.headersType;
      this.data = reply.data;
      this.limit = reply.limit;
      this.group = 0;
      this.page = 1;
      this.pageSize = reply.pageSize;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;

      for (var i = 0; i < this.headersType.length; i++) {
        if (this.headersType[i] == "string")
          this.fulltext.push(this.headers[i]);
      }

    });


  }

  create() {
    this.router.navigate ( [ '/roles/create'] );
  }

  view(role) {
    console.log(role);
    const modalRef = this.modalService.open(ModalViewRole);
    modalRef.componentInstance.role = role;
    modalRef.result.then((result) => {}, (result) => {});
  }

  delete(id){
    if(confirm('Are you sure you want to delete')) {
      this.roleService.countRole({id:id}).subscribe((reply)=>{

console.log(reply);
this.noemployees=reply;
    this.roleService.getRole({id :id}).subscribe((reply)=>{
      console.log(reply);
      console.log("mokhtar");

    })

      if (this.noemployees==0){
        console.log("tmam");
        
        this.roleService.delete({ delete:  id}).subscribe((reply) => {
          console.log(reply);
          for(var i = 0; i < this.rows.length; i++)
            {
              if(this.rows[i].id == id)
                this.rows.splice(i,1);
            }
        })

return;

      }  
      const modalRef = this.modalService.open(ModalDeleteRole);
      modalRef.componentInstance.roles = this.rows;
      modalRef.componentInstance.del = id;
        
      modalRef.result.then((result) => {
        this.roleService.delete({replacement: result, delete:  id}).subscribe((reply) => {
          console.log(reply);
          for(var i = 0; i < this.rows.length; i++)
            {
              if(this.rows[i].id == id)
                this.rows.splice(i,1);
            }
        })
      }, (result) => {});
    })
    }
  }

  onPager(event) {

      if(!isNaN(event))
      {
        if (((Math.floor(event / this.pageSize)) != this.group)) {
        this.roleService.paginate({
          "page": event,
          "columns": this.headersSearch,
          "keyword": this.keyword,
          "column": this.selectedHeader,
          "order": this.order
        }).subscribe((reply) => {

          this.group = event % this.pageSize;
          this.data = reply;
          this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);

        });
      } 
      else
        this.rows = this.data.slice(((event - 1) % this.pageSize) * this.pageSize, (((event - 1) % this.pageSize) * this.pageSize) + this.pageSize);
      }
      else
        this.page = 1;
  }

  search() {
    this.spinner = 1;
    this.roleService.search({
      "columns": this.headersSearch,
      "keyword": this.keyword
    }).subscribe((reply) => {
      console.log(reply);
      this.data = reply.data;
      this.group = 0;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.collectionSize = reply.count.count;
      console.log(reply.count.count);
      this.spinner = 0;
    })
  }

  sort(column) {
    if (this.selectedHeader == column && this.order == "asc") {
      this.order = "desc";
      this.asc = 0;
    } else if (this.selectedHeader == column && this.order == "desc") {
      this.order = "asc";
      this.asc = 1;
    } else
      this.selectedHeader = column;

    this.spinner = 1;
    this.roleService.sort({
      "column": column,
      "order": this.order,
      "keyword": this.keyword,
      "columns": this.headersSearch
    }).subscribe((reply) => {
      this.data = reply;
      this.page = 1;
      this.rows = this.data.slice(this.page - 1, this.pageSize);
      this.spinner = 0;
    })
  }




}

@Component({
  selector: 'modal-view-role',
  templateUrl: 'modalViewRole.html',
  styleUrls:['./viewAll.component.scss']
})
export class ModalViewRole implements OnInit {

  @Input() role;
  deleted = [];
  added = [];
  privileges = [];
  edit = false;
  date;
  priv;
  
  

  constructor(public activeModal: NgbActiveModal, public roleService: RoleService) {
  }

  ngOnInit() {
    if (!(this.role.privileges instanceof Array)) {
      this.role.privileges = this.role.privileges.split(';');
      this.role.privileges.pop();
      console.log(this.role.privileges);
    }

    let result = [];

    this.roleService.getPrivileges().subscribe((reply) => {
      this.privileges = reply;
      for (let i = 0; i < this.privileges.length; i++) {
        if (this.role.privileges.indexOf(this.privileges[i].name) === -1) {
          result.push(this.privileges[i]);
        }
      }

      this.privileges = result;

    });
  }

  add(){
    if(this.priv && this.role.privileges.indexOf(this.priv) < 0){
      this.role.privileges.push(this.priv);
      this.added.push(this.priv);
      console.log(this.added)
      for(var i = 0; i < this.privileges.length; i++)
      {
        if(this.privileges[i].name == this.priv)
        {
          this.privileges.splice(i, 1);
        }

      }
      this.priv = null;
    }
  }

  close() {
    this.activeModal.close({});
  }

  editRole() {
    if (this.role.name==null)
{
return;
}
 this.edit = !this.edit;
    console.log(this.deleted);
    if(!this.edit){
      var privileges = this.role.privileges.toString().replace(/,/g, ';') + ';';
      this.roleService.edit({value: this.role.value, name: this.role.name, id: this.role.id, deleted: this.deleted, added: this.added, privileges: privileges }).subscribe((reply) => {
        console.log(reply);
      });
    }
  }

  remove(privilege){
    var i = this.role.privileges.indexOf(privilege);
    this.privileges.push({name:privilege, value:"0"});
    if(i> -1)
    {
      this.deleted.push(this.role.privileges.splice(i,1)[0]);
    }
      
  }

}


@Component({
  selector: 'modal-delete-role',
  templateUrl: 'modalDeleteRole.html',
  styleUrls:['./viewAll.component.scss']
})
export class ModalDeleteRole implements OnInit {

  @Input() roles;
  @Input() del;
  role;
  viewroles = [];

  constructor(public activeModal: NgbActiveModal, public roleService: RoleService) {
  }

  ngOnInit(){
    for(var i = 0; i < this.roles.length; i++)
    {
      if(this.roles[i].id != this.del)
        this.viewroles.push(this.roles[i]);
    }
  }


  close() {
    console.log(this.role);
    this.activeModal.close(this.role);
  }




}

