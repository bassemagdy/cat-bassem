import { Routes } from '@angular/router';

import { RolesComponent } from './create/roles.component';
import { ViewAllComponent } from './viewAll/viewAll.component';

export const RolesRoutes: Routes = [{
  path:'',
  children: [{
  path: 'create',
  component: RolesComponent,
  data: {
    heading: 'Create Role',
    removeFooter: true
  }
}, {
  path: '',
  component: ViewAllComponent,
  data: {
    heading: 'View All Roles',
    removeFooter: true
  }
}]}];
