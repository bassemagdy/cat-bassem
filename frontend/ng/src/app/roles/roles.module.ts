import { NgModule, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {  NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RolesComponent } from './create/roles.component';
import { ViewAllComponent } from './viewAll/viewAll.component';
import { RolesRoutes } from './roles.routing';

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(RolesRoutes),
            FormsModule,
            ReactiveFormsModule,
            JsonpModule,
            NgbModule],
            
  declarations: [RolesComponent,ViewAllComponent]
})

export class RolesModule {}
