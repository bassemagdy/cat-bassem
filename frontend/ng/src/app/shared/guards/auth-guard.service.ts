import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

   constructor( private router: Router) {}

  canActivate() {
    let cookie: CookieService = new CookieService;
    if (cookie.get('token') == undefined) {
        this.router.navigate ( [ '/authentication/signin'  ] );
        return false;
    }else {
        return true;
    }
  }

  canActivateChild() {
    let cookie: CookieService = new CookieService;
    if (cookie.get('token') == undefined) {
        this.router.navigate ( [ '/authentication/signin'  ] );
        return false;
    }else {
        return true;
    }
  }
}
