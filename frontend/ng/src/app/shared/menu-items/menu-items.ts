import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  privilage: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '/',
    name: 'HOME',
    type: 'link',
    icon: 'basic-accelerator',
    privilage: '1'
  },
   {
    state: 'products',
    name: 'Products',
    type: 'sub',
    icon: 'basic-webpage-multiple',
    privilage: '2',
    badge: [
      {
        type: 'primary',
        value: '3'
      }
    ],
    children: [
      {
        state: 'viewCategories',
        name: 'View Product Types'
      },
      {
        state: 'viewInstances',
        name: 'View Products'
      },
    ]
  },
  {
    state: 'services',
    name: 'Services',
    type: 'link',
    icon: 'basic-webpage-txt',
    privilage: '3'
  },
   {
    state: 'leads',
    name: 'Leads',
    type: 'link',
    icon: 'ecommerce-graph1',
    privilage: '4'
  },
  {
    state: 'departments',
    name: 'Departments',
    type: 'link',
    icon: 'basic-spread-text-bookmark',
    privilage: '5'
  },
  {
    state: 'roles',
    name: 'Roles',
    type: 'link',
    icon: 'basic-lock',
    privilage: '6'
  },
  {
    state: 'resources',
    name: 'Marketing Channels',
    type: 'link',
    icon: 'ecommerce-graph2',
    privilage: '7'
  },
  {
    state: 'stages',
    name: 'Lead Stages',
    type: 'link',
    icon: ' icon-list',
    privilage: '8'
  },
  {
    state: 'employees',
    name: 'Employees',
    type: 'link',
    icon: ' icon-people',
    privilage: '9'
  },
  {
    state: 'payments',
    name: 'Payments',
    type: 'link',
    icon: ' fa fa-money',
    privilage: '10'
  },
  {
    state: 'customers',
    name: 'Customers',
    type: 'link',
    icon: ' icon-people',
    privilage: '11'
  },
  {
    state: 'cars',
    name: 'Cars',
    type: 'link',
    icon: ' fa fa-car',
    privilage: '12'
  },
 
  {
    state: 'trips',
    name: 'Trips',
    type: 'sub',
    icon: ' fa fa-plane',
    privilage: '13',
    badge: [
      {
        type: 'primary',
        value: '3'
      }
    ],
    children: [
      {
        state: 'trips',
        name: 'Trips'
      },
      {
        state: 'booktrip',
        name: 'Book a trip'
      },
      {
        state:'airport',
        name:'From Airport'
      },
      {
        state: 'zone',
        name: 'All Zones'
      },
      {
        state:'addzone',
        name:'Add New Zone'
      },
      {
        state:'zone-prices',
        name:'Zone Price'
      },
      
      {
        state:'cars-map',
        name:'Cars Map'
      }
    ]
  },

  {
    state: 'cartypes',
    name: 'Car Types',
    type: 'link',
    icon: ' fa fa-car',
    privilage: '14'
  },


  {
    state: 'status',
    name: 'Status',
    type: 'link',
    icon: ' icon-check',
    privilage: '14'
  },

  {
    state: 'drivers',
    name: 'Drivers',
    type: 'link',
    icon: ' icon-people',
    privilage: '15'
  },

  {
    state: 'settings',
    name: 'Settings',
    type: 'link',
    icon: ' icon-options',
    privilage: '16'
  },
  {
    state: 'logout',
    name: 'Log Out',
    type: 'link',
    icon: ' icon-logout',
    privilage: '-1'
  }
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
