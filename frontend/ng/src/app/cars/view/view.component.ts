import { Component, Input, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { DepartmentService } from '../../services/department.service';
import { CarsService } from '../../services/car.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service'; 
// import {RlTagInputModule} from 'angular2-tag-input';
import { StatusService } from '../../services/car-status.service';
import { CarTypesService } from '../../services/car-types.service';

@Component({
  selector: 'app-view-car',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  
  id;

  car = {
    number: "",
    type: "",
    validUntil: "",
    licenseNumber: "",
    initialKilometers: "",
    carModel:"",
    chassisNumber:"",
    status:""
    
  };

statusname;
state;
  edit = false;
  alert: alert = {
      visible: false,
      type: "",
      message: ""
    };
types=[];
 
  constructor(private router: Router,private activatedRoute: ActivatedRoute, private cookie: CookieService, private carsService:CarsService, private modalService: NgbModal, public statusService : StatusService,  public carTypesService: CarTypesService) {
    
    this.activatedRoute.params.subscribe((params: Params) => {

        this.id = params['id'];
    });

    this.statusService.index().subscribe((reply) => {
      this.state = reply.data;
      console.log("/////");
      console.log(reply.data);
    });
    this.carTypesService.index().subscribe((reply) => {
      this.types = reply.data;

    });
    
  }

  ngOnInit(){
      console.log(this.id);
     this.carsService.view({id: this.id}).subscribe((reply) => {
      console.log(reply);
      this.car = reply;
    
     
      });
   
  }

  editCar(){
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/cars'] );
  }


  done(){
    console.log(this.car);
    this.car.status=this.statusname;
    this.carsService.edit({ car: this.car}).subscribe((reply) => {
         let carRef = firebase.database().ref("/map/0/"+this.id);
         carRef.set({
          id: this.id,
          lat: 30.1124,
          lng: 31.4003,
          occupied:this.car.status
        });
      console.log(reply);
      
      this.router.navigate ( [ '/cars'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    })
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




