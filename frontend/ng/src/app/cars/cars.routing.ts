import { Routes } from '@angular/router';

import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewall/viewall.component';
import { ViewComponent } from './view/view.component';

export const CarsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'create',
      component: CreateComponent,
      data: {
        heading: 'Create Car'
      }
    },
    {
      path: '',
      component: ViewAllComponent,
      data: {
        heading: 'View All Cars'
      }
    },
    {
      path: 'view/:id',
      component: ViewComponent,
      data: {
        heading: 'View Car'
      }
    }
  ]
  }
];