import { Component, Input } from '@angular/core';
import { StatusService } from '../../services/car-status.service';
import { DepartmentService } from '../../services/department.service';
import { CarsService } from '../../services/car.service';
import { CarTypesService } from '../../services/car-types.service'
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
// import {RlTagInputModule} from 'angular2-tag-input';
import * as firebase from 'firebase';


@Component({
  selector: 'app-create-cars',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  car = {
    number: "",
    validUntil: "",
    licenseNumber: "",
    initialKilometers: "",
    carModel:"",
    chassisNumber:"",
    status:"",
    car_type_id:0,
  };

  statusname;
  types;
  state;
  alert: alert = {
      visible: false,
      type: "",
      message: ""
    };
   
  
 
  constructor(private router: Router,private cookie: CookieService, private carsService: CarsService, private modalService: NgbModal , public statusService : StatusService , private typesService : CarTypesService) {


    this.statusService.index().subscribe((reply) => {
      this.state = reply.data;
      console.log("/////");
      console.log(reply.data);
    });

    this.typesService.index().subscribe((reply) => {
      this.types = reply.data;
      console.log(reply.data);

    });

  }

 back(){
  this.router.navigate ( [ '/cars'] );
 } 



  done(){
    console.log(this.car);
    let id=[];
    let status=[];

    this.alert = {
            visible: true,
            type: "info",
            message: "Please wait - this may take a while"
          }
    this.car.status= this.statusname;
    this.carsService.create({ car: this.car }).subscribe((reply) => {
     
      this.carsService.index().subscribe((repo) => {
        if(reply){
        console.log(repo.data[repo.data.length-1].id);
        id.push(repo.data[repo.data.length-1].id);
        status.push(repo.data[repo.data.length-1].status);
        console.log(repo.data[repo.data.length-1].status);
        let password = reply;
        let driversRef = firebase.database().ref("/map/0/"+id[0]);
        driversRef.set({
          id: id[0],
          lat: 30.1124,
          lng: 31.4003,
          occupied:status[0]
        });
      }
    })
      console.log(reply);
      this.router.navigate ( [ '/cars'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: "danger",
            message: "Something went wrong. Please try again"
          }

    })
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}


  export interface From {
      name: string;
      email: string;
  }

  export interface To {
      name: string;
      email: string;
  }

  export interface Bcc {
      name: string;
      email: string;
  }

  interface RootObject {
      html: string;
      text: string;
      subject: string;
      from: string;
      to: string;
      bcc: string;
  }