
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RlTagInputModule } from 'angular2-tag-input';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { TagInputModule } from 'ngx-chips';
import {  NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { CarsRoutes } from './cars.routing';
import { CreateComponent } from './create/create.component';
import { ViewAllComponent } from './viewall/viewall.component';
import { ViewComponent } from './view/view.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CarsRoutes),
    FormsModule,
    RlTagInputModule,
    NgbCollapseModule,
    TagInputModule,
    ReactiveFormsModule,
    NgbModule
  ],
  declarations: [CreateComponent, ViewAllComponent, ViewComponent]
   

})

export class CarsModule {}
