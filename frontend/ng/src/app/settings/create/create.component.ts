import { Component, Input } from '@angular/core';
import { DriversService } from '../../services/drivers.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {  NgbModal, NgbActiveModal, ModalDismissReasons, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import {SettingsService} from '../../services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  
    settings = {
    name: '',
    value:'',
    type:'',
   


  };

  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };

  constructor(private router: Router, private cookie: CookieService, 
    private settingsService: SettingsService, 
    private modalService: NgbModal , 
    public driversService : DriversService
  ) {
 
  }
back(){
  this.router.navigate( [ '/settings']);


}



  done() {
    console.log(this.settings);
    this.alert = {
            visible: true,
            type: 'info',
            message: 'Please wait - this may take a while'
          };
    this.settingsService.create({ settings: this.settings
    }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/settings/'] );
    }, (reply) => {
      console.log(reply);
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}





