import { Component, Input, OnInit } from '@angular/core';
import {DriversService} from '../../services/drivers.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {SettingsService} from '../../services/settings.service';



@Component({
  selector: 'app-view-settings',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  id;
  edit = false;
  alert: alert = {
      visible: false,
      type: '',
      message: ''
    };
  settings;

  constructor(private router: Router,private activatedRoute: ActivatedRoute,
     private cookie: CookieService,
     public driversService : DriversService,
     private settingsService: SettingsService, 

    ) {
    this.activatedRoute.params.subscribe((params: Params) => {
        this.id = params['id'];
    });
}

  ngOnInit() {
     this.settingsService.view({id: this.id}).subscribe((reply) => {
     console.log(this.id);
      console.log(reply);
      this.settings = reply;
      console.log("/////////////");
      console.log(this.settings);
    });
  }

  editSettings() {
    this.edit = !this.edit;
  }

  back(){
    this.router.navigate  ( [ '/settings/'] );
  }


  done() {
    console.log(this.settings);
    this.driversService.edit({ 
        
    name:this.settings.name ,
    value:this.settings.value,
    type: this.settings.type,
    id:this.settings.id }).subscribe((reply) => {
      console.log(reply);
      this.router.navigate ( [ '/settings/'] );
    }, (reply) => {
      this.alert = {
            visible: true,
            type: 'danger',
            message: 'Something went wrong. Please try again'
          };
    });
  }


}

interface alert {
  visible: boolean,
  type: string,
  message: string
}




