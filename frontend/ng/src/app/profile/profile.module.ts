import { NgModule, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import {  NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { ProfileComponent } from './profile.component';
import { ProfileRoutes } from './profile.routing';

@NgModule({
  imports: [CommonModule,
            RouterModule.forChild(ProfileRoutes),
            FormsModule,
            ReactiveFormsModule,
            JsonpModule,
            NgbModule],
            
  declarations: [ProfileComponent]
})

export class ProfileModule {}
