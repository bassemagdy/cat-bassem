import {Component, OnInit} from '@angular/core';
import {TablesService} from '../services/tables.service';
import { EmployeeService } from '../services/employee.service';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/services/cookies.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent  {

  id;
  employee = {};
  edit;

  constructor(public cookie: CookieService, public employeeService: EmployeeService,private router: Router) {
    this.id = this.cookie.get('id');
    this.employeeService.getEmployee({id: this.id}).subscribe((reply) => {
      this.employee = reply;
    });
  }

  editEmployee() {
    this.edit = !this.edit;
  }

back(){
    
      this.router.navigate ( [ '/'] );
      
     }
  done() {
    this.employeeService.editProfile({ employee: this.employee}).subscribe((reply) => {
      console.log(reply);
      this.edit = !this.edit;
    }, (reply) => {

    });
  }
}
