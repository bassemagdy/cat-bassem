<?php

use App\Mail\RegistrationMailer;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/taskboard', [
   'uses' => 'TaskController@index'
]);

Route::post('/update','TaskController@update');

Route::options('*',[
   'uses' => 'TaskController@preflight'
]);


Route::get('/', [
   'uses' => 'TablesController@index'
]);


Route::post('/searchcars', [
    'uses' => 'CarController@search'
 ]);
Route::get('/indexx', [
    'uses' => 'TablesController@indexx'
 ]);

Route::post('/paginate', [
   'uses' => 'TablesController@paginate'
]);

Route::post('/search', [
   'uses' => 'TablesController@search'
]);
Route::post('/searchDate', [
    'uses' => 'TablesController@searchDate'
 ]);

Route::post('/sort', [
   'uses' => 'TablesController@sort'
]);


Route::post('/createTable', [
   'uses' => 'DynamicTablesController@createTable'
]);


Route::post('/addColumn', [
   'uses' => 'DynamicTablesController@addColumn'
]);


Route::post('/deleteColumn', [
   'uses' => 'DynamicTablesController@deleteColumn'
]);


Route::post('/addRow', [
   'uses' => 'DynamicTablesController@addRow'
]);

Route::post('/login', [
   'uses' => 'LoginController@login'
]);

Route::get('/login', [
   'uses' => 'LoginController@login'
]);

Route::post('/createProduct', [
   'uses' => 'ProductController@createProduct'
]);

Route::post('/addProductInstance', [
   'uses' => 'ProductController@addProductInstance'
]);

Route::post('/editProductInstance', [
   'uses' => 'ProductController@editProductInstance'
]);

Route::post('/editProductCategory', [
   'uses' => 'ProductController@editProductCategory'
]);

Route::post('/deleteProductCategory', [
   'uses' => 'ProductController@deleteCategory'
]);

Route::post('/deleteProductInstance', [
   'uses' => 'ProductController@deleteInstance'
]);

Route::get('/getAllProducts', [
   'uses' => 'ProductController@getAllProducts'
]);

Route::get('/getAllProductCategories', [
   'uses' => 'ProductController@getAllProductCategories'
]);

Route::post('/createService', [
   'uses' => 'ServiceController@createService'
]);

Route::post('/getAllServiceCategories', [
   'uses' => 'ServiceController@getAllServiceCategories'
]);

Route::post('/editServiceCategory', [
   'uses' => 'serviceController@editServiceCategory'
]);

Route::post('/deleteService', [
   'uses' => 'ServiceController@deleteCategory'
]);

Route::post('/priceee', [
    'uses' => 'ServiceController@getPrice'
]);



Route::post('/createLead', [
   'uses' => 'LeadController@createLead'
]);

Route::get('/getLeads', [
   'uses' => 'LeadController@getLeads'
]);

Route::get('/getAppLeads', [
   'uses' => 'LeadController@getAppLeads'
]);

Route::post('/updateLead', [
   'uses' => 'LeadController@updateLead'
]);

Route::post('/getLead', [
   'uses' => 'LeadController@getLead'
]);

Route::post('/deleteLead', [
   'uses' => 'LeadController@deleteLead'
]);

Route::post('/getEmployeesOfManager', [
   'uses' => 'EmployeeController@getEmployeesOfManager'
]);

Route::post('/assignLead', [
   'uses' => 'LeadController@assignLead'
]);

Route::get('/getLeadsFull', [
   'uses' => 'LeadController@getLeadsFull'
]);

Route::post('/editLead', [
   'uses' => 'LeadController@edit'
]);

Route::post('/getLeadStages', [
    'uses' => 'LeadController@getLeadStages'
 ]);

 Route::post('/getLeads', [
    'uses' => 'LeadController@getLeads'
 ]);


// Route::post('/search', [
//    'uses' => 'LeadController@search'
// ]);

Route::post('/getProductCategory', [
   'uses' => 'ProductController@getCategory'
]);

Route::post('/getServiceCategory', [
   'uses' => 'ServiceController@getCategory'
]);

Route::post('/getProductInstance', [
   'uses' => 'ProductController@getInstance'
]);

Route::get('/getRole', [
    'uses' => 'RoleController@get'
]);

Route::get('/getPrivileges', [
   'uses' => 'RoleController@getPrivileges'
]);

Route::post('/createRole', [
   'uses' => 'RoleController@createRole'
]);

Route::get('/viewAllRoles', [
   'uses' => 'RoleController@viewAllRoles'
]);

Route::post('/countRole', [
    'uses' => 'RoleController@viewcount'
 ]);

Route::post('/paginateRoles', [
   'uses' => 'RoleController@paginate'
]);

Route::post('/searchRoles', [
   'uses' => 'RoleController@search'
]);

Route::post('/sortRoles', [
   'uses' => 'RoleController@sort'
]);

Route::post('/editRole', [
   'uses' => 'RoleController@edit'
]);

Route::post('/deleteRole', [
   'uses' => 'RoleController@delete'
]);

Route::post('/createEmployee', [
   'uses' => 'EmployeeController@create'
]);

Route::get('/viewAllEmployees', [
   'uses' => 'EmployeeController@viewAllEmployees'
]);

Route::post('/paginateEmployees', [
   'uses' => 'EmployeeController@paginate'
]);

Route::post('/searchEmployees', [
   'uses' => 'EmployeeController@search'
]);

Route::post('/sortEmployees', [
   'uses' => 'EmployeeController@sort'
]);

Route::post('/getEmployee', [
   'uses' => 'EmployeeController@getEmployee'
]);

Route::post('/editEmployee', [
   'uses' => 'EmployeeController@editEmployee'
]);

Route::post('/editProfile', [
    'uses' => 'EmployeeController@editProfile'
 ]);

Route::post('/deleteEmployee', [
   'uses' => 'EmployeeController@deleteEmployee'
]);


Route::get('/email', [
    'uses' => 'EmployeeController@email'
]);

Route::post('/forgotPassword', [
    'uses' => 'LoginController@forgotPassword'
]);

Route::post('/resetPassword', [
    'uses' => 'LoginController@resetPassword'
]);

Route::post('/createDepartment', [
    'uses' => 'DepartmentController@create'
]);

Route::post('/editDepartment', [
    'uses' => 'DepartmentController@edit'
]);

Route::post('/deleteDepartment', [
    'uses' => 'DepartmentController@delete'
]);

Route::get('/departments', [
    'uses' => 'DepartmentController@index'
]);

Route::get('/departments/all', [
    'uses' => 'DepartmentController@all'
]);

Route::post('/department', [
    'uses' => 'DepartmentController@view'
]);

Route::post('/createResource', [
    'uses' => 'ResourcesController@create'
]);

Route::post('/editresource', [
    'uses' => 'ResourcesController@edit'
]);

Route::get('/resources/all', [
    'uses' => 'ResourcesController@all'
]);

Route::get('/resources', [
    'uses' => 'ResourcesController@index'
]);

Route::post('/deleteresource', [
    'uses' => 'ResourcesController@delete'
]);

Route::post('/resource', [
    'uses' => 'ResourcesController@view'
]);

Route::post('/disableEmployee', [
    'uses' => 'EmployeeController@disableEmployee'
]);


Route::post('/notification', [
    'uses' => 'NotificationController@view'
]);


Route::post('/createStage', [
    'uses' => 'StageController@create'
]);

Route::post('/editstage', [
    'uses' => 'StageController@edit'
]);

Route::get('/stages/all', [
    'uses' => 'StageController@all'
]);

Route::get('/stages', [
    'uses' => 'StageController@index'
]);

Route::post('/deletestage', [
    'uses' => 'StageController@delete'
]);

Route::post('/countstage', [
    'uses' => 'StageController@viewcount'
]);

Route::post('/stage', [
    'uses' => 'StageController@view'
]);

Route::post('/sendmail', [
    'uses' => 'EmployeeController@sendmail'
]);


Route::post('/total', [
    'uses' => 'ProductController@getTotalInstance'
]);

Route::post('/createpayment', [
    'uses' => 'PaymentsController@create'
]);


Route::post('/editpayment', [
    'uses' => 'PaymentsController@edit'
]);

Route::get('/payments/all', [
    'uses' => 'PaymentsController@all'
]);

Route::get('/payments', [
    'uses' => 'PaymentsController@index'
]);

Route::post('/deletepayment', [
    'uses' => 'PaymentsController@delete'
]);

Route::post('/payment', [
    'uses' => 'PaymentsController@view'
]);

Route::post('/customers', [
    'uses' => 'CustomerController@viewAll'
]);

Route::post('/paginateCustomer', [
    'uses' => 'CustomerController@paginate'
]);

Route::post('/deleteCustomer', [
    'uses' => 'CustomerController@delete'
]);

Route::post('/create', [
    'uses' => 'CustomerController@addCustomer'
]);

Route::post('/getPhoneNumber', [
    'uses' => 'LeadController@getPhoneNumber'
]);

Route::post('/archiveLead', [
    'uses' => 'LeadController@archiveLead'
]);

Route::post('/viewCustomer', [
    'uses' => 'CustomerController@viewCustomer'
]);

Route::post('/updateCustomer', [
    'uses' => 'CustomerController@update'
]);
//////carRoutes////////
Route::post('/cars', [
    'uses' => 'CarController@index'
]);
Route::post('/editCar', [
    'uses' => 'CarController@edit'
]);
Route::post('/createCar', [
    'uses' => 'CarController@create'
]);
Route::post('/deleteCar', [
    'uses' => 'CarController@delete'
]);

Route::post('/viewCar', [
    'uses' => 'CarController@view'
]);

Route::post('/paginateCar', [
    'uses' => 'CarController@paginate'
]);

Route::post('/sortCars', [
    'uses' => 'CarController@sort'
]);
/////////// trips////////

Route::post('/trips', [
    'uses' => 'TripController@index'
]);
Route::post('/cancelTrip', [
    'uses' => 'TripController@cancelTrip'
]);

Route::post('/paginateTrip', [
    'uses' => 'TripController@paginate'
]);


Route::post('/sortTrip', [
    'uses' => 'TripController@sort'
]);

Route::post('/booking', [
    'uses' => 'TripController@Booking'
]);

Route::post('/geo', [
    'uses' => 'TripController@geo'
]);

Route::post('/getAddress', [
    'uses' => 'TripController@getAddress'
]);

//////////////////status///////////////////

Route::post('/editStatus', [
    'uses' => 'StatusController@edit'
]);

Route::get('/status/all', [
    'uses' => 'StatusController@index'
]);

Route::post('/createStatus', [
    'uses' => 'StatusController@create'
]);

Route::post('/deleteStatus', [
    'uses' => 'StatusController@delete'
]);

Route::post('/status', [
    'uses' => 'StatusController@view'
]);

/////////////////////car_types////////////////
Route::get('/carTypes', [
    'uses' => 'CartypeController@index'
]);

Route::post('/createType', [
    'uses' => 'CartypeController@create'
]);

Route::post('/editType', [
    'uses' => 'CartypeController@edit'
]);

Route::post('/deleteType', [
    'uses' => 'CartypeController@delete'
]);

Route::post('/carType', [
    'uses' => 'CartypeController@view'
]);

/////////////////////Driver////////////////

Route::get('/drivers', [
    'uses' => 'DriverController@index'
]);

Route::post('/createDriver', [
    'uses' => 'DriverController@create'
]);

Route::post('/editDriver', [
    'uses' => 'DriverController@edit'
]);

Route::post('/deleteDriver', [
    'uses' => 'DriverController@delete'
]);

Route::post('/driver', [
    'uses' => 'DriverController@view'
]);


////////////////zone//////////////////



Route::post('/allZones', [
    'uses' => 'ZoneController@index'
]);


Route::post('/createzoneprice', [
    'uses' => 'ZonepricingController@create'
]);


Route::post('/zoneprices', [
    'uses' => 'ZonepricingController@index'
]);
Route::get('/paginatezoneprices', [
    'uses' => 'ZonepricingController@paginate'
]);
Route::get('/getprice', [
    'uses' => 'ZonepricingController@getprice'
]);
Route::post('/editzoneprice', [
    'uses' => 'ZonepricingController@edit'
]);

Route::post('/zoneprice', [
    'uses' => 'ZonepricingController@view'
]);
Route::get('/otherzone', [
    'uses' => 'ZonepricingController@getOtherZone'
]);
Route::post('/deletezoneprice', [
    'uses' => 'ZonepricingController@delete'
]);



Route::post('/zones', [
    'uses' => 'ZoneController@index'
]);
Route::post('/editZone', [
    'uses' => 'ZoneController@edit'
]);
Route::post('/createZone', [
    'uses' => 'ZoneController@create'
]);
Route::post('/deleteZone', [
    'uses' => 'ZoneController@delete'
]);

Route::post('/viewZone', [
    'uses' => 'ZoneController@view'
]);
Route::post('/paginateZone', [
    'uses' => 'ZoneController@paginate'
]);
/////////////////////////////


////////////////////settings///////////////////

Route::post('/settings', [
    'uses' => 'SettingsController@index'
]);

Route::post('/createSettings', [
    'uses' => 'SettingsController@create'
]);

Route::post('/editSettings', [
    'uses' => 'SettingsController@edit'
]);

Route::post('/deleteSettings', [
    'uses' => 'SettingsController@delete'
]);

Route::post('/Setting', [
    'uses' => 'SettingsController@view'
]);
