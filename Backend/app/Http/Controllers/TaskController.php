<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $tasks = array("start" => array((object) ["id" => "1", "text" => "Lead1"],
            (object) ["id" => "2", "text" => "Lead2"],
            (object) ["id" => "3", "text" => "Lead3"],
            (object) ["id" => "4", "text" => "Lead4"],
            (object) ["id" => "5", "text" => "Lead5"],
            (object) ["id" => "6", "text" => "Lead6"],
            (object) ["id" => "7", "text" => "Lead7"],
            (object) ["id" => "8", "text" => "Lead8"],
            (object) ["id" => "9", "text" => "Lead9"]),
            "middle" => array((object) ["id" => "4", "text" => "Lead4"],
                (object) ["id" => "10", "text" => "Lead6"],
                (object) ["id" => "11", "text" => "Lead6"],
                (object) ["id" => "12", "text" => "Lead2"],
                (object) ["id" => "13", "text" => "Lead2"],
                (object) ["id" => "14", "text" => "Lead2"],
                (object) ["id" => "15", "text" => "Lead2"],
                (object) ["id" => "16", "text" => "Lead2"],
                (object) ["id" => "22", "text" => "Lead2"],
                (object) ["id" => "92", "text" => "Lead2"],
                (object) ["id" => "872", "text" => "Lead2"]),
            "end" => array((object) ["id" => "7", "text" => "Lead7"],
                (object) ["id" => "88", "text" => "Lead8"],
                (object) ["id" => "95", "text" => "Lead9"],
                (object) ["id" => "26", "text" => "Lead2"],
                (object) ["id" => "255", "text" => "Lead2"],
                (object) ["id" => "24", "text" => "Lead2"],
                (object) ["id" => "122", "text" => "Lead2"],
                (object) ["id" => "132", "text" => "Lead2"],
                (object) ["id" => "52", "text" => "Lead2"],
                (object) ["id" => "982", "text" => "Lead2"],
                (object) ["id" => "552", "text" => "Lead2"],
                (object) ["id" => "112", "text" => "Lead2"],
                (object) ["id" => "5452", "text" => "Lead2"],
                (object) ["id" => "7872", "text" => "Lead2"],
                (object) ["id" => "642", "text" => "Lead2"]),
            "end2" => array((object) ["id" => "7", "text" => "Lead7"],
                (object) ["id" => "8", "text" => "Lead8"],
                (object) ["id" => "9", "text" => "Lead9"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"]),
            "end3" => array((object) ["id" => "7", "text" => "Lead7"],
                (object) ["id" => "8", "text" => "Lead8"],
                (object) ["id" => "9", "text" => "Lead9"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"]),
            "end7" => array((object) ["id" => "7", "text" => "Lead7"],
                (object) ["id" => "8", "text" => "Lead8"],
                (object) ["id" => "9", "text" => "Lead9"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"],
                (object) ["id" => "2", "text" => "Lead2"]));

        return response()->json($tasks);

    }

    public function preflight(Request $request)
    {

    }

    public function update(Request $request)
    {

        error_log($request);
        return response()->json("okay");

    }
}
