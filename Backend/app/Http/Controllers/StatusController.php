<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatusController extends Controller
{

    public function index()
    {
        $status = DB::select('select status.id , status.name from status ');
        $count = count($status);

        $response = (object) ["headersType" => array("string"),
            "headers" => array("Name"),
            "data" => $status,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);
    }

    public function create(Request $request)
    {

        $data = [

            'name' => $request->status["name"],
        ];

        DB::insert('insert into status (name) values (?)', [$request->status["name"]]);
        return response()->json($data);

    }
    public function view(Request $request)
    {
        $status = DB::select('select status.*, status.name as name  from status where status.id = ?', [$request->id]);
        return response()->json($status[0]);
    }

    public function edit(Request $request)
    {
        DB::update('update status set name = ? where id=?',
            [$request->name, $request->id]);
        return response()->json("okay");
    }

    public function delete(Request $request)
    {
        $del = DB::delete('delete from status where id = ?', [$request->id]);

        return response()->json("okay");
    }

}
