<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $id, $name, $manager, $role, $user;

    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['login', 'forgotPassword', 'resetPassword']]);

        $token = JWTAuth::getToken();

        if ($token) {
            try {
                if (!$user = JWTAuth::parseToken()->authenticate()) {
                    exit(response()->json(['error' => 'Invalid user, User cannot be found on the system.'], 401));
                }
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                exit(response()->json(['error' => 'Token expired'], 401));
            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                exit(response()->json(['error' => 'Invalid Token'], 401));
            } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                exit(response()->json(['error' => 'Token not Found'], 401));
            } catch (Exception $e) {
                exit(response()->json(['error' => 'Something went wrong'], 401));
            }

            $this->id = $user['attributes']['id'];
            $this->name = $user['attributes']['name'];
            $this->manager = $user['attributes']['manager_id'];
            $this->department = $user['attributes']['department'];
            $this->role = $user['attributes']['role'];
            $this->role = $user['attributes']['department_role'];
            $this->user = $user;
        }
    }
    // public function check(){
    //     return $this->user;
    // }

}
