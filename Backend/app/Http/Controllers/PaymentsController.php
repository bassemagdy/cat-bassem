<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentsController extends Controller
{

    public function index()
    {
        $payments = DB::select('select payments.id,payments.name from payments ');

        $response = (object) ["headersType" => array("string"),
            "headers" => array("Name"),
            "data" => $payments,
            "pageSize" => 10,
            "limit" => 100];

        return response()->json($response);
    }

    public function all()
    {
        $payments = DB::select('select payments.*, payments.name as name  from payments');
        return response()->json($payments);

    }

    public function create(Request $request)
    {

        $data = [

            'name' => $request->resources["name"],
        ];

        DB::insert('insert into payments (name) values (?)', [$request->name]);
        return response()->json($data);

    }
    public function view(Request $request)
    {
        $payments = DB::select('select payments.*, payments.name as name  from payments where payments.id = ?', [$request->id]);
        return response()->json($payments[0]);
    }

    public function edit(Request $request)
    {
        DB::update('update payments set name = ? where id=?',
            [$request->name, $request->id]);

        return response()->json("okay");
    }

    public function delete(Request $request)
    {
        DB::delete('delete from payments where id = ?', [$request->id]);
        return response()->json("okay");
    }
}
