<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LeadController extends Controller
{
    public function createLead(Request $request)
    {
        $customer = DB::select('select * from customer where phone = ? or email = ?', [$request->customerPhone, $request->customerEmail]);
        if (count($customer) == 0) {
            $customer = DB::table('customer')->insertGetId(
                ['name' => $request->customerName,
                    'phone' => $request->customerPhone,
                    'phone2' => $request->customerPhone2,
                    'payment_info' => $request->customerPayment,
                    'address' => $request->customerAddress,
                    'email' => $request->customerEmail,
                    'email2' => $request->customerEmail2,
                    'company' => $request->customerCompany,
                    'vip' => $request->vip,
                ]
            );
        } else {
            $customer = $customer[0]->id;
        }

        $lead = DB::table('lead')->insertGetId(
            [
                'customer_id' => $customer,
                'date_created' => $request->date_created,
                'created_by' => $request->created_by,
                'stage' => $request->stage,
                'department' => $request->department,
                'source' => $request->lead_source,
                'assigned_to' => $request->assignedEmployee,
            ]
        );

        for ($i = 0; $i < count($request->chosenproducts); $i++) {
            DB::insert('insert into product_lead (instance_id,lead_id, count) values (?,?,?)', [$request->chosenproducts[$i], $lead, $request->count[$i]]);
        }
        for ($i = 0; $i < count($request->serviceID); $i++) {
            $price = $request->output[0][0];
            $price = str_replace("price:", "", $price);
            $price = str_replace(";", "", $price);
            DB::insert('insert into service_lead values (?,?,?,?)', [$request->serviceID[$i], $lead, implode($request->output[$i]), $price]);
        }
        return response()->json($lead);

        //  return response()->json("okay");
    }

    public function checkRoles()
    {
        $current_user_id = $this->user->id;
        $current_user_role = $this->user->role;
        $superAdmins = DB::select('select employee.id from employee left join roles on employee.role = roles.id where roles.name ="Super Admin"');
        return $superAdmins[0]->id;
        for ($i = 0; $i < count($superAdmins); $i++) {
            if ($current_user_id == $superAdmins[$i]->id) {
                return true;
            }
        }
        return false;
    }

    public function archiveLead(Request $request)
    {
        DB::update('update lead set Archived = ? where id=?',
            [$request->check, $request->id]);
        return response()->json($request);
    }

    public function getLeads(Request $request)
    {

        if (!isset($request->id) && $this->checkRoles() == true) {
            $leads = DB::select('select lead.*, datediff(CURDATE(), lead.date_updated) as last_update, customer.name as CustomerName, product_instance.name as ProductName,
                                 service_category.name as ServiceName, employee.name as EmployeeName, stage.name as stage_name
                                from lead
                                left join customer on lead.customer_id = customer.id
                                left join product_lead on lead.id = product_lead.lead_id
                                left join product_instance on product_instance.id = product_lead.instance_id
                                left join service_lead on service_lead.lead_id = lead.id
                                left join service_category on service_category.id = service_lead.service_id
                                left join employee on employee.id = lead.assigned_to
                                left join stage on lead.stage = stage.id
                                group by lead.id
                                order by date_created DESC
                                limit 0, 20 ');
        }

        // elseif($request->id == -1)
        elseif ($request->id == -1) {
            $leads = DB::select('select lead.*, datediff(CURDATE(), lead.date_updated) as last_update, customer.name as CustomerName, product_instance.name as ProductName,
                            service_category.name as ServiceName, employee.name as EmployeeName, stage.name as stage_name
                            from lead
                            left join customer on lead.customer_id = customer.id
                            left join product_lead on lead.id = product_lead.lead_id
                            left join product_instance on product_instance.id = product_lead.instance_id
                            left join service_lead on service_lead.lead_id = lead.id
                            left join service_category on service_category.id = service_lead.service_id
                            left join employee on employee.id = lead.assigned_to
                            left join stage on lead.stage = stage.id
                            where lead.assigned_to is null
                            group by lead.id
                            order by date_created DESC
                            limit 0, 20 ');
        }
        // elseif($this->checkRoles() == true){
        //     $leads = DB::select('select lead.*, datediff(CURDATE(), lead.date_updated) as last_update, customer.name as CustomerName, product_instance.name as ProductName,
        //     service_category.name as ServiceName, employee.name as EmployeeName, stage.name as stage_name
        //     from lead
        //     left join customer on lead.customer_id = customer.id
        //     left join product_lead on lead.id = product_lead.lead_id
        //     left join product_instance on product_instance.id = product_lead.instance_id
        //     left join service_lead on service_lead.lead_id = lead.id
        //     left join service_category on service_category.id = service_lead.service_id
        //     left join employee on employee.id = lead.assigned_to
        //     left join stage on lead.stage = stage.id
        //     group by lead.id
        //     order by date_created DESC
        //     limit 0, 20 ');
        // }
        else {
            $leads = DB::select('select lead.*, datediff(CURDATE(), lead.date_updated) as last_update, customer.name as CustomerName, product_instance.name as ProductName,
                            service_category.name as ServiceName, employee.name as EmployeeName, stage.name as stage_name
                            from lead
                            left join customer on lead.customer_id = customer.id
                            left join product_lead on lead.id = product_lead.lead_id
                            left join product_instance on product_instance.id = product_lead.instance_id
                            left join service_lead on service_lead.lead_id = lead.id
                            left join service_category on service_category.id = service_lead.service_id
                            left join employee on employee.id = lead.assigned_to
                            left join stage on lead.stage = stage.id
                            where lead.assigned_to = ? || lead.created_by = ?
                            group by lead.id
                            order by date_created DESC
                            limit 0, 20 ', [$request->id, $request->id]);
        }

        return response()->json($leads);
    }

    public function updateLead(Request $request)
    {
        $leads = DB::update('update lead set stage = ?, comment = ?, reminder = ?, date_updated = now() where id = ? ',
            [$request->destination, $request->comment, $request->reminder, $request->id]);

        DB::insert('insert into lead_stage (lead_id,stage_id, comment, reminder, timestamp) values (?,?,?,?, NOW())', [$request->id, $request->destination, $request->comment, $request->reminder]);

        return response()->json($leads);
    }

    public function getLead(Request $request)
    {

        $customer = DB::select('select c.*, resources.name as source_name, department.name as department_name from customer as c
                                left join lead on lead.customer_id = c.id
                                left join resources on lead.source = resources.id
                                left join department on lead.department = department.id
                                where c.id = ?'

            , [$request->customer]);
        $product_leads = DB::select('select product_instance.name as ProductName,product_category.name as CategoryName,
                                     product_instance.id as ProductID,product_category.id as CategoryID,
                                     product_instance.data as Data,
                                     product_lead.count as Count
                                     from product_lead inner join product_instance
                                     on product_lead.instance_id = product_instance.id
                                     inner join product_category
                                     on product_instance.category_id = product_category.id
                                      where lead_id = ?',
            [$request->id]);
        $service_leads = DB::select('select service_category.name as Name, service_category.id as ServiceID, service_lead.data as Data, service_category.definition as Definition
                                     from service_lead
                                     inner join service_category
                                     on service_category.id = service_lead.service_id
                                     where lead_id = ?', [$request->id]);
        $leads = (object) array('products' => $product_leads, 'services' => $service_leads, 'customer' => $customer[0]);

        return response()->json($leads);
    }

    public function assignLead(Request $request)
    {

        DB::update('update lead set assigned_to = ? where id = ?', [$request->employee, $request->lead]);
        return response()->json('okay');
    }

    public function getLeadsFull()
    {

        $leads = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                             customer.phone as CustomerPhone, employee.name as EmployeeName
                             from lead
                             inner join customer
                             on customer.id = lead.customer_id
                             inner join employee
                             on employee.id = lead.assigned_to');
        return response()->json($leads);
    }

    // public function search(Request $request)
    // {
    //    $results = DB::select('SELECT lead.*, customer.name as CustomerName, customer.email as CustomerEmail, customer.phone as CustomerPhone, employee.name as EmployeeName
    //                           FROM customer inner join lead on lead.customer_id = customer.id inner join employee on employee.id = lead.assigned_to
    //                           WHERE MATCH (customer.name, customer.phone, customer.email)
    //                           AGAINST (\'+'.$request->keyword.'*\' IN BOOLEAN MODE)');

    //    return response()->json($results);

    // }

    public function edit(Request $request)
    {
        for ($i = 0; $i < count($request->products); $i++) {
            DB::update('update product_lead set count = ? where instance_id = ? and lead_id = ?', [$request->products[$i]["Count"], $request->products[$i]["ProductID"], $request->id]);
        }

        for ($j = 0; $j < count($request->services); $j++) {
            $sqlarray = "";
            $price;
            for ($i = 0; $i < count($request->services[$j]["Data"]); $i++) {
                if ($request->services[$j]["Data"][$i][0] == "price") {
                    $price = $request->services[$j]["Data"][$i][1];
                }

                $sqlarray = $sqlarray . $request->services[$j]["Data"][$i][0] . ':' . $request->services[$j]["Data"][$i][1] . ';';
            }

            DB::update('update service_lead set data = ?, price = ? where service_id = ? and lead_id = ?', [$sqlarray, $price, $request->services[$j]["ServiceID"], $request->id]);
        }

        //add new products and services

        for ($i = 0; $i < count($request->chosenproducts); $i++) {
            DB::insert('insert into product_lead (instance_id,lead_id, count) values (?,?,?)', [$request->chosenproducts[$i], $request->id, $request->count[$i]]);
        }
        for ($i = 0; $i < count($request->serviceID); $i++) {
            $price = $request->servicedata[0][0];
            $price = str_replace("price:", "", $price);
            $price = str_replace(";", "", $price);
            DB::insert('insert into service_lead values (?,?,?,?)', [$request->serviceID[$i], $request->id, implode($request->servicedata[$i]), $price]);
        }

        //delete products and services

        for ($i = 0; $i < count($request->deletedProducts); $i++) {
            DB::delete('delete from product_lead where instance_id = ? and lead_id = ?', [$request->deletedProducts[$i], $request->id]);
        }
        for ($i = 0; $i < count($request->deletedServices); $i++) {
            DB::delete('delete from service_lead where service_id = ? and lead_id = ?', [$request->deletedServices[$i], $request->id]);
        }

        DB::update('update lead set  assigned_to = ?, stage = ?, department = ? where id = ?', [$request->lead["assigned_to"], $request->lead["stage"], $request->lead["department"], $request->id]);
        DB::update('update customer set name = ?, phone = ?, email = ? , company = ?, address = ? ,phone2 = ? ,  email2= ?    where id = ?', [$request->customer["name"], $request->customer["phone"], $request->customer["email"], $request->customer["company"], $request->customer["address"], $request->customer["phone2"], $request->customer["email2"], $request->lead["customer_id"]]);

        return response()->json("okay");
    }

    public function deleteLead(Request $request)
    {

        DB::delete('delete from lead where id = ?', [$request->id]);
        DB::delete('delete from product_lead where lead_id = ?', [$request->id]);
        DB::delete('delete from service_lead where lead_id = ?', [$request->id]);
        return response()->json("okay");
    }

    public function getAppLeads()
    {

        $lead = DB::select('select * from lead');
        $customer = DB::select('select customer.*, lead.id as LeadID from customer inner join lead on lead.customer_id = customer.id');
        $product_leads = DB::select('select product_instance.name as ProductName,product_category.name as CategoryName, product_lead.lead_id as LeadID,
                                     product_instance.id as ProductID,product_category.id as CategoryID,
                                     product_instance.data as Data,
                                     product_lead.count as Count
                                     from product_lead inner join product_instance
                                     on product_lead.instance_id = product_instance.id
                                     inner join product_category
                                     on product_instance.category_id = product_category.id');
        $service_leads = DB::select('select service_category.name as Name, service_category.id as ServiceID, service_lead.data as Data, service_category.definition as Definition, service_lead.lead_id as LeadID
                                     from service_lead
                                     inner join service_category
                                     on service_category.id = service_lead.service_id');
        $leads = (object) array('leads' => $lead, 'products' => $product_leads, 'services' => $service_leads, 'customer' => $customer[0]);

        return response()->json($leads);
    }

    public function index()
    {

        $data = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                             customer.phone as CustomerPhone, employee.name as EmployeeName,
                             product_instance.name as ProductName,
                             service_category.name as ServiceName
                             from lead
                             left outer join customer
                             on customer.id = lead.customer_id
                             left outer join employee
                             on employee.id = lead.assigned_to
                             left outer join product_lead
                             on lead.id = product_lead.lead_id
                             left outer join product_instance
                              on product_instance.id = product_lead.instance_id
                              left outer join service_lead
                             on service_lead.lead_id = lead.id
                              left outer join service_category
                               on service_category.id = service_lead.service_id
                             limit 0, 20');

        $count = DB::select('select count(*) as count
                             from lead
                             left outer join customer
                             on customer.id = lead.customer_id
                             left outer join employee
                             on employee.id = lead.assigned_to
                             left outer join product_lead
                             on lead.id = product_lead.lead_id
                             left outer join product_instance
                              on product_instance.id = product_lead.instance_id
                              left outer join service_lead
                             on service_lead.lead_id = lead.id
                              left outer join service_category
                               on service_category.id = service_lead.service_id');
        $response = (object) ["headersType" => array("string", "string", "string", "string", "date", "string", "date", "string", "string"),
            "headers" => array("Name", "CustomerName", "CustomerEmail", "CustomerPhone", "Reminder", "Comment", "Date_Created", "Assigned_To", "Stage"),
            "data" => $data,
            "pageSize" => 10,
            "limit" => 20,
            "count" => $count[0]];

        return response()->json($response);
    }

    public function paginate(Request $request)
    {

        error_log($request);
        if ($request->columns && $request->keyword) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }

            if (!$request->column) {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                            customer.phone as CustomerPhone, employee.name as EmployeeName,
                                            product_instance.name as ProductName,
                                            service_category.name as ServiceName
                                            from lead
                                            left outer join customer
                                            on customer.id = lead.customer_id
                                            left outer join employee
                                            on employee.id = lead.assigned_to
                                            left outer join product_lead
                                            on lead.id = product_lead.lead_id
                                            left outer join product_instance
                                            on product_instance.id = product_lead.instance_id
                                            left outer join service_lead
                                            on service_lead.lead_id = lead.id
                                            left outer join service_category
                                            on service_category.id = service_lead.service_id
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                            customer.phone as CustomerPhone, employee.name as EmployeeName,
                                            product_instance.name as ProductName,
                                            service_category.name as ServiceName
                                            from lead
                                            left outer join customer
                                            on customer.id = lead.customer_id
                                            left outer join employee
                                            on employee.id = lead.assigned_to
                                            left outer join product_lead
                                            on lead.id = product_lead.lead_id
                                            left outer join product_instance
                                            on product_instance.id = product_lead.instance_id
                                            left outer join service_lead
                                            on service_lead.lead_id = lead.id
                                            left outer join service_category
                                            on service_category.id = service_lead.service_id
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           limit ' . $request->page . ',20');
                }

            } else {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                            customer.phone as CustomerPhone, employee.name as EmployeeName,
                                            product_instance.name as ProductName,
                                            service_category.name as ServiceName
                                            from lead
                                            left outer join customer
                                            on customer.id = lead.customer_id
                                            left outer join employee
                                            on employee.id = lead.assigned_to
                                            left outer join product_lead
                                            on lead.id = product_lead.lead_id
                                            left outer join product_instance
                                            on product_instance.id = product_lead.instance_id
                                            left outer join service_lead
                                            on service_lead.lead_id = lead.id
                                            left outer join service_category
                                            on service_category.id = service_lead.service_id
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)  order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                            customer.phone as CustomerPhone, employee.name as EmployeeName,
                                            product_instance.name as ProductName,
                                            service_category.name as ServiceName
                                            from lead
                                            left outer join customer
                                            on customer.id = lead.customer_id
                                            left outer join employee
                                            on employee.id = lead.assigned_to
                                            left outer join product_lead
                                            on lead.id = product_lead.lead_id
                                            left outer join product_instance
                                            on product_instance.id = product_lead.instance_id
                                            left outer join service_lead
                                            on service_lead.lead_id = lead.id
                                            left outer join service_category
                                            on service_category.id = service_lead.service_id
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)  order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . $request->page . ',20');
                }

            }

        } else
        if ($request->column) {
            if ($request->page % 10 == 0) {
                $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                            customer.phone as CustomerPhone, employee.name as EmployeeName,
                                            product_instance.name as ProductName,
                                            service_category.name as ServiceName
                                            from lead
                                            left outer join customer
                                            on customer.id = lead.customer_id
                                            left outer join employee
                                            on employee.id = lead.assigned_to
                                            left outer join product_lead
                                            on lead.id = product_lead.lead_id
                                            left outer join product_instance
                                            on product_instance.id = product_lead.instance_id
                                            left outer join service_lead
                                            on service_lead.lead_id = lead.id
                                            left outer join service_category
                                            on service_category.id = service_lead.service_id
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                            customer.phone as CustomerPhone, employee.name as EmployeeName,
                                            product_instance.name as ProductName,
                                            service_category.name as ServiceName
                                            from lead
                                            left outer join customer
                                            on customer.id = lead.customer_id
                                            left outer join employee
                                            on employee.id = lead.assigned_to
                                            left outer join product_lead
                                            on lead.id = product_lead.lead_id
                                            left outer join product_instance
                                            on product_instance.id = product_lead.instance_id
                                            left outer join service_lead
                                            on service_lead.lead_id = lead.id
                                            left outer join service_category
                                            on service_category.id = service_lead.service_id
                                           on employee.id = lead.assigned_to
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            }

        } else {
            if ($request->page % 10 == 0) {
                $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                            customer.phone as CustomerPhone, employee.name as EmployeeName,
                                            product_instance.name as ProductName,
                                            service_category.name as ServiceName
                                            from lead
                                            left outer join customer
                                            on customer.id = lead.customer_id
                                            left outer join employee
                                            on employee.id = lead.assigned_to
                                            left outer join product_lead
                                            on lead.id = product_lead.lead_id
                                            left outer join product_instance
                                            on product_instance.id = product_lead.instance_id
                                            left outer join service_lead
                                            on service_lead.lead_id = lead.id
                                            left outer join service_category
                                            on service_category.id = service_lead.service_id limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                            customer.phone as CustomerPhone, employee.name as EmployeeName,
                                            product_instance.name as ProductName,
                                            service_category.name as ServiceName
                                            from lead
                                            left outer join customer
                                            on customer.id = lead.customer_id
                                            left outer join employee
                                            on employee.id = lead.assigned_to
                                            left outer join product_lead
                                            on lead.id = product_lead.lead_id
                                            left outer join product_instance
                                            on product_instance.id = product_lead.instance_id
                                            left outer join service_lead
                                            on service_lead.lead_id = lead.id
                                            left outer join service_category
                                            on service_category.id = service_lead.service_id limit ' . $request->page . ',20');
            }

        }

        return response()->json($results);
    }

    public function search(Request $request)
    {
        if (trim($request->keyword) == "") {
            $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                                customer.phone as CustomerPhone, employee.name as EmployeeName,
                                                product_instance.name as ProductName,
                                                service_category.name as ServiceName
                                                from lead
                                                left outer join customer
                                                on customer.id = lead.customer_id
                                                left outer join employee
                                                on employee.id = lead.assigned_to
                                                left outer join product_lead
                                                on lead.id = product_lead.lead_id
                                                left outer join product_instance
                                                on product_instance.id = product_lead.instance_id
                                                left outer join service_lead
                                                on service_lead.lead_id = lead.id
                                                left outer join service_category
                                                on service_category.id = service_lead.service_id');
            $count = DB::select('select count(*) as count
                                                from lead
                                                left outer join customer
                                                on customer.id = lead.customer_id
                                                left outer join employee
                                                on employee.id = lead.assigned_to
                                                left outer join product_lead
                                                on lead.id = product_lead.lead_id
                                                left outer join product_instance
                                                on product_instance.id = product_lead.instance_id
                                                left outer join service_lead
                                                on service_lead.lead_id = lead.id
                                                left outer join service_category
                                                on service_category.id = service_lead.service_id');

            $response = (object) ["data" => $results,
                "count" => $count[0]];
            error_log("hi");
            return response()->json($response);
        } else {
            $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                                customer.phone as CustomerPhone, employee.name as EmployeeName,
                                                product_instance.name as ProductName,
                                                service_category.name as ServiceName
                                                from lead
                                                left outer join customer
                                                on customer.id = lead.customer_id
                                                left outer join employee
                                                on employee.id = lead.assigned_to
                                                left outer join product_lead
                                                on lead.id = product_lead.lead_id
                                                left outer join product_instance
                                                on product_instance.id = product_lead.instance_id
                                                left outer join service_lead
                                                on service_lead.lead_id = lead.id
                                                left outer join service_category
                                                on service_category.id = service_lead.service_id
                                                WHERE
                                                MATCH (customer.name,customer.email,customer.phone)
                                                AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                                or
                                                MATCH (lead.comment,lead.stage)
                                                AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                                limit 0, 100');
            $count = count($results);

            $response = (object) ["data" => $results,
                "count" => $count[0]];
            error_log("hi");
            return response()->json($response);
        }
    }

    public function sort(Request $request)
    {

        if ($request->keyword && $request->columns) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }
            $results = DB::select('select lead.*, customer.name as CustomerName, customer.email,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName
                                           from lead
                                           inner join customer
                                           on customer.id = lead.customer_id
                                           inner join employee
                                           on employee.id = lead.assigned_to
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE) order by ' . $request->column . ' ' . $request->order . '
                                           limit 0,20');
        } else {
            error_log('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName
                                           from lead
                                           inner join customer
                                           on customer.id = lead.customer_id
                                           inner join employee
                                           on employee.id = lead.assigned_to
                                           order by ' . $request->column . ' ' . $request->order . ' limit 0,20');
            $results = DB::select('select  lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName
                                           from lead
                                           inner join customer
                                           on customer.id = lead.customer_id
                                           inner join employee
                                           on employee.id = lead.assigned_to
                                           order by ' . $request->column . ' ' . $request->order . ' limit 0,20');
        }

        return response()->json($results);
    }

    public function getLeadStages(Request $request)
    {

        $re = DB::select('select lead_stage.stage_id , lead_stage.reminder , lead_stage.comment , lead_stage.timestamp from lead_stage where lead_stage.lead_id = ? ', [$request->id]);

        $ree = DB::select(' select lead_stage.stage_id,lead_stage.reminder,
   lead_stage.comment,lead_stage.timestamp , stage.name
   FROM lead_stage
   JOIN stage
   ON lead_stage.stage_id = stage.id
   where  lead_stage.lead_id = ? ', [$request->id]);

        return response()->json($ree);

    }

    public function getPhoneNumber(Request $request)
    {

        $number = DB::select('select customer.id as Id ,
      customer.name as CustomerName,
      customer.address as CustomerAddress ,
      customer.company as CustomerComapny ,
      customer.email as CustomerEmail, customer.email2 as CustomerEmail2,
      customer.phone as CustomerPhone, customer.phone2 as CustomerPhone2 from customer where customer.phone= ?
      ', [$request->phone]);

        if (!(empty($number))) {

            $lead = DB::select('select lead.stage as Stage,
        lead.id as Id,
        lead.department as Department,
        lead.source as Source
        from lead
        where lead.customer_id =?', [$number[0]->Id]);

            $leads = (object) array('leads' => $lead, 'customer' => $number);
            return response()->json($leads);

        }

        return response()->json(-1);

    }
}
