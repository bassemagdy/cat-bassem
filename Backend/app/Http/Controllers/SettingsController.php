<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{

    public function index()
    {
        $settings = DB::select('select settings.id, settings.name, settings.value, settings.type from settings ');

        $count = count($settings);

        $response = (object) ["headersType" => array("string", "string", "string"),
            "headers" => array("Name", "value", "type"),
            "data" => $settings,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);
    }

    public function create(Request $request)
    {

        DB::insert('insert into settings (name ,value , type) values (? , ? , ? )',
            [$request->settings["name"], $request->settings["value"], $request->settings["type"]]);
        return response()->json("ok");

    }
    public function view(Request $request)
    {
        $settings = DB::select('select settings.* from settings where settings.id = ?', [$request->id]);
        return response()->json($settings[0]);
    }

    public function edit(Request $request)
    {
        DB::update('update settings set name = ? , value = ? , type = ?  where id= ?',
            [$request->name, $request->value, $request->type, $request->id]);

        return response()->json("okay");
    }

    public function delete(Request $request)
    {
        $del = DB::delete('delete from settings where id = ?', [$request->id]);

        return response()->json("okay");
    }

}
