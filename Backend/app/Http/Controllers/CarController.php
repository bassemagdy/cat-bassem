<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CarController extends Controller
{

    public function index()
    {

        $cars = DB::select('select car.*, car.id , car.number , car.type , car.validUntil , car.licenseNumber , car.initialKilometers ,car.carModel , car.chassisNumber
                            from car limit 0,100');

        $count = count($cars);

        $response = (object) [
            "headersType" => array("string", "string", "date", "int", "int", "int", "int", "string"),
            "headers" => array("number", "type", "validUntil", "licenseNumber", "initialKilometers", "carModel", "chassisNumber", "status"),
            "data" => $cars,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);

    }

    public function search(Request $request)
    {
        if (trim($request->keyword) == "") {
            $results = DB::select('select car.*, car.id , car.number , car.type , car.validUntil , car.licenseNumber , car.initialKilometers ,car.carModel , car.chassisNumber
            from car limit 0,100');
            $count = DB::select('select car.*, car.id , car.number , car.type , car.validUntil , car.licenseNumber , car.initialKilometers ,car.carModel , car.chassisNumber
            from car limit 0,100');

            $response = (object) ["data" => $results,
                "count" => $count[0]];
            error_log("hi");
            return response()->json($response);
        } else {

            $results = DB::select('select * from car
                                    WHERE
                                    number LIKE "%' . $request->keyword . '%"
                                    OR
                                    MATCH (number,type)
                                    AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                  limit 0,100');
            $count = count($results);

            $response = (object) ["data" => $results,
                "count" => $count[0]];
            error_log("hi");
            return response()->json($response);
        }
    }

    public function create(Request $request)
    {

        $result = DB::select('select * from car_type WHERE id=' . $request->car['car_type_id']);
        $type = $result[0]->model . ' ' . $result[0]->manufacturer;
        DB::insert('insert into car (number,type,validUntil,licenseNumber,initialKilometers,carModel ,chassisNumber, status, car_type_id) values (? , ? , ? ,?, ? , ? ,? , ? , ? )',
            [$request->car["number"], $type, $request->car["validUntil"], $request->car["licenseNumber"], $request->car["initialKilometers"], $request->car["carModel"], $request->car["chassisNumber"], $request->car["status"], $request->car['car_type_id']]);

        return response()->json("done");
    }

    public function delete(Request $request)
    {

        DB::delete('delete from car where id=?', [$request->id]);
        return response()->json("deleted");

    }

    public function edit(Request $request)
    {

        DB::update('update car set number = ? ,type = ? , validUntil=? , licenseNumber=?, initialKilometers = ? , 	carModel = ? , 	chassisNumber =?  , status = ? where id = ?',
            [$request->car["number"], $request->car["type"], $request->car["validUntil"], $request->car["licenseNumber"], $request->car["initialKilometers"], $request->car["carModel"], $request->car["chassisNumber"], $request->car["status"], $request->car["id"]]);

        return response()->json("edited");

    }

    public function view(Request $request)
    {

        $car = DB::select('select car.*, car.id , car.number , car.type , car.validUntil, car.licenseNumber ,car.initialKilometers ,car.carModel , car.chassisNumber  , car.status from car where id = ?', [$request->id]);
        return response()->json($car[0]);

    }

    public function paginate(Request $request)
    {

        if ($request->columns && $request->keyword) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }

            if (!$request->column) {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select car.number,car.type,car.validUntil, car.licenseNumber ,car.initialKilometers
                                           from car


                                           limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select car.number,car.type,car.validUntil ,car.licenseNumber ,car.initialKilometers
                                           from car




                                           limit ' . $request->page . ',20');
                }

            } else {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select car.number,car.type,car.validUntil ,car.licenseNumber ,car.initialKilometers
                                          from car


                                           order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select car.number,car.type,car.validUntil ,car.licenseNumber ,car.initialKilometers
                                           from car


                                           order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . $request->page . ',20');
                }

            }

        } else
        if ($request->column) {
            if ($request->page % 10 == 0) {
                $results = DB::select('select car.number,car.type,car.validUntil ,car.licenseNumber ,car.initialKilometers
                                       from car
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select car.number,car.type,car.validUntil ,car.licenseNumber ,car.initialKilometers
                                       from car

                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            }

        } else {
            if ($request->page % 10 == 0) {
                $results = DB::select('select car.number,car.type,car.validUntil ,car.licenseNumber ,car.initialKilometers
                                           from car

                                            limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select car.number,car.type,car.validUntil ,car.licenseNumber ,car.initialKilometers
                                           from car

                                           limit ' . $request->page . ',20');
            }

        }

        return response()->json($results);
    }

}
