<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TripController extends Controller
{

    public function index()
    {

        $trips = DB::select('select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                     trip.status , trip.distance , trip.duration , car.number , car.type
                     from trip
                     inner join car
                     on trip.car_id = car.id');

        $count = count($trips);

        $response = (object) ["headersType" => array("time", "time", "string", "string", "string", "string", "int", "string", "string"),
            "headers" => array("start_time", "end_time", "addressFrom", "addressTo", "status", "distance", "duration", "car_number", "car_type", "fromZone", "toZone", "price"),
            "data" => $trips,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);

    }

    public function paginate(Request $request)
    {

        if ($request->columns && $request->keyword) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }

            if (!$request->column) {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                                                        trip.status , trip.distance , trip.duration , car.number , car.type
                                                        from trip
                                                        inner join car
                                                        on trip.car_id == car.id
                                                        limit ' . $request->page . ',20');
                } else {
                    $results = DB::select(' select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                                                        trip.status , trip.distance , trip.duration , car.number , car.type
                                                        from trip
                                                        inner join car
                                                        on trip.car_id == car.id
                                                        limit ' . $request->page . ',20');
                }

            } else {
                if ($request->page % 10 == 0) {
                    $results = DB::select(' select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                                                        trip.status , trip.distance , trip.duration , car.number , car.type
                                                        from trip
                                                        inner join car
                                                        on trip.car_id == car.id
                                                        order by ' . $request->column . ' ' . $request->order .
                        ' limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                                                    trip.status , trip.distance , trip.duration , car.number , car.type
                                                    from trip
                                                    inner join car
                                                    on trip.car_id == car.id
                                                    order by ' . $request->column . ' ' . $request->order .
                        ' limit ' . $request->page . ',20');
                }

            }

        } else
        if ($request->column) {
            if ($request->page % 10 == 0) {
                $results = DB::select('select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                                                      trip.status , trip.distance , trip.duration , car.number , car.type
                                                      from trip
                                                      inner join car
                                                      on trip.car_id == car.id
                                                      order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                                                    trip.status , trip.distance , trip.duration , car.number , car.type
                                                    from trip
                                                    inner join car
                                                    on trip.car_id == car.id
                                                    order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            }

        } else {
            if ($request->page % 10 == 0) {
                $results = DB::select('select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                                                    trip.status , trip.distance , trip.duration , car.number , car.type
                                                    from trip
                                                    inner join car
                                                    on trip.car_id == car.id
                                                    limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select trip.*, trip.car_id , trip.start_time , trip.end_time , trip.placeX , trip.placeY ,
                                                    trip.status , trip.distance , trip.duration , car.number , car.type
                                                    from trip
                                                    inner join car
                                                    on trip.car_id == car.id
                                                    limit ' . $request->page . ',20');
            }

        }

        return response()->json($results);
    }

    function checkCarType ($carID,$carTypeId){
        $result = DB::select('select * from car where id = ? AND car_type_id = ? AND status = ?', [$carID,$carTypeId,'out of service']);
        if (!empty($result)) {
            return true;
        }
        return false;
    }
    
    public function Booking(Request $request)
    {
        // return response()->json($request);
        $str = $request->booking["trip_date"]. ' ' . $request->booking["trip_time"].':00';
        $trips = DB::select("SELECT * From trip Where car_id
                    not in ( Select trip.car_id from trip WHERE trip.start_time = '$str' OR
                     trip.end_time = '$str')
                    AND NOT (TIMESTAMPDIFF(MINUTE, trip.end_time, '$str') < 15 OR
                     TIMESTAMPDIFF(MINUTE, trip.start_time, '$str') < 120)");
        $arr = (array)$trips;
        $flag = false;    

        if (!empty($arr)) {
            $items=array();
            $c = count($trips);
            for ($i=0 ; $i < count($trips) ;$i++) {
                $place = $trips[$i]->placeY;
                $myArray = explode(',', $place);
                if ($this->checkCarType($trips[$i]->car_id,$request->booking['car_type_id'])) {
                    $cID = $trips[$i]->car_id;
                    $latKm = (float)$myArray[0];
                    $lngKm = (float)$myArray[1];
                    $distFunction = $this->distance($latKm, $lngKm, (float)$request->booking["lat"], (float)$request->booking["lng"], "K");
                    $items[$i]['car_id'] = $cID;
                    $items[$i]['distance'] = $distFunction;
                    $flag = true;
                }
            }
            if ($flag) {
                $lowest_distance = min(array_column($items, 'distance'));
                $index = array_search($lowest_distance, array_column($items, 'distance'));
                $carID = $items[$index]['car_id'];
                $calcl = $this->calculations($request->booking["fromConcat"], $request->booking["toConcat"]);
                $time = $request->booking["trip_date"] . ' ' . $request->booking["trip_time"];
                $minutesToAdd = $calcl['distanceMins'];
                $placeX = $request->booking["lat"] .','. $request->booking["lng"];
                $placeY = $request->booking["latToo"] .','. $request->booking["lngToo"];
                $dist = $calcl['distance'];
                $dur = $calcl['duration'];
                $addressTo = $request->booking["AddressTo"];
                $addressFrom = $request->booking["AddressFrom"];
                $land_mark = $request->booking["land_mark"];
                $passenger_name = $request->booking["name"];
                $email= $request->booking["email"];
                $phone =$request->booking["phone"];
                $numOfBags =$request->booking["numOfBags"];
                $numOfPass = $request->booking["numOfPassengers"];
                // $carType = $request->booking["car_type"];
                $ref_id =$request->booking["source"];
                $flightNumber = $request->booking["flightNumber"];
                $fromZone = $request->booking["zoneVarFrom"];
                $toZone = $request->booking["zoneVarTo"];
                $carTypeId = $request->booking['car_type_id'];
                $result = DB::select('select * from car_type WHERE id='.$request->booking['car_type_id']);
                $carType = $result[0]->model.' '.$result[0]->manufacturer;

                // $price = $request->booking["price"];
                $fromid =$request->booking["chosenZoneFromId"];
                $toid =$request->booking["chosenZoneToId"];
                $price = DB::select('select * from zone_pricing where from_id = ? AND to_id =?', [$fromid, $toid]);
                if ($price) {
                    $price2 = $price[0];
                }
                    
                if ($dist != 0) {
                    DB::insert("INSERT INTO trip ( car_id, start_time, end_time, placeX, placeY, distance, duration, addressTo ,addressFrom,land_mark,  passenger_name,email,phone, numOfBags,numOfPass,carType,ref_id, flightNumber,fromZone, toZone,price,car_type_id)
                     VALUES ('$carID', '$time', TIMESTAMPADD(MINUTE,'$minutesToAdd','$time'),  '$placeX','$placeY', '$dist','$dur', '$addressTo', '$addressFrom','$land_mark','$passenger_name','$email','$phone','$numOfBags','$numOfPass','$carType ','$ref_id', '$flightNumber', '$fromZone','$toZone','$price2->price','$carTypeId')");
                    $x = DB::select("SELECT * FROM trip WHERE ID = (SELECT MAX(ID) FROM trip)");
                    
                    // return response()->json($x);
                    return response()->json($carID);
                }
            }    
        } 
        $query_parking = DB::select("SELECT * FROM car WHERE car.id not in(SELECT trip.car_id FROM trip) AND car_type_id = ? AND status != ?",[$request->booking['car_type_id'],'out of service']);
        if ($query_parking != null) {
            $carIndexP = array_rand((array)$query_parking);
            $carIDP = $query_parking[$carIndexP]->id;
            $calcl = $this->calculations($request->booking["fromConcat"], $request->booking["toConcat"]);
            $time = $request->booking["trip_date"] . ' ' . $request->booking["trip_time"];
            $minutesToAdd = $calcl['distanceMins'];
            $placeX = $request->booking["lat"] .','. $request->booking["lng"];
            $placeY = $request->booking["latToo"] .','. $request->booking["lngToo"];
            $addressFrom = $request->booking["AddressFrom"];
            $addressTo = $request->booking["AddressTo"];
            $dist = $calcl['distance'];
            $dur = $calcl['duration'];
            $land_mark = $request->booking["land_mark"];
            $passenger_name = $request->booking["name"];
            $email= $request->booking["email"];
            $phone =$request->booking["phone"];
            $numOfBags =$request->booking["numOfBags"];
            $numOfPass = $request->booking["numOfPassengers"];
            $carType = $request->booking["car_type"];
            $ref_id =$request->booking["source"];
            $flightNumber = $request->booking["flightNumber"];
            $fromZone = $request->booking["zoneVarFrom"];
            $toZone = $request->booking["zoneVarTo"];
            // $price = $request->booking["price"];
            $fromid =$request->booking["chosenZoneFromId"];
            $toid =$request->booking["chosenZoneToId"];
            $carTypeId = $request->booking['car_type_id'];

            $price = DB::select('select * from zone_pricing where from_id = ? AND to_id =?', [$fromid, $toid]);
            if ($price) {
                $price2 = $price[0];
            }
            if ($dist!=0) {
                DB::insert("INSERT INTO trip ( car_id, start_time, end_time, placeX, placeY, distance, duration,addressTo,addressFrom,land_mark,passenger_name,email,phone, numOfBags,numOfPass,carType,ref_id,flightNumber,fromZone, toZone,price,car_type_id)  VALUES ('$carIDP', '$time', TIMESTAMPADD(MINUTE,'$minutesToAdd','$time'),  '$placeX','$placeY', '$dist','$dur', '$addressTo', '$addressFrom','$land_mark','$passenger_name','$email','$phone','$numOfBags','$numOfPass','$carType ','$ref_id','$flightNumber','$fromZone','$toZone','$price2->price','$carTypeId')");
                $x = DB::select("SELECT * FROM trip WHERE ID = (SELECT MAX(ID) FROM trip)");
                    
                    // return response()->json($x);
                // return response()->json($x[0]);
                
                return response()->json($carIDP);
            }
        }else{
            return response()->json("false");
        }
    }


    public function geo(Request $request)
    {

        $request->from = str_replace(" ", "+", urlencode($request->from));
        $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . $request->from . "&sensor=false&region=EG&language=ar";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch), true);

        if ($response['status'] != 'OK') {
            return response()->json("null");
        }
        $response_counter = count($response['results']);
        $arr_counter = 0;
        $array_data = array();
        while ($arr_counter < $response_counter) {
            $object = new \stdClass();

            $geometry = $response['results'][$arr_counter]['geometry'];
            $latitude = $geometry['location']['lat'];
            $longitude = $geometry['location']['lng'];
            $object->id = $arr_counter;
            $object->latitude = $latitude;
            $object->longitude = $longitude;
            array_push($array_data, $object);

            $arr_counter++;
        }
        return response()->json($array_data);
        $array = array(
            'latitude' => $geometry['location']['lat'],
            'longitude' => $geometry['location']['lng'],
            'location_type' => $geometry['location_type'],
        );
        return response()->json($response['results']);
    }

    public function getAddress(Request $request)
    {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($request->lat) . ',' . trim($request->lng) . '&sensor=false';
        $json = @file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        if ($status == "OK") {
            return response()->json($data->results[0]->formatted_address);
        } else {
            return response()->json("");
        }
    }

    public function cancelTrip(Request $request)
    {
        DB::update('update trip set status = ? where id=?',
            [$request->check, $request->id]);
        return response()->json($request);
    }

    private function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function calculations($start, $destination)
    {

        $apiUrl = 'http://maps.googleapis.com/maps/api/directions/json';
        $url = $apiUrl . '?' . 'origin=' . urlencode($start) . '&destination=' . urlencode($destination) . '&region=EG';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($curl);
        if (curl_errno($curl)) {
            throw new Exception(curl_error($curl));
        }
        $json = json_decode(trim($res), true);
        $route = $json['routes'][0];
        $totalDistance = 0;
        foreach ($route['legs'] as $leg) {
            $totalDistance = $totalDistance + $leg['distance']['value'];
        }
        $totalDistance = round($totalDistance / 1000);

        $response = array();
        $response["duration"] = $json['routes']['0']['legs']['0']['duration']['text'];
        $response["distance"] = $json['routes']['0']['legs']['0']['distance']['text'];

        $time = explode(' ', $response["duration"]);
        if (sizeof($time) < 3) {
            if ($time[1] == "hour") {
                $response["distanceMins"] = (float) ($time[0] * 60);
            } else {
                $response["distanceMins"] = (float) ($time[0]);
            }
        } else {
            $timeFinal = (float) ($time[0] * 60) + (float) ($time[2]);
            $response["distanceMins"] = $timeFinal;
        }
        return $response;
    }

}
