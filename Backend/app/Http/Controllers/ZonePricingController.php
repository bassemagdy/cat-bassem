<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Mail;

class ZonepricingController extends Controller
{


    public function index(){
        // $zones = DB::select('select zone_pricing.id , zone_pricing.from_id , zone_pricing.to_id , zone_pricing.price , zone_pricing.peak_hour_price from zone_pricing ');
       $zones = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
       from zone_pricing
       left join zone on zone_pricing.from_id = zone.id
       left join zone as Z on zone_pricing.to_id = Z.id
       ');


        $count = count($zones);
        foreach ($zones as $zone) {
          $type = DB::select('select * from car_type where id='.$zone->car_type_id);
          if($type){
            // die(json_encode($type));
            $zone->car_type = $type[0]->model.' '.$type[0]->manufacturer;
          }else{
            $zone->car_type = '_';
          }
        }
        $response = (object)[ "headersType" => array( "string","string","int","int","int"),
        "headers" => array("From","To","Price","Peak Hour Price","Type"),
        "data" => $zones,
        "pageSize" => 10,
        "limit"=> 100,
        "count" => $count];

        return response()->json($response);
    }

    public function getprice(Request $request){
        $fromid =$request->from_id;
        $toid =$request->to_id;
        $price = DB::select('select * from zone_pricing where from_id = ? AND to_id =?',[$fromid, $toid]);
//         if($price){
//         $price2 = $price[0]->price;
//  }
 return response()->json($price);
    }


    public function getChilds($id)
    {
      $result = DB::select('select id from zone where parent_zone_id = ?', [$id]);
      return $result;
    }
    public function getOtherZone(Request $request){
        $result = DB::select('select id from zone_pricing where from_id = ? AND to_id= ? AND car_type_id=? AND price=? AND peak', [$request->to_id,$request->from_id,$request->car_type]);
        return $result;
    }
    // public function getOtherZone(Request $request){
    //     $result = DB::select('select id from zone_pricing where from_id = ? AND to_id= ? AND car_type_id=? AND price=? AND peak_hour_price=?', [$request->to_id,$request->from_id,$request->car_type,$request->price,$request->peak_hour_price]);
    //     return $result;
    // }

    public function addZonePricing($from_id,$to_id,$price,$peak_hour_price,$car_type_id)
    {   
        // DB::select('select  from zone_pricing')
        DB::insert('insert into zone_pricing (from_id,to_id,price,peak_hour_price,car_type_id ) values (? , ? , ? , ? , ?)',[$from_id,$to_id,$price,$peak_hour_price,$car_type_id]);
        DB::insert('insert into zone_pricing (from_id,to_id,price,peak_hour_price,car_type_id ) values (? , ? , ? , ? , ?)',[$to_id,$from_id,$price,$peak_hour_price,$car_type_id]);
    }

    public function editZonePricing($from_id,$to_id,$price,$peak_hour_price,$car_type_id,$id)
    {
        DB::update('update zone_pricing set from_id = ? , to_id = ? , price = ? , peak_hour_price = ?, car_type_id=?  where id=?',
        [$from_id ,$to_id, $price, $peak_hour_price, $car_type_id,$id]);

    }

    public function create(Request $request){

        $fromTemp = DB::select('select id from zone where id = ?', [$request->zone["from_id"]])[0];
        $toTemp = DB::select('select id from zone where id = ?', [$request->zone["to_id"]])[0];
        $price = $request->zone['price'];
        $peak_hour_price = $request->zone['peak_hour_price'];
        $car_type_id = $request->zone['car_type_id'];
        $from = array($fromTemp->id);
        $to = array($toTemp->id);
        foreach ($this->getChilds($fromTemp->id) as $zone) {
          $from[] = $zone->id;
        }
        foreach ($this->getChilds($toTemp->id) as $zone) {
          $to[] = $zone->id;
        }
        foreach ($from as $from_id) {
          foreach ($to as $to_id) {
            $this->addZonePricing($from_id,$to_id,$price,$peak_hour_price,$car_type_id);
          }
        }

        return response()->json(['status' => 'success','data' => true]);
    }

    public function view(Request $request)
    {
        $status = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
        from zone_pricing
        left join zone on zone_pricing.from_id = zone.id
        left join zone as Z on zone_pricing.to_id = Z.id
        where zone_pricing.id = ?', [$request->id]);
        return response()->json($status[0]);
    }


    public function edit(Request $request)
    {
        // $other_id= $this->getOtherZone($request->to_id,$request->from_id,$request->price,$request->peak_hour_price,$request->car_type);
        // return response()->json($request);
        // return response()->json($other_id[0]);
        $this->editZonePricing($request->from_id,$request->to_id,$request->price,$request->peak_hour_price,$request->car_type,$request->id);
        // if(count($other_id>0)){
        // $this->editZonePricing($request->to_id,$request->from_id,$request->price,$request->peak_hour_price,$request->car_type,$other_id[0]);
    // }
        return response()->json("okay" );
    }


    public function delete(Request $request)
    {
      $del =  DB::delete('delete from zone_pricing where id = ?', [$request->id]);

        return response()->json("okay");
    }

    public function paginate(Request $request)
    {
        
        if($request->columns && $request->keyword)
        {   
            $sqlstring = '';
            foreach ($request->columns as $column) {
                 $sqlstring = $sqlstring.','.$column;
            }

            if(!$request->column)
            {
               if($request->page%10 == 0)
                   $results = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
                   from zone_pricing
                   left join zone on zone_pricing.from_id = zone.id
                   left join zone as Z on zone_pricing.to_id = Z.id
                                           limit '.$request->page.',20');
               else
                   $results = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
                   from zone_pricing
                   left join zone on zone_pricing.from_id = zone.id
                   left join zone as Z on zone_pricing.to_id = Z.id
                                           limit '.$request->page.',20');

            }
            else
            {
               if($request->page%10 == 0)
                   
                   $results = DB::select(' select zone_pricing .* , zone.name as F ,Z.name as T
                   from zone_pricing
                   left join zone on zone_pricing.from_id = zone.id
                   left join zone as Z on zone_pricing.to_id = Z.id
                                           order by '.$request->column.' '.$request->order.
                                           ' limit '.$request->page.',20');
                else
                  $results = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
                  from zone_pricing
                  left join zone on zone_pricing.from_id = zone.id
                  left join zone as Z on zone_pricing.to_id = Z.id
                                       order by '.$request->column.' '.$request->order.
                                       ' limit '.$request->page.',20');
                
            }
           
        }
        else 
            if( $request->column)
            {
               if($request->page%10 == 0)
                   $results = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
                   from zone_pricing
                   left join zone on zone_pricing.from_id = zone.id
                   left join zone as Z on zone_pricing.to_id = Z.id
                                         order by '.$request->column.' '.$request->order.
                                        ' limit '.$request->page.',20');
                else
                $results = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
                from zone_pricing
                left join zone on zone_pricing.from_id = zone.id
                left join zone as Z on zone_pricing.to_id = Z.id
                order by '.$request->column.' '.$request->order.
                                       ' limit '.$request->page.',20');
                
            }
            else {
                if($request->page%10 == 0)
                   $results = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
                   from zone_pricing
                   left join zone on zone_pricing.from_id = zone.id
                   left join zone as Z on zone_pricing.to_id = Z.id
                                       limit '.$request->page.',20');
               else
                   $results = DB::select('select zone_pricing .* , zone.name as F ,Z.name as T
                   from zone_pricing
                   left join zone on zone_pricing.from_id = zone.id
                   left join zone as Z on zone_pricing.to_id = Z.id
                                       limit '.$request->page.',20');
        
            }
                 

        return response()->json($results);
    }



}
