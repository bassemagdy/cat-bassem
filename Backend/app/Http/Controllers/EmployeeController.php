<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Mail;

class EmployeeController extends Controller
{
    public function index()
    {}

    public function getEmployeesOfManager(Request $request)
    {

        $customers = DB::select('select name, id from employee where manager_id = ? ', [$request->id]);
        return response()->json($customers);
    }

    public function create(Request $request)
    {

        $hashed_random_password = str_random(8);

        $data = [
            'name' => $request->employee["name"],
            'username' => $request->employee["username"],
            'password' => $hashed_random_password,
            'email' => $request->employee["email"],
            'CRMLINK' => 'http://catphaseone.azurewebsites.net/',
        ];

        Mail::send('email.registration', $data, function ($message) use ($data) {
            $message->to($data['email'])->subject('Registration Details');
        });

        DB::insert('insert into employee (name,email,phone,department,username,password,manager_id,role,disable)
                    values (?,?,?,?,?,?,?,?,?)', [$request->employee["name"], $request->employee["email"], $request->employee["phone"], $request->employee["department"],
            $request->employee["username"], Hash::make($hashed_random_password), $request->manager_id, $request->employee["role"], '0']);

        return response()->json("okay");
    }

    public function viewAllEmployees()
    {

        $data = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                                  employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable,department.id as department_id,
                                  roles.id as role_id
                             from employee
                             left join department on employee.department = department.id
                             left join roles on employee.role = roles.id
                             left join employee as e on e.id = employee.manager_id
                             limit 0, 100');
        $count = DB::select('select count(*) as count
                               from employee');
        $response = (object) ["headersType" => array("string", "string", "string", "string", "string", "string", "string"),
            "headers" => array("Name", "Email", "Phone", "Department", "Username", "Manager", "Role"),
            "data" => $data,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count[0]];

        return response()->json($response);
    }

    public function paginate(Request $request)
    {

        if ($request->columns && $request->keyword) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }

            if (!$request->column) {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                    employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                           from employee
                                           left join department on employee.department = department.id
                                           left join roles on employee.role = roles.id
                                           left join employee as e on e.id = employee.manager_id
                                           WHERE
                                           MATCH (employee.username,employee.name,employee.phone,employee.email,employee.address) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(department.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(roles.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(e.name)  AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
                } else {
                    $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                    employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                           from employee
                                           left join department on employee.department = department.id
                                           left join roles on employee.role = roles.id
                                           left join employee as e on e.id = employee.manager_id
                                           WHERE
                                           MATCH (employee.username,employee.name,employee.phone,employee.email,employee.address) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(department.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(roles.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(e.name)  AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
                }

            } else {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                    employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                           from employee
                                           left join department on employee.department = department.id
                                           left join roles on employee.role = roles.id
                                           left join employee as e on e.id = employee.manager_id
                                           WHERE
                                           WHERE
                                           MATCH (employee.username,employee.name,employee.phone,employee.email,employee.address) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(department.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(roles.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(e.name)  AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
                } else {
                    $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                    employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                           from employee
                                           left join department on employee.department = department.id
                                           left join roles on employee.role = roles.id
                                           left join employee as e on e.id = employee.manager_id
                                           WHERE
                                           MATCH (employee.username,employee.name,employee.phone,employee.email,employee.address) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(department.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(roles.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           or MATCH(e.name)  AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
                }

            }

        } else
        if ($request->column) {
            if ($request->page % 10 == 0) {
                $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                       from employee
                                       left join department on employee.department = department.id
                                       left join roles on employee.role = roles.id
                                       left join employee as e on e.id = employee.manager_id
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
            } else {
                $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                       from employee
                                       left join department on employee.department = department.id
                                       left join roles on employee.role = roles.id
                                       left join employee as e on e.id = employee.manager_id
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
            }

        } else {
            if ($request->page % 10 == 0) {
                $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                    employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                           from employee
                                           left join department on employee.department = department.id
                                           left join roles on employee.role = roles.id
                                           left join employee as e on e.id = employee.manager_idselect employee.id,employee.username,e.name as manager_name,employee.manager_id,
                                           employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                                                  from employee
                                                                  left join department on employee.department = department.id
                                                                  left join roles on employee.role = roles.id
                                                                  left join employee as e on e.id = employee.manager_id
                                            limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
            } else {
                $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                    employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                           from employee
                                           left join department on employee.department = department.id
                                           left join roles on employee.role = roles.id
                                           left join employee as e on e.id = employee.manager_id
                                           limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
            }

        }

        return response()->json($results);
    }

    public function search(Request $request)
    {
        if (trim($request->keyword) == "") {
            $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
            employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                from employee
                                left join department on employee.department = department.id
                                left join roles on employee.role = roles.id
                                left join employee as e on e.id = employee.manager_id
                                limit 0, 100');
            $count = DB::select('select count(*) as count from employee');

            $response = (object) ["data" => $results,
                "count" => $count[0]];
            error_log("hi");
            return response()->json($response);
        } else {
            $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
            employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                from employee
                                left join department on employee.department = department.id
                                left join roles on employee.role = roles.id
                                left join employee as e on e.id = employee.manager_id
                                WHERE
                                MATCH (employee.username,employee.name,employee.phone,employee.email,employee.address) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                or MATCH(department.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                or MATCH(roles.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                or MATCH(e.name)  AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)');
            $count = DB::select('select count(*) as count from employee
                                WHERE MATCH (employee.username,employee.name,employee.phone,employee.email,employee.address)
                                AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)');

            $response = (object) ["data" => $results,
                "count" => $count[0]];
            error_log("hi");
            return response()->json($response);
        }
    }

    public function sort(Request $request)
    {
        if ($request->column == "Department") {
            $request->column = "department_name";
        }

        if ($request->column == "Role") {
            $request->column = "role_name";
        }

        if ($request->column == "Manager") {
            $request->column = "manager_name";
        }

        if ($request->keyword && $request->columns) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }
            $results = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
            employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                   from employee
                                   left join department on employee.department = department.id
                                   left join roles on employee.role = roles.id
                                   left join employee as e on e.id = employee.manager_id
                                   WHERE
                                    MATCH (employee.username,employee.name,employee.phone,employee.email,employee.address) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                    or MATCH(department.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                    or MATCH(roles.name) AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                    or MATCH(e.name)  AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                    order by ' . $request->column . ' ' . $request->order . '
                                   limit 0,100');
        } else {
            error_log('select * from employee limit 0,100');
            $results = DB::select('
            select employee.id,employee.username,e.name as manager_name,employee.manager_id,
                    employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable
                                           from employee
                                           left join department on employee.department = department.id
                                           left join roles on employee.role = roles.id
                                           left join employee as e on e.id = employee.manager_id
            order by ' . $request->column . ' ' . $request->order . ' limit 0,100');
        }

        return response()->json($results);
    }

    public function getEmployee(Request $request)
    {
        $employee = DB::select('select employee.id,employee.username,e.name as manager_name,employee.manager_id,
        employee.name,employee.email,employee.phone,department.name as department_name,roles.name as role_name,employee.disable,employee.address, department.id as department_id,roles.id as role_id
                               from employee
                               left join department on employee.department = department.id
                               left join roles on employee.role = roles.id
                               left join employee as e on e.id = employee.manager_id where employee.id = ?', [$request->id]);
        return response()->json($employee[0]);
    }

    public function editProfile(Request $request)
    {

        $q = substr($request->employee["password"], 0, 3);
        if ($q != "$2a" && $q != "$2y") {
            DB::update('update employee set name = ?, email = ?, phone = ?, password = ? where id = ?',
                [$request->employee["name"], $request->employee["email"], $request->employee["phone"], Hash::make($request->employee["password"]),
                    $request->employee["id"]]);

            return response()->json("okay");
        } else {
            DB::update('update employee set name = ?, email = ?, phone = ?, department = ?, username = ?, manager_id = ?, role = ? where id = ?',
                [$request->employee["name"], $request->employee["email"], $request->employee["phone"], $request->employee["department"],
                    $request->employee["username"], $request->employee["manager_id"], $request->employee["role"], $request->employee["id"]]);

            return response()->json("okay");
        }
    }

    public function editEmployee(Request $request)
    {
        DB::update('update employee set name = ?, email = ?, phone = ?, department = ?, username = ?,  manager_id = ?, role = ?, address = ? where id = ?',
            [$request->employee["name"], $request->employee["email"], $request->employee["phone"], $request->employee["department_id"],
                $request->employee["username"], $request->employee["manager_id"], $request->employee["role_id"], $request->employee["address"], $request->employee["id"]]);

        return response()->json("okay");
    }

    public function deleteEmployee(Request $request)
    {
        DB::delete('delete from employee where id = ?', [$request->id]);
        return response()->json("okay");
    }

    public function disableEmployee(Request $request)
    {
        DB::update('update employee set disable= ? where id = ?', [$request->disable, $request->empid]);
        return response()->json("okay");
    }

    public function sendmail(Request $request)
    {

        /*            $key = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDPZAyRttWQPLE41fLG7JYYo43vA0PK0pzmLdGdy3v+/NNxyLNXwArNLMI+KH0rj4oqIyUUyziD0uY3fz2dJ35Rvfafwtg5UNqdAYajT67lDtbsci5XqmAAwWhXDvUlMU2fgAWSvbC7bMP+goOu7UCy9Z0AD5W83nOCnzDcAsDW2QIDAQAB';

    $sPubKey = openssl_get_publickey($key);

    $oApi = new SmtpApi($sPubKey);

    $response = Curl::to('https://api.sendpulse.com/smtp/emails');
    $aEmail =
    array('html' => '<p>HTML text of email message</p>',
    'text' => 'Email message text',
    'encoding' => 'UTF-8',
    'subject' => 'Email message subject',
    'from' => array(
    'name' => 'Sender Name',
    'email' => 'sender@example.com',
    ),
    'to' => array(
    array(
    'name' => 'Recipient Name',
    'email' => 'mi2015do@hotmail.com'
    ),
    array(
    'email' => 'recipient2@example.com'
    ),
    ),
    'bcc' => array(
    array(
    'name' => 'Recipient Name',
    'email' => 'recipient3@example.com'
    ),
    array(
    'email' => 'recipient4@example.com'
    ),
    ),
    );

    $res = $oApi->send_email($aEmail);
    returnResponseObject();

    // $content = $response->content;

    if ($res['error']){ // check if operation succeeds
    $content = $response->content;

    return response()->json($content);

    } else {
    return response()->json("ok");

    }

    }

     */

    }
}
