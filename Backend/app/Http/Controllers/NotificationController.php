<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function view(Request $request)
    {

        $notifications = DB::select('select *  from notifications where notifications.user_id = ? order by date DESC limit 0,4 ', [$request->id]);
        return response()->json($notifications);
    }
}
