<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceController extends Controller
{
    //

    public function index()
    {

    }

    public function createService(Request $request)
    {
        $sqlarray = "";
        for ($i = 0; $i < count($request->data); $i++) {
            $sqlarray = $sqlarray . $request->data[$i][0] . ':' . $request->data[$i][1];
            if (count($request->data[$i]) > 2) {
                $sqlarray = $sqlarray . ':' . implode(',', $request->data[$i][2]);
            }

            $sqlarray = $sqlarray . ';';
        }

        $sqlarray = "price:int;" . $sqlarray;

        DB::insert('insert into service_category (name,definition) values (?,?)', [$request->name, $sqlarray]);
        return response()->json("okay");
    }

    public function getAllServiceCategories()
    {
        $results = DB::select('select * from service_category');
        return response()->json($results);
    }

    public function getCategory(Request $request)
    {
        $results = DB::select('select * from service_category where id = ?', [$request->id]);
        return response()->json($results);
    }

    public function deleteCategory(Request $request)
    {
        DB::delete('delete from service_category where id = ?', [$request->id]);
        return response()->json("okay");
    }

    public function editServiceCategory(Request $request)
    {
        $sqlarray = "";
        for ($i = 0; $i < count($request->data); $i++) {
            $sqlarray = $sqlarray . $request->data[$i][0] . ':' . $request->data[$i][1];
            if (count($request->data[$i]) > 2 && count($request->data[$i][2]) > 0) {
                $sqlarray = $sqlarray . ':' . implode(',', $request->data[$i][2]);
            }

            $sqlarray = $sqlarray . ';';
        }

        DB::insert('update service_category  set name = ?, definition = ? where id = ?', [$request->name, $sqlarray, $request->id]);
        return response()->json("okay");
    }

    public function getPrice(Request $request)
    {
        $results = DB::select('select price from service_lead where lead_id = ?', [$request->id]);
        return response()->json($results);
    }
}
