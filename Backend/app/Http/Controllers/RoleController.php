<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    //
    public function index()
    {}

    public function getPrivileges()
    {
        $privileges = DB::select('select * from privileges order by id');
        return response()->json($privileges);
    }

    public function createRole(Request $request)
    {
        $privileges = array();
        for ($i = 0; $i < count($request->values); $i++) {
            $privileges[] = $request->values[$i];
        }

        $val = implode(',', $privileges);

        DB::insert('insert into roles (name, value, privileges) values (?,?,?)', [$request->rolename, $val, implode($request->names)]);
        return response()->json("okay");

    }

    public function viewAllRoles()
    {
        $data = DB::select('select roles.id,roles.name,count(employee.name) as count,privileges
                             from roles
                             left join employee on employee.role = roles.id
                             group by roles.id
                             limit 0, 100');
        $count = DB::select('select count(*) as count
                               from roles');
        $response = (object) ["headersType" => array("string", "string", "int", "string"),
            "headers" => array("Name", "Count", "Value", "Privileges"),
            "data" => $data,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count[0]];

        return response()->json($response);
    }

    public function paginate(Request $request)
    {

        if ($request->columns && $request->keyword) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }

            if (!$request->column) {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select * from roles
                                           WHERE MATCH (privileges, name)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
                } else {
                    $results = DB::select('select * from roles
                                           WHERE MATCH (privileges, name)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
                }

            } else {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select * from roles
                                           WHERE MATCH (privileges, name)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)   order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
                } else {
                    $results = DB::select('select * from roles
                                           WHERE MATCH (privileges, name)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)   order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
                }

            }

        } else
        if ($request->column) {
            if ($request->page % 10 == 0) {
                $results = DB::select('select *
                                       from roles
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
            } else {
                $results = DB::select('select *
                                       from roles
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
            }

        } else {
            if ($request->page % 10 == 0) {
                $results = DB::select('select *
                                           from roles limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
            } else {
                $results = DB::select('select *
                                          from roles limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
            }

        }

        return response()->json($results);
    }

    public function search(Request $request)
    {
        $results = DB::select('select * from roles
                               WHERE MATCH (privileges, name)
                               AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)  limit 0, 100');
        $count = DB::select('select count(*) as count from roles
                               WHERE MATCH (privileges, name)
                               AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)');

        $response = (object) ["data" => $results,
            "count" => $count[0]];
        error_log("hi");
        return response()->json($response);
    }

    public function sort(Request $request)
    {
        if ($request->keyword && $request->columns) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }
            $results = DB::select('select * from roles
                                   WHERE MATCH (privileges, name)
                                   AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE) order by ' . $request->column . ' ' . $request->order . '
                                   limit 0,100');
        } else {
            error_log('select * from roles limit 0,100');
            $results = DB::select('select  * from roles order by ' . $request->column . ' ' . $request->order . ' limit 0,100');
        }

        return response()->json($results);
    }

    public function edit(Request $request)
    {
        error_log("is this working");
        $val = $request->value;
        $added = $request->privileges;

        $privileges = array();

        $parray = explode(";", $request->privileges);

        for ($i = 0; $i < (count($parray) - 1); $i++) {
            // $x = DB::select ('select value from privileges where match (name) against (\'+'.$parray[$i].'*\' IN BOOLEAN MODE)');
            $x = DB::select('select value from privileges where name = ?', [$parray[$i]]);

            if ($x) {
                $privileges[$i] = $x[0]->value;
            }
        }

        $val = implode(";", $privileges);

        DB::update('update roles set value = ?, name = ?, privileges = ? where id = ?', [$val, $request->name, $request->privileges, $request->id]);
        return response()->json("okay");
    }

    public function get(Request $request)
    {
        $result = DB::select('select * from  roles where id = ?', [$request->id]);
        return response()->json($result);
    }

    public function delete(Request $request)
    {
        $del = DB::delete('delete from roles where id = ?', [$request->delete]);
        if ($del) {
            DB::update('update employee set role = ? where role = ?', [$request->replacement, $request->delete]);

        }
        return response()->json("okay");
    }

    public function viewcount(Request $request)
    {

        $resources = DB::select('select * from employee where role = ?', [$request->id]);
        return response()->json(count($resources));

    }

}
