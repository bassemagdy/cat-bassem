<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DriverController extends Controller
{

    public function index()
    {
        $drivers = DB::select('select driver.id, driver.name, driver.national_id, driver.phone_number,	driver.license_number,	driver.license_degree,	driver.license_expiry,	driver.code from driver ');

        $count = count($drivers);

        $response = (object) ["headersType" => array("string", "int", "int", "int", "string", "date", "string"),
            "headers" => array("Name", "national_id", "phone_number", "license_number", "license_degree", "license_expiry", "code"),
            "data" => $drivers,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);
    }

    public function create(Request $request)
    {

        $data = [

            'name' => $request->driver["name"],
            'national_id' => $request->driver["national_id"],
            'phone_number' => $request->driver["phone_number"],
            'license_number' => $request->driver["license_number"],
            'license_degree' => $request->driver["license_degree"],
            'license_expiry' => $request->driver["license_expiry"],
        ];

        $code = str_random(6);

        DB::insert('insert into driver (name ,national_id , phone_number , license_number, license_degree , license_expiry ,code) values (? , ? , ? , ? ,? ,? ,?)',
            [$request->driver["name"], $request->driver["national_id"], $request->driver["phone_number"], $request->driver["license_number"], $request->driver["license_degree"], $request->driver["license_expiry"], $code]);
        return response()->json($code);

    }
    public function view(Request $request)
    {
        $driver = DB::select('select driver.* from driver where driver.id = ?', [$request->id]);
        return response()->json($driver[0]);
    }

    public function edit(Request $request)
    {
        DB::update('update driver set name = ? , national_id = ? , phone_number = ? , license_number= ? , license_degree = ? , license_expiry = ?  where id=?',
            [$request->name, $request->national_id, $request->phone_number, $request->license_number, $request->license_degree, $request->license_expiry, $request->id]);

        return response()->json("okay");
    }

    public function delete(Request $request)
    {
        $del = DB::delete('delete from driver where id = ?', [$request->id]);
        return response()->json("okay");
    }

}
