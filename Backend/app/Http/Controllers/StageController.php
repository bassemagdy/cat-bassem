<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StageController extends Controller
{

    public function index()
    {
        $resources = DB::select('select stage.id,stage.name,stage.sort,count(lead.stage) as count from stage
        left join lead on lead.stage = stage.id
        group by stage.id order by sort ASC');
        $count = count($resources);

        $response = (object) ["headersType" => array("string", "int", "int"),
            "headers" => array("Name", "Leads Count", "Order"),
            "data" => $resources,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);
    }

    public function all()
    {
        $resources = DB::select('select stage.*, stage.name as name  from stage order by sort');
        return response()->json($resources);

    }

    public function create(Request $request)
    {

        $data = [

            'name' => $request->resources["name"],
        ];

        DB::insert('insert into stage (name,sort) values (?,?)', [$request->name, $request->sort]);
        return response()->json($data);

    }
    public function view(Request $request)
    {
        $resources = DB::select('select stage.*, stage.name as name  from stage where stage.id = ?', [$request->id]);
        return response()->json($resources[0]);
    }

    public function edit(Request $request)
    {
        DB::update('update stage set name = ?,sort = ? where id=?',
            [$request->name, $request->sort, $request->id]);

        return response()->json("okay");
    }

    public function delete(Request $request)
    {
        $del = DB::delete('delete from stage where id = ?', [$request->delete]);
        if ($del) {
            DB::update('update lead_stage set stage_id = ? where stage_id = ?', [$request->replacement, $request->delete]);

            DB::update('update lead set stage = ? where stage = ?', [$request->replacement, $request->delete]);

        }
        return response()->json("okay");
    }

    public function viewcount(Request $request)
    {

        $resources = DB::select('select * from lead where stage = ?', [$request->delete]);
        return response()->json(count($resources));

    }

}
