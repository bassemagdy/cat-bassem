<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    //

    public function index()
    {

    }

    public function createProduct(Request $request)
    {
        $sqlarray = "";
        for ($i = 0; $i < count($request->data); $i++) {
            $sqlarray = $sqlarray . $request->data[$i][0] . ':' . $request->data[$i][1];
            if (count($request->data[$i]) > 2) {
                $sqlarray = $sqlarray . ':' . implode(',', $request->data[$i][2]);
            }

            $sqlarray = $sqlarray . ';';
        }

        $sqlarray = "price:int;" . $sqlarray;
        $sqlarray = "limit:int;" . $sqlarray;

        DB::insert('insert into product_category (name,definition) values (?,?)', [$request->name, $sqlarray]);
        return response()->json("okay done");
    }

    public function getProductCategory(Request $request)
    {
        $result = DB::select('select * from  product_category where id = ?', [$request->id]);
        return response()->json($result);
    }

    public function addProductInstance(Request $request)
    {
        $sqlarray = "";
        $price;
        $limit;

        for ($i = 0; $i < count($request->data); $i++) {
            if (strpos($request->data[$i], 'price:') !== false) {
                $price = substr($request->data[$i], 6);
            }

            if (strpos($request->data[$i], 'limit:') !== false) {
                $limit = substr($request->data[$i], 6);
            }

            $sqlarray = $sqlarray . $request->data[$i] . ';';
        }

        DB::insert('insert into product_instance (category_id,name,data,price,limited) values (?,?,?,?,?)', [$request->id, $request->name, $sqlarray, $price, $limit]);
        return response()->json("okay man");
    }

    public function getAllProducts()
    {
        $results = DB::select('select product_instance.id, product_category.name as "Category", product_instance.name as "Name", product_instance.data from product_instance
                               inner join product_category on product_instance.category_id=product_category.id where deleted!=1
                               ');
        return response()->json($results);
    }

    public function getAllProductCategories()
    {
        $results = DB::select('select * from product_category');
        return response()->json($results);
    }

    public function getCategory(Request $request)
    {
        $results = DB::select('select * from product_category where id = ?', [$request->id]);
        return response()->json($results);
    }

    public function getInstance(Request $request)
    {
        $results = DB::select('select * from product_instance inner join product_category on product_instance.category_id = product_category.id where product_instance.id = ?', [$request->id]);
        return response()->json($results);
    }

    public function editProductInstance(Request $request)
    {
        $sqlarray = "";
        $price;
        $limit;

        for ($i = 0; $i < count($request->data); $i++) {
            if ($request->data[$i][0] == 'price') {
                $price = $request->data[$i][1];
            }

            if ($request->data[$i][0] == 'limit') {
                $limit = $request->data[$i][1];
            }

            for ($j = 0; $j < 2; $j++) {
                $sqlarray = $sqlarray . $request->data[$i][$j];
                if ($j != 1) {
                    $sqlarray = $sqlarray . ':';
                }

            }
            $sqlarray = $sqlarray . ';';
        }

        DB::insert('update product_instance set name = ?, data = ?, price = ?, limited = ? where id = ?', [
            $request->name,
            $sqlarray,
            $price,
            $limit,
            $request->id]);
        return response()->json("okay");
    }

    public function editProductCategory(Request $request)
    {
        $sqlarray = "";
        for ($i = 0; $i < count($request->data); $i++) {
            $sqlarray = $sqlarray . $request->data[$i][0] . ':' . $request->data[$i][1];
            if (count($request->data[$i]) > 2 && count($request->data[$i][2]) > 0) {
                $sqlarray = $sqlarray . ':' . implode(',', $request->data[$i][2]);
            }

            $sqlarray = $sqlarray . ';';
        }

        DB::insert('update product_category  set name = ?, definition = ? where id = ?', [$request->name, $sqlarray, $request->id]);
        return response()->json("okay");
    }

    public function deleteCategory(Request $request)
    {
        DB::delete('delete from product_category where id = ?', [$request->id]);
        return response()->json("okay");
    }

    public function deleteInstance(Request $request)
    {
        DB::update('update product_instance set deleted=1 where id = ?', [$request->id]);
        return response()->json("okay");
    }

    public function getTotalInstance(Request $request)
    {
        $results = DB::select('select product_instance.price as price, product_lead.count  as limited
        from product_instance  inner join product_lead
        on product_instance.id = product_lead.instance_id
        inner join product_category
        on product_instance.category_id = product_category.id
         where lead_id = ?', [$request->id]);

        return response()->json($results);
    }

}
