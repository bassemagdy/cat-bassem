<?php

namespace App\Http\Controllers;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class DynamicTablesController extends Controller
{
    //
    public function index()
    {

    }

    public function createTable(Request $request)
    {
        Schema::create($request->tablename, function (Blueprint $table) {
            $table->increments('id');
        });
        return response()->json("okay");

    }

    public function addColumn(Request $request)
    {
        $this->colname = $request->colname;
        $this->coltype = $request->coltype;

        Schema::table(strtolower($request->tablename), function (Blueprint $table) {
            if ($this->coltype == "varchar") {
                $table->string($this->colname);
            } else if ($this->coltype == "int") {
                $table->integer($this->colname);
            } else if ($this->coltype == "boolean") {
                $table->boolean($this->colname);
            } else if ($this->coltype == "datetime") {
                $table->dateTime($this->colname);
            } else if ($this->coltype == "date") {
                $table->date($this->colname);
            } else if ($this->coltype == "time") {
                $table->time($this->colname);
            }

        });
        return response()->json("okay");

    }

    public function deleteColumn(Request $request)
    {
        $this->coldel = $request->coldel;
        error_log($this->coldel);
        Schema::table($request->tablename, function (Blueprint $table) {
            $table->dropColumn($this->coldel);
        });

        return response()->json("okay");
    }

    public function addRow(Request $request)
    {
        error_log($request);
        //DB::insert('insert into '. $request->tablename.' values ('. .')');
    }

}
