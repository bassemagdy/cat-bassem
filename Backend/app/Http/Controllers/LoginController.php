<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use JWTAuth;
use Mail;
use Tymon\JWTAuthExceptions\JWTException;

class LoginController extends Controller
{
    //
    public function index()
    {

    }

    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Invalid Username or Password'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'Invalid Username or Password'], 401);
        }

        $users = DB::select('select * from employee where username = ?', [$request->username]);
        $privilages = DB::select('select * from roles where id = ?', [$users[0]->role]);

        $disbale = $users[0]->disable;

        if ($disbale == 1) {
            return response()->json(['error' => 'User is not allowed to login'], 401);
        }

        $request->session()->put('id', $users[0]->id);
        $request->session()->save();
        Session::put('id', $users[0]->id);
        Session::save();

        $response = (object) array(
            "id" => $users[0]->id,
            "role" => $users[0]->role,
            "name" => $users[0]->name,
            "token" => $token,
            "privilages" => "," . $privilages[0]->value . ",");
        // if no errors are encountered we can return a JWT
        return response()->json($response);
    }

    public function forgotPassword(Request $request)
    {
        $link_hash = str_random(8);

        $user = DB::select('select * from employee where email = ?', [$request->email]);
        $update = DB::update('update employee set vercode = ? where email = ?', [$link_hash, $request->email]);

        if ($update) {
            $data = [
                'name' => $user[0]->username,
                'email' => $request->email,
                'link' => $link_hash,
            ];

            Mail::send('email.forgotpassword', $data, function ($message) use ($data) {
                $message->to($data['email'])
                    ->subject('Forgot Password?');
            });
            return response()->json("An email has been sent to restore your password.");
        }
        return response()->json("This email is incorrect.");

    }

    public function resetPassword(Request $request)
    {
        $user = DB::select('select * from employee where vercode = ?', [$request->code]);
        $update = DB::update('update employee set password = ? , vercode = null where vercode = ?', [$request->password, $request->code]);
        error_log($update);
        if ($update) {
            //return response()->json(user[0]->id);
            return response()->json("okay");
        } else {
            return response()->json("not okay");
        }

    }

}
