<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartypeController extends Controller
{

    public function index()
    {
        $types = DB::select('select car_type.id , car_type.model ,  car_type.manufacturer , car_type.productionYear, car_type.NumberOfPassengers, car_type.NumberOfBags from car_type ');
        $count = count($types);

        $response = (object) ["headersType" => array("string", "string", "string","string","string"),
            "headers" => array("Model", "Manufacturer", "productionYear", "capacityPeople", "capacityBags"),
            "data" => $types,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);
    }

    public function create(Request $request)
    {

        $data = [

            'model' => $request->car_type["model"],
            'manufacturer' => $request->car_type["manufacturer"],
            'model' => $request->car_type["productionYear"],
            'NumberOfPassengers' => $request->car_type["capacityPeople"],
            'NumberOfBags' => $request->car_type["capacityPeople"],

        ];

        DB::insert('insert into car_type (model , manufacturer ,productionYear,NumberOfPassengers, NumberOfBags ) values (? , ? , ?,?,?)', [$request->car_type["model"], $request->car_type["manufacturer"], $request->car_type["productionYear"], $request->car_type["capacityPeople"], $request->car_type["capacityBags"]]);
        return response()->json($data);

    }
    public function view(Request $request)
    {
        $type = DB::select('select car_type.*, car_type.model  , car_type.manufacturer  , car_type.productionYear,  car_type.NumberOfPassengers, car_type.NumberOfBags from car_type where car_type.id = ?', [$request->id]);
        return response()->json($type[0]);
    }

    public function edit(Request $request)
    {
        DB::update('update car_type set model = ? ,  manufacturer = ? , productionYear = ?, NumberOfPassengers=?, NumberOfBags=?  where id=?',
            [$request->model, $request->manufacturer, $request->productionYear, $request->capacityPeople, $request->capacityBags, $request->id]);

        return response()->json("okay");
    }

    public function delete(Request $request)
    {
        $del = DB::delete('delete from car_type where id = ?', [$request->id]);

        return response()->json("okay");
    }

}
