<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = DB::select('select department.*,employee.name as manager_name,format((count(e.name)),0) as count
                            from department
                            left join employee on department.manager_id = employee.id
                            left join employee as e on e.department = department.id
                            group by department.id');
        $count = count($departments);

        $response = (object) ["headersType" => array("string", "string", "int"),
            "headers" => array("name", "Manager", "Count"),
            "data" => $departments,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);
    }

    public function all()
    {
        $departments = DB::select('select department.*,employee.name as manager_name from department left join employee on department.manager_id=employee.id ');
        return response()->json($departments);
    }

    public function create(Request $request)
    {
        $data = [
            'name' => $request->department["name"],
            'manager_id' => $request->department["manager"],
        ];

        if (!isset($request->department["manager"])) {
            DB::insert('insert into department (name)
            values (?)', [$request->department["name"]]);
        } else {
            DB::insert('insert into department (name,manager_id)
                        values (?,?)', [$request->department["name"], $request->department["manager"]]);
        }
        return response()->json("okay");
    }

    public function view(Request $request)
    {
        $department = DB::select('select department.*,employee.name as manager_name from department left join employee on department.manager_id=employee.id where department.id = ?', [$request->id]);
        return response()->json($department[0]);
    }

    public function edit(Request $request)
    {
        DB::update('update department set name = ?, manager_id = ? where id=?',
            [$request->department["name"], $request->department["manager_id"], $request->department['id']]);

        return response()->json("okay");
    }

    public function delete(Request $request)
    {
        DB::delete('delete from department where id = ?', [$request->id]);
        return response()->json("okay");
    }

}
