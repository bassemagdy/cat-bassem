<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ZoneController extends Controller
{

    public function index()
    {

        $zones = DB::select('select zone.*, zone.id , zone.latLng , zone.name
                            from zone limit 0,100');

        $count = count($zones);

        $response = (object) [
            "headersType" => array("int", "string", "string", "string"),
            "headers" => array("id", "latLng", "name"),
            "data" => $zones,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);

    }

    public function create(Request $request)
    {
        if ($request->parent_zone_id == -1) {
            $request->parent_zone_id = null;
        }
        DB::insert('insert into zone (latLng,name,parent_zone_id) values (? , ? , ?)',
            [$request->latLng, $request->name, $request->parent_zone_id]);
        return response()->json($request);
    }

    public function delete(Request $request)
    {
        DB::delete('delete from zone where id=?', [$request->id]);
        return response()->json("deleted");
    }

    public function edit(Request $request)
    {
        DB::update('update zone set latLng = ? , name=?',
            [$request->zone["latLng"], $request->zone["name"]]);
        return response()->json("edited");
    }

    public function view(Request $request)
    {
        $zone = DB::select('select zone.*, zone.id , zone.latLng , zone.name, from zone where id = ?', [$request->id]);
        return response()->json($zone[0]);
    }

    public function paginate(Request $request)
    {
        if ($request->columns && $request->keyword) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }

            if (!$request->column) {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select zone.latLng,zone.name
                                           from zone


                                           limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select zone.latLng,zone.name
                    from zone limit ' . $request->page . ',20');
                }

            } else {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select zone.latLng,zone.name from zone
                                           order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select zone.latLng,zone.name from zone
                                           order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . $request->page . ',20');
                }

            }

        } else
        if ($request->column) {
            if ($request->page % 10 == 0) {
                $results = DB::select('select zone.latLng,zone.name from zone
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select zone.latLng,zone.name from zone

                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            }

        } else {
            if ($request->page % 10 == 0) {
                $results = DB::select('select zone.latLng,zone.name from zone

                                            limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select zone.latLng,zone.name from zone

                                           limit ' . $request->page . ',20');
            }

        }

        return response()->json($results);
    }

}
