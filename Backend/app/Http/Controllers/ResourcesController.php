<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResourcesController extends Controller
{

    public function index()
    {
        $resources = DB::select('select resources.id,resources.name,count(lead.source) as count from resources
        left join lead on lead.source = resources.id
        group by resources.id ');
        $count = count($resources);

        $response = (object) ["headersType" => array("string", "int"),
            "headers" => array("Name", "Leads Count"),
            "data" => $resources,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);
    }

    public function all()
    {
        $resources = DB::select('select resources.*, resources.name as name  from resources');
        return response()->json($resources);

    }

    public function create(Request $request)
    {

        $data = [

            'name' => $request->resources["name"],
        ];

        DB::insert('insert into resources (name) values (?)', [$request->name]);
        return response()->json($data);

    }
    public function view(Request $request)
    {
        $resources = DB::select('select resources.*, resources.name as name  from resources where resources.id = ?', [$request->id]);
        return response()->json($resources[0]);
    }

    public function edit(Request $request)
    {
        DB::update('update resources set name = ? where id=?',
            [$request->name, $request->id]);

        return response()->json("okay");
    }

    public function delete(Request $request)
    {
        DB::delete('delete from resources where id = ?', [$request->id]);
        return response()->json("okay");
    }
}
