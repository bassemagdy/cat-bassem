<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function viewAll()
    {

        $customers = DB::select('select customer.*,customer.id , customer.name , customer.email , customer.phone from customer
                            limit 0, 100');
        $count = count($customers);

        $response = (object) [
            "headersType" => array("string", "string", "int"),
            "headers" => array("name", "email", "phone"),
            "data" => $customers,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count];

        return response()->json($response);
    }

    public function paginate(Request $request)
    {

        if ($request->columns && $request->keyword) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }

            if (!$request->column) {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select customer.name,customer.email,customer.phone
                                           from customer


                                           limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select customer.name,customer.email,customer.phone
                                           from customer




                                           limit ' . $request->page . ',20');
                }

            } else {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select customer.name,customer.email,customer.phone
                                          from customer


                                           order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . $request->page . ',20');
                } else {
                    $results = DB::select('select customer.name,customer.email,customer.phone
                                           from customer


                                           order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . $request->page . ',20');
                }

            }

        } else
        if ($request->column) {
            if ($request->page % 10 == 0) {
                $results = DB::select('select customer.name,customer.email,customer.phone
                                       from customer
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select customer.name,customer.email,customer.phone
                                       from customer

                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . $request->page . ',20');
            }

        } else {
            if ($request->page % 10 == 0) {
                $results = DB::select('select customer.name,customer.email,customer.phone
                                           from customer

                                            limit ' . $request->page . ',20');
            } else {
                $results = DB::select('select customer.name,customer.email,customer.phone
                                           from customer

                                           limit ' . $request->page . ',20');
            }

        }

        return response()->json($results);
    }

    public function delete(Request $request)
    {

        DB::delete('delete from customer where id = ? ', [$request->id]);
        return response()->json("okay");

    }

    public function viewCustomer(Request $request)
    {

        $res = DB::select('select  customer.name , customer.email , customer.phone ,customer.email2 , customer.phone2 , customer.company , customer.address from customer where customer.id= ? ', [$request->id]);
        return response()->json($res);

    }

    public function addCustomer(Request $request)
    {

// DB::insert('insert into customer ');
        // }
        $customer = DB::table('customer')->insertGetId(
            ['name' => $request->customerName,
                'phone' => $request->customerPhone,
                'phone2' => $request->customerPhone2,
                'payment_info' => $request->customerPayment,
                'address' => $request->customerAddress,
                'email' => $request->customerEmail,
                'email2' => $request->customerEmail2,
                'company' => $request->customerCompany,
                'vip' => $request->vip,
            ]
        );
        return response()->json($customer);

    }

    public function update(Request $request)
    {

        DB::update('update customer set name = ? , address = ? , company = ? , email = ? , phone = ? , phone2 = ? , email2 = ? where customer.id = ? ',
            [$request->customer["customerName"], $request->customer["customerAddress"], $request->customer["customerCompany"], $request->customer["customerEmail"], $request->customer["customerPhone"], $request->customer["customerPhone2"], $request->customer["customerEmail2"], $request->customer["id"]]);
        return response()->json("okay");

    }

}
