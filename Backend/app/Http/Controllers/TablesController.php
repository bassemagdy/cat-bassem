<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TablesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                             customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                             from lead
                             left join customer
                             on customer.id = lead.customer_id
                             left join employee
                             on employee.id = lead.assigned_to
                             left join stage
                             on stage.id = lead.stage
                             limit 0, 100');
        $count = DB::select('select count(*) as count
                             from lead
                             left join customer
                             on customer.id = lead.customer_id
                             left join employee
                             on employee.id = lead.assigned_to');
        $response = (object) ["headersType" => array("string", "string", "string", "date", "string", "date", "string", "string"),
            "headers" => array("CustomerName", "CustomerEmail", "CustomerPhone", "Reminder", "Comment", "Date_Created", "Assigned_To", "Stage"),
            "data" => $data,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count[0]];

        return response()->json($response);
    }

    public function indexx(Request $request)
    {
        $data = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                             customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                             from lead
                             left join customer
                             on customer.id = lead.customer_id
                             left join employee
                             on employee.id = lead.assigned_to
                             left join stage
                             on stage.id = lead.stage
                             where customer_id = ?
                             limit 0, 100
                             ',
            [$request->id]);
        $count = DB::select('select count(*) as count
                             from lead
                             left join customer
                             on customer.id = lead.customer_id
                             left join employee
                             on employee.id = lead.assigned_to');
        $response = (object) ["headersType" => array("string", "string", "string", "date", "string", "date", "string", "string"),
            "headers" => array("CustomerName", "CustomerEmail", "CustomerPhone", "Reminder", "Comment", "Date_Created", "Assigned_To", "Stage"),
            "data" => $data,
            "pageSize" => 10,
            "limit" => 100,
            "count" => $count[0]];

        return response()->json($response);
    }

    public function paginate(Request $request)
    {

        if ($request->columns && $request->keyword) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }

            if (!$request->column) {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                           left join stage
                                           on stage.id = lead.stage
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
                } else {
                    $results = DB::select('select  lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                            left join stage
                                            on stage.id = lead.stage
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                                           limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
                }

            } else {
                if ($request->page % 10 == 0) {
                    $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                            left join stage
                                            on stage.id = lead.stage
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)  order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
                } else {
                    $results = DB::select('select  lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                           left join stage
                                           on stage.id = lead.stage
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)  order by ' . $request->column . ' ' . $request->order . '
                                           limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
                }

            }

        } else
        if ($request->column) {
            if ($request->page % 10 == 0) {
                $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                            left join stage
                                            on stage.id = lead.stage
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
            } else {
                $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                            left join stage
                                            on stage.id = lead.stage
                                       order by ' . $request->column . ' ' . $request->order .
                    ' limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
            }

        } else {
            if ($request->page % 10 == 0) {
                $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                           left join stage
                                           on stage.id = lead.stage
                                           limit ' . (floor(($request->page - 1) / 10) * 100) . ',' . (floor(($request->page - 1) / 10) * 100 + 100));
            } else {
                $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                           left join stage
                                           on stage.id = lead.stage
                                           limit ' . (floor($request->page / 10) * 100) . ',' . (floor($request->page / 10) * 100 + 100));
            }

        }

        return response()->json($results);
    }

    public function search(Request $request)
    {
        if (trim($request->keyword) == "") {
            $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                               customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                               from lead
                               left join customer
                               on customer.id = lead.customer_id
                               left join employee
                               on employee.id = lead.assigned_to
                               left join stage
                               on lead.stage = stage.id');
            $count = DB::select('select count(*) as count  from lead
                                left join customer
                                on customer.id = lead.customer_id
                                left join employee
                                on employee.id = lead.assigned_to');

            $response = (object) ["data" => $results,
                "count" => $count[0]];
            error_log("hi");
            return response()->json($response);
        }

        $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                               customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                               from lead
                               left join customer
                               on customer.id = lead.customer_id
                               left join employee
                               on employee.id = lead.assigned_to
                               left join stage
                               on lead.stage = stage.id
                               WHERE
                               MATCH (customer.name,customer.email,customer.phone)
                               AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                               or
                               MATCH (lead.comment)
                               AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                               or
                               MATCH (stage.name)
                               AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                               limit 0, 100');
        $count = DB::select('select count(*) as count  from lead
                             left join customer
                             on customer.id = lead.customer_id
                             left join employee
                             on employee.id = lead.assigned_to
                             WHERE
                               MATCH (customer.name,customer.email,customer.phone)
                               AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)
                               or
                               MATCH (lead.comment)
                               AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE)');

        $response = (object) ["data" => $results,
            "count" => $count[0]];
        error_log("hi");
        return response()->json($response);
    }

    public function searchDate(Request $request)
    {
        if (trim($request->dateFrom) == "" || trim($request->dateTo) == "") {
            $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                               customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                               from lead
                               left join customer
                               on customer.id = lead.customer_id
                               left join employee
                               on employee.id = lead.assigned_to
                               left join stage
                               on lead.stage = stage.id');
            $count = DB::select('select count(*) as count  from lead
                                left join customer
                                on customer.id = lead.customer_id
                                left join employee
                                on employee.id = lead.assigned_to');

            $response = (object) ["data" => $results,
                "count" => $count[0]];
            error_log("hi");
            return response()->json($response);
        }

        $dateto = date('Y-m-d', strtotime($request->dateTo . ' + 1 days'));
        $datefrom = $request->dateFrom;
        $results = DB::select('select lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                               customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                               from lead
                               left join customer
                               on customer.id = lead.customer_id
                               left join employee
                               on employee.id = lead.assigned_to
                               left join stage
                               on lead.stage = stage.id
                               WHERE
                               lead.date_created >= ? AND lead.date_created < ?
                               limit 0, 100', [$datefrom, $dateto]);

        $response = (object) ["data" => $results,
            "count" => count($results)]
        ;
        error_log("hi");
        return response()->json($response);
    }

    public function sort(Request $request)
    {
        if ($request->column == 'Stage') {
            $request->column = "stage.sort";
        }

        if ($request->keyword && $request->columns) {
            $sqlstring = '';
            foreach ($request->columns as $column) {
                $sqlstring = $sqlstring . ',' . $column;
            }
            $results = DB::select('select lead.*, customer.name as CustomerName, customer.email,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                           left join stage
                                           on stage.id = lead.stage
                                           WHERE MATCH (customer.name,customer.email,customer.phone)
                                           AGAINST (\'+' . $request->keyword . '*\' IN BOOLEAN MODE) order by ' . $request->column . ' ' . $request->order . '
                                           limit 0,100');
        } else {
            $results = DB::select('select  lead.*, customer.name as CustomerName, customer.email as CustomerEmail,
                                           customer.phone as CustomerPhone, employee.name as EmployeeName, stage.name as stage
                                           from lead
                                           left join customer
                                           on customer.id = lead.customer_id
                                           left join employee
                                           on employee.id = lead.assigned_to
                                           left join stage
                                           on stage.id = lead.stage
                                           order by ' . $request->column . ' ' . $request->order . ' limit 0,100');
        }

        return response()->json($results);
    }

}
