<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = DB::select('select * from role where id = ?', [$request->role]);
        $mod = bcmod($role->value.'', $role->privilege.'');
        if($mod == '0')
            return $next($request);
        else
            return response()->json("not authorised");
    }
}
