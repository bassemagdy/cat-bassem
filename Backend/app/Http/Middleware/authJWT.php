<?php

namespace App\Http\Middleware;
use Closure;
use JWTAuth;
use Exception;

class authJWT
{
    public function handle($request, Closure $next)
    {

        try {
            if (JWTAuth::parseToken()->authenticate()) {
                exit(response()->json(['error' => 'Invalid user, User cannot be found on the system.'], 401));                
            }
        }
        catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) 
        {
            exit('TOOD : Refresh Token');
        } 
        catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) 
        {
            exit(response()->json(['error' => 'Invalid Token'], 401));                
        } 
        catch (Tymon\JWTAuth\Exceptions\JWTException $e) 
        {    
            exit(response()->json(['error' => 'Token not Found'], 401));                            
        }
        catch (Exception $e) {
            return response()->json(['error'=>'Something is wrong']);
        }

        return $next($request);
    }
}