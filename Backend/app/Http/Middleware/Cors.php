<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       
       // header("Access-Control-Allow-Origin: *");

        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin',
            'Content-Type' => 'application/json'
        ];
        if($request->getMethod() == "OPTIONS") {
            // The client-side application can set only headers allowed in Access-Control-Allow-Headers
            error_log("options");
            return response('OK',200)->header('Access-Control-Allow-Origin','*')
                                     ->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
                                     ->header('Access-Control-Allow-Headers', 'Content-Type, X-Auth-Token, Origin');
    
        }
        $response = $next($request);
        error_log('in handle');
        foreach($headers as $key => $value)
            $response->header($key, $value);
       // error_log($response);
        return $response;
            
    }
}
