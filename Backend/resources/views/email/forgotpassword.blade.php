<p>Dear {{ $name}},</p>
 <p> We recently received a request to change your CAT password. Please click on the link below to follow through.</p>
 <p> Link: http://catphaseone.azurewebsites.net/authentication/reset/{{ $link }}</p>
 <p> If you did not request to change your password, kindly ignore this message.</p>

 <p>  Best,</p>
 <p>  CAT team </p>