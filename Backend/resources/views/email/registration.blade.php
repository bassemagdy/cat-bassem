<p>Dear {{ $name}},</p>
 <p> Welcome onboard. The following are your authentication credentials.</p>
 <p> Username: {{ $username }}</p>
 <p> Password: {{ $password }}</p>

 Please go the to the following link to login. <a href="{{ $CRMLINK }}" >{{ $CRMLINK }}</a>

 <p>  Best,</p>
 <p>  CAT team </p>